<?php
use Frank\PageRenderer\PageRenderer;

// Get the default wordpress header
get_header();

$renderer = new PageRenderer();

$renderer->setIs404(true);

$renderer->render();

get_footer();
?>
