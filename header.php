<?php
/**
 * The header for our theme
 *
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-5TV9QCV');</script>
	<!-- End Google Tag Manager -->

	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php
	// Example preloading fonts
	/*
	<link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/FuturaCom-Bold.woff2" type="font/woff2" crossorigin />
	<link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/FuturaCom-Book.woff2" type="font/woff2" crossorigin />
	<link rel="preload" as="font" href="<?php echo get_template_directory_uri(); ?>/assets/fonts/FuturaCom-Medium.woff2" type="font/woff2" crossorigin />
	*/
	?>

	<?php wp_head(); ?>
	<script data-cfasync="false">
	// Global variable to pass backend vars to javascript
	var websiteData = {};
	websiteData.urlWebsite = '<?php echo get_site_url(); ?>';
	websiteData.urlTheme = '<?php echo get_template_directory_uri(); ?>';
	websiteData.googleMapsApi = '<?php echo GOOGLE_MAPS_API; ?>';
	websiteData.userIP = '<?php echo (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];  ?>';
	</script>
</head>

<body <?php body_class(); ?>>
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5TV9QCV"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->

	<?php
	// Google tags manager plugin tag
	if (function_exists('gtm4wp_the_gtm_tag')) {
		gtm4wp_the_gtm_tag();
	}
	?>
