// Helpers Functions
/**
 * Add an item to a sessionStorage() array
 * @param {String} name  The sessionStorage() key
 * @param {String} value The sessionStorage() value
 */
 var addToSessionStorageArray = function (name, value) {

    // Get the existing data
    var existing = sessionStorage.getItem(name);

    // If no existing data, create an array
    // Otherwise, convert the sessionStorage string to an array
    existing = existing ? existing.split(',') : [];

    // Add new data to sessionStorage Array
    existing.push(value);

    // Save back to sessionStorage
    sessionStorage.setItem(name, existing.toString());

};

/**
 * Add an item to a sessionStorage() object
 * @param {String} name  The sessionStorage() key
 * @param {String} key   The sessionStorage() value object key
 * @param {String} value The sessionStorage() value object value
 */
var addToSessionStorageObject = function (name, key, value) {

    // Get the existing data
    var existing = sessionStorage.getItem(name);

    // If no existing data, create an array
    // Otherwise, convert the sessionStorage string to an array
    existing = existing ? JSON.parse(existing) : {};

    // Add new data to sessionStorage Array
    existing[key] = value;

    // Save back to sessionStorage
    sessionStorage.setItem(name, JSON.stringify(existing));

};

/**
 * Declare Variables
 */
 var radius_circle;
 var geocoder;
 var markers = [];
 var centerMarker = [];
 var map;
 var type;
 var markerClusterer;
 var mapCenter = { lat: -30.48941970550993, lng: 133.59244824999996 };
 var radius_km = 30;
 var location_distance;
 var markerImage = websiteData.urlTheme + "/assets/images/common-location-purple.png";
 var centerMarkerImage = websiteData.urlTheme + "/assets/images/bluedot48.png";

/**
 * jQuery Functions
 */
 jQuery(function($) {

    function setMapHeight() {
        // Set container height
        const site_header_height = $('.header').outerHeight();
        const service_locator_form_height = $('.service_locator-search').outerHeight();
        // const service_locator_infobar_height = $('#service_locator-info_bar').outerHeight();
        const service_locator_infobar_height = 0;
        const map_height = $(window).height() - site_header_height;
        //$('.service_locator, .service_locator-content_wrapper, .service_locator-wrap').css('height', 'auto');
        $('.service_locator-map').css('height', map_height + "px");
        $('.service_locator-sidepane').css('height', 'auto');
        $('.service_locator-listing').css('height', 'auto');

        if (window.matchMedia('(min-width: 60rem)').matches) {
            // functionality for screens above than 992px
            $('.service_locator, .service_locator-content_wrapper, .service_locator-wrap').css('height', map_height + "px");
            $('.service_locator-sidepane').css('height', map_height + "px");
            $('.service_locator-listing').css('height', map_height - service_locator_form_height - service_locator_infobar_height + "px");
        }

    }
    setMapHeight();
    $(window).resize(function() {
        setMapHeight();
    });

    // Initialize Map
    function initializeMap() {

        const mapStyle = [
            {
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e6e6e6"
                }
              ]
            },
            {
              "elementType": "labels.icon",
              "stylers": [
                {
                  "visibility": "off"
                }
              ]
            },
            {
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "elementType": "labels.text.stroke",
              "stylers": [
                {
                  "color": "#e6e6e6"
                }
              ]
            },
            {
              "featureType": "administrative.land_parcel",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#bdbdbd"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "poi",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "poi.park",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "road",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#ffffff"
                }
              ]
            },
            {
              "featureType": "road.arterial",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#757575"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#dadada"
                }
              ]
            },
            {
              "featureType": "road.highway",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#616161"
                }
              ]
            },
            {
              "featureType": "road.local",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            },
            {
              "featureType": "transit.line",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#e5e5e5"
                }
              ]
            },
            {
              "featureType": "transit.station",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#eeeeee"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "geometry",
              "stylers": [
                {
                  "color": "#c9c9c9"
                }
              ]
            },
            {
              "featureType": "water",
              "elementType": "labels.text.fill",
              "stylers": [
                {
                  "color": "#9e9e9e"
                }
              ]
            }
          ];

        if (window.matchMedia('(min-width: 60rem)').matches) {
            var mapZoom = 5;
            var mapMinZoom = 5;
            var mapMaxZoom = 18;
        } else {
            var mapZoom = 4;
            var mapMinZoom = 4;
            var mapMaxZoom = 18;
        }

        // Load Map
        map = new google.maps.Map(document.getElementById("service_locator-map"), {
            zoom: mapZoom,
            minZoom: mapMinZoom,
            maxZoom: mapMaxZoom,
            center: mapCenter,
            styles: mapStyle,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            mapTypeControl: false,
            streetViewControl: false,
            fullscreenControl: false,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.TOP_RIGHT,
            },
        });

        geocoder = new google.maps.Geocoder();
        var initMapCenter = map.getCenter();
        var providerJson = '/wp-json/wp/v2/service-partner?status=publish';
        //console.log(initMapCenter);
        putMarkers('', providerJson, initMapCenter);
        serviceLocatorList(providerJson);
        postcodeAutocomplete(providerJson);

        // Reset Session Storage
        sessionStorage.removeItem('service_locator');
        addToSessionStorageObject('service_locator', 'provider_data', providerJson);
        addToSessionStorageObject('service_locator', 'service_category', '');
        addToSessionStorageObject('service_locator', 'pin_address', '');

    }
    google.maps.event.addDomListener(window, 'load', initializeMap);

    // Put Markers
    function putMarkers(type, serviceProvider, pinLatLng) {
        $('.service_locator-listing_tabs').empty();
        $('.service_locator-progress').css('z-index', 100).animate({opacity: 1}, 300);
        // Add Markers from Service Provider Json
        $.getJSON(serviceProvider, function(data) {

            var num = 0;

            // Create nearby provider array
            var nearby_provider_obj = [];

            // Add markers to the map.
            $.each(data, function(key, value) {
                var provider_id = value.id;
                var lat = value.acf.location.lat;
                var lng = value.acf.location.lng;
                var latLng = new google.maps.LatLng(lat, lng);

                var location_name = value.acf.location_site_name;
                var location_city = value.acf.location.city;
                var location_postcode = value.acf.location.post_code;
                var service_types = value.service_types;

                if (type == 'nearby') {
                    //distance in meters between your location and the marker
                    var distance_from_location = google.maps.geometry.spherical.computeDistanceBetween(pinLatLng, latLng);

                    if (distance_from_location <= radius_km * 1000) {
                        //console.log(distance_from_location);

                        // Add to provider list array
                        var list_provider_obj = {
                            "id" : provider_id,
                            "distance" : distance_from_location,
                            "latitude" : lat,
                            "longitude" : lng,
                            "location_name" : location_name,
                            "location_city" : location_city,
                            "location_postcode" : location_postcode,
                            "service_types" : service_types,
                        };

                        nearby_provider_obj.push(list_provider_obj);

                        var marker = new google.maps.Marker({
                            position: latLng,
                            map: map,
                            icon: markerImage
                        });
                        markers.push(marker);

                        // Add Listener on marker clicked
                        google.maps.event.addListener(marker, 'click', function(evt) {
                            map.panTo(marker.getPosition());
                            serviceLocatorPane(provider_id);
                        });

                        num++;
                    }

                } else {
                    var distance_from_location = google.maps.geometry.spherical.computeDistanceBetween(pinLatLng, latLng);
                    // Add to provider list array
                    var list_provider_obj = {
                        "id" : provider_id,
                        "distance" : distance_from_location,
                        "latitude" : lat,
                        "longitude" : lng,
                        "location_name" : location_name,
                        "location_city" : location_city,
                        "location_postcode" : location_postcode,
                        "service_types" : service_types,
                    };

                    nearby_provider_obj.push(list_provider_obj);
                    //console.log(nearby_provider_obj);

                    var marker = new google.maps.Marker({
                        position: latLng,
                        map: map,
                        icon: markerImage
                    });
                    markers.push(marker);

                    // Add Listener on marker clicked
                    google.maps.event.addListener(marker, 'click', function(evt) {
                        map.panTo(marker.getPosition());
                        serviceLocatorPane(provider_id);
                    });
                    num++;
                }

            });

            // Add center marker
            // var centerDot = new google.maps.Marker({
            //     position: pinLatLng,
            //     map: map,
            //     icon: centerMarkerImage,
            //     zIndex: 10000
            // });
            // centerMarker.push(centerDot);

            var nearby_provider = nearby_provider_obj.sort(function(a,b) { return parseFloat(a.distance) - parseFloat(b.distance) } );
            //console.log(nearby_provider);

            // Update service provider list
            nearby_provider.forEach(function(element, index, array) {
                //console.log(element.id);
                serviceProviderItem(element.id, element.location_name, element.location_city, element.location_postcode, element.distance, element.service_types);
            });

            // Add a marker clusterer to manage the markers.
            markerClusterer = new MarkerClusterer(map, markers, {
                imagePath: '/wp-content/themes/coact-2018/assets/icons/common-cluster.svg',
                averageCenter: true,
                enableRetinaIcons: true,
                gridSize : 68,
                styles: [{
                    height: 40,
                    textColor: 'white',
                    textSize: 14,
                    url: '/wp-content/themes/coact-2018/assets/icons/common-cluster.png',
                    width: 40,
                    textLineHeight: 40,
                    fontWeight: 'bold',
                    fontFamily: 'Arial, sans-serif'
                }]
            });
            markerClusterer.addMarkers(markers);

            // Show number of results text
            if (type == 'nearby') {
                if (num > 0) {
                    var provider_result = 'Showing ' + num  + ' Service Partners';
                    $('.service_locator-listing_title').html( provider_result );
                } else {
                    var provider_result = 'Sorry, no service providers found';
                    $('.service_locator-listing_title').html( provider_result );
                }
            } else {
                var provider_result = 'Showing ' + num  + ' Service Partners';
                $('.service_locator-listing_title').html( provider_result );
            }
        }).done(function(data) {
            $('.service_locator-progress').css('z-index', -1).animate({opacity: 0}, 300);
        });

    }

    // Service Locator List
    function serviceLocatorList(serviceProvider) {
        //console.log('serviceLocatorList');
        $.getJSON(serviceProvider, function(data) {
            //console.log(data);
            $.each(data, function(key, value) {

                var provider_id = value.id;

                var location_name = value.acf.location_site_name;
                var location_city = value.acf.location.city;
                var location_postcode = value.acf.location.post_code;

                var service_types = value.service_types;

                serviceProviderItem(provider_id, location_name, location_city, location_postcode, location_distance, service_types);

            });
        }).done(function(data) {
        });
    }

    function serviceProviderItem(provider_id, location_name, location_city, location_postcode, location_distance, service_types) {
        if (!location_distance) {
            location_distance = '';
        } else {
            location_distance = location_distance / 1000;
            location_distance = '<span class="inline-block">&nbsp;&nbsp;|&nbsp;&nbsp;~' + location_distance.toFixed(2) + ' km</span>';
        }
        var service_type_tags = '';
        for (var i = 0; i < service_types.length; i++) {
            var tag = $.map(service_types_tax, function(val) {
                return val.term_id == service_types[i] ? val.term_name : null;
            });
            service_type_tags += '<span class="service_locator-listing_tag tag_element">'+tag[0]+'</span>';
        }

        var provider_item = '<li class="service_locator-listing_tab link_overlay">'
                + '<button class="service_locator-listing_tab_link link_overlay-btn" title="View ' + location_name + '" data-id="' + provider_id + '"></button>'
                + '<h4 class="service_locator-listing_partner_title">' + location_name + '</h4>'
                + '<div class="service_locator-listing_tags tags">'
                    + service_type_tags
                + '</div>'
                + '<span class="svg_icon right-arrow"><div><div>'
                    + '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="injected-svg" data-src="' + '/wp-content/themes/coact-2018/assets/icons/common-next.svg"><title>icon / next</title><path d="M9 8.175L12.709 12 9 15.825 10.142 17 15 12l-4.858-5z"></path></svg>'
                + '</div></div></span>'
            + '</li>';

        $('.service_locator-listing_tabs').append( provider_item );
    }

    $('.select_service_type').select2({
        placeholder: 'Select a service type',
        allowClear: true,
        minimumResultsForSearch: Infinity
    });

    $('.select_service_type').on('select2:select', function (e) {
        var data = e.params.data;
        var service_provider_category = data.id;
        $('#input_postcode').val('');
        $('.service_locator-postcode .btn-clear').hide();
        selectServiceType(service_provider_category);
    });

    $('.select_service_type').on('select2:clear', function (e) {
        var service_provider_category = '';
        $('#input_postcode').val('');
        $('.service_locator-postcode .btn-clear').hide();
        selectServiceType(service_provider_category);
    });

    function selectServiceType(service_provider_category) {
        var providerJson = '/wp-json/wp/v2/service-partner?status=publish&service_types=' + service_provider_category;
        addToSessionStorageObject('service_locator', 'service_category', service_provider_category);
        addToSessionStorageObject('service_locator', 'provider_data', providerJson);
        //console.log(service_provider_category);

        // Remove all radius and markers from map before displaying new ones
        if (radius_circle) {
            radius_circle.setMap(null);
            radius_circle = null;
        }
        deleteMarkers();
        markerClusterer.clearMarkers();
        $('.service_locator-listing_tabs').empty();

        var markCenter = map.getCenter();
        //console.log(providerJson);
        putMarkers('', providerJson, markCenter);

        map.setCenter(mapCenter);
        map.panTo(mapCenter);
        map.setZoom(5);
    }

    function postcodeAutocomplete(serviceProvider) {
        const input = document.getElementById('input_postcode');
        enableEnterKey(input);
        const autocomplete = new google.maps.places.Autocomplete((input), {
            types: ['(regions)'],
            //types: ['geocode'],
            componentRestrictions: {country: 'au'}
        });

        //enableEnterKey(input);
        autocomplete.addListener("place_changed", function() {
            var address = $('#input_postcode').val();
            var sessionObj = JSON.parse(sessionStorage.getItem('service_locator'));
            var providerJson = sessionObj.provider_data;
            //console.log(address);
            const place = autocomplete.getPlace();
            if (!place.place_id) {
              return;
            }
            //console.log(providerJson);
            addToSessionStorageObject('service_locator', 'provider_data', providerJson);
            addToSessionStorageObject('service_locator', 'pin_address', address);
            //$('#service_locator-list').show();
            showNearbySites(providerJson, address);
        });
    }

    $('#input_postcode').on('keyup blur', function(event) {
        if($(this).val().length != 0) {
            $('.service_locator-postcode .btn-clear').show();
        }
    });

    $(document).on('click', '.service_locator-postcode .btn-clear', function(e) {
        e.preventDefault();
        $('#input_postcode').val('');
        $('.service_locator-postcode .btn-clear').hide();
        $('.select_service_type').trigger({
            type: 'select2:clear'
        });
        $('.select_service_type').val(null).trigger('change');
    });

    // Simulate key down on enter key in autocomplete
    function enableEnterKey(input) {

        /* Store original event listener */
        const _addEventListener = input.addEventListener

        const addEventListenerWrapper = function(type, listener) {
            if (type === 'keydown') {
                /* Store existing listener function */
                const _listener = listener
                listener = function(event) {
                    /* Simulate a 'down arrow' keypress if no address has been selected */
                    const suggestionSelected = document.getElementsByClassName('pac-item-selected').length
                    if (event.key === 'Enter' && !suggestionSelected) {
                        const e = new KeyboardEvent('keydown', {
                            key: 'ArrowDown',
                            code: 'ArrowDown',
                            keyCode: 40,
                        })
                        _listener.apply(input, [e])
                    }
                    _listener.apply(input, [event])
                }
            }
            _addEventListener.apply(input, [type, listener])
        }

        input.addEventListener = addEventListenerWrapper
    }

    // Show nearby location
    function showNearbySites(serviceProvider, address) {

        // Remove all radius and markers from map before displaying new ones
        if (radius_circle) {
            radius_circle.setMap(null);
            radius_circle = null;
        }
        deleteMarkers();
        markerClusterer.clearMarkers();
        $('#service_locator-list').empty();

        // Display Markers
        if (geocoder) {
            geocoder.geocode({'address': address}, function (results, status) {
                if (status == google.maps.GeocoderStatus.OK) {
                    if (status != google.maps.GeocoderStatus.ZERO_RESULTS) {
                        var pinLatLng = results[0].geometry.location;
                        radius_circle = new google.maps.Circle({
                            center: pinLatLng,
                            radius: radius_km * 1000,
                            strokeColor: "#B86AAB",
                            strokeOpacity: 0.6,
                            strokeWeight: 2,
                            fillColor: "#B86AAB",
                            fillOpacity: 0.1,
                            clickable: false,
                            map: map
                        });
                        if (radius_circle) {
                            map.fitBounds(radius_circle.getBounds());
                        }

                        putMarkers('nearby', serviceProvider, pinLatLng);

                    } else {
                        //alert("No results found while geocoding!");
                        console.log("No results found while geocoding!");
                    }
                } else {
                    //alert("Geocode was not successful: " + status);
                    console.log("Geocode was not successful: " + status);
                }
            });
        }

    }

    $(document).on('click', '.link_overlay-btn', function(e) {
        e.preventDefault();
        var data_id = $(this).data('id');
        //console.log(data_id);
        serviceLocatorPane(data_id);
    });

    // Service Locator Details Pane
    function serviceLocatorPane(provider_id) {
        $('.service_locator-progress').css('z-index', 100).animate({opacity: 1}, 300);
        $.getJSON("/wp-json/wp/v2/service-partner/" + provider_id, function(data) {

            var title = data.acf.location_site_name;
            var service_types_data = data.service_types;
            var details_service_type = '';
            for (var i = 0; i < service_types_data.length; i++) {
                var tag = $.map(service_types_tax, function(val) {
                    return val.term_id == service_types_data[i] ? val.term_name : null;
                });
                if (i > 0) {
                    details_service_type += '&comma;&nbsp;<span class="service_locator-listing_tag tag">'+tag[0]+'</span>';
                } else {
                    details_service_type += '<span class="service_locator-listing_tag tag">'+tag[0]+'</span>';
                }
            }
            var contact_numbers = data.acf.contact_numbers;
            var list_contact_numbers = '';
            contact_numbers.forEach(function(element) {
                //console.log(element.phone_label);
                var tel = element.phone_number;
                tel = tel.replace(/\s+/g, '');

                list_contact_numbers += '<div class="service_locator-proofPoint">'
                            + '<span class="svg_icon"><div><div><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="injected-svg" data-src="' + '/wp-content/themes/coact-2018/assets/icons/common-phone.svg"><path class="svg_inherit" d="M6.62 10.79a15.15 15.15 0 0 0 6.59 6.59l2.2-2.2a1 1 0 0 1 1.02-.24c1.12.37 2.33.57 3.57.57a1 1 0 0 1 1 1V20a1 1 0 0 1-1 1A17 17 0 0 1 3 4a1 1 0 0 1 1-1h3.5a1 1 0 0 1 1 1c0 1.25.2 2.45.57 3.57a1 1 0 0 1-.25 1.02l-2.2 2.2z"></path></svg></div></div></span>'
                            + '<a href="tel:' + tel + '" class="phantom-phone-number">'+ element.phone_label + ':&nbsp;' + element.phone_number + '</a>'
                            + '</div>';
            });

            var address_data = data.acf.location.address;
            var description_data = data.acf.description;

            var checkmark_data = data.acf.checkmark_list;
            var checkmark_list = '';
            checkmark_data.forEach(function(element) {
                checkmark_list += '<div class="service_locator-proofPoint"><span class="svg_icon"><div><div><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="injected-svg" data-src="' + '/wp-content/themes/coact-2018/assets/icons/common-tick.svg"><title>icon / tick</title><g fill="none" fill-rule="evenodd"><path d="M0 0h24v24H0z"></path><path fill="#45C2BF" fill-rule="nonzero" d="M9 16.2L4.8 12l-1.4 1.4L9 19 21 7l-1.4-1.4z"></path></g></svg></div></div></span><span>' + element.point + '</span></div>';
            });

            var link_data = data.link;

            var details_body = '';

                details_body += '<div class="service_locator-details_title details_block">';
                    if (title) {
                        details_body += '<h3>'+title+'</h3>';
                    }
                    if (details_service_type) {
                        details_body += '<div class="service_locator-details_tags"><div class="tags">';
                            details_body += details_service_type;
                        details_body += '</div></div>';
                    }
                details_body += '</div>';

                details_body += '<div class="details_block">';
                    details_body += '<div class="service_locator-details_contact list_block">';

                        if (address_data) {
                            details_body += '<div class="service_locator-proofPoint">'
                                + '<span class="svg_icon"><div><div><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" class="injected-svg" data-src="' + '/wp-content/themes/coact-2018/assets/icons/common-location.svg"><path class="svg_inherit" d="M12 0C7.3 0 3.5 3.76 3.5 8.4 3.5 14.7 12 24 12 24s8.5-9.3 8.5-15.6C20.5 3.76 16.7 0 12 0zm0 12a3.5 3.5 0 1 1 0-7 3.5 3.5 0 0 1 0 7z"></path></svg></div></div></span>'
                                + '<span>' + address_data + '</span>'
                                + '</div>';
                        }

                        if (list_contact_numbers) {
                            details_body += list_contact_numbers;
                        }

                    details_body += '</div>'; // .list_block

                    if (description_data) {
                        details_body += '<div class="service_locator-details_description">' + description_data + '</div>';
                    }

                details_body += '</div>'; // .details_block

                if (checkmark_list) {
                    details_body += '<div class="service_locator-details_proofpoints details_block list_block">';

                        details_body += checkmark_list;

                    details_body += '</div>'; // .list_block
                }

            $('.service_locator-details_body').append( details_body );


            // Footer
            details_footer = '';
            if (link_data) {
                details_footer += '<a href="' + link_data + '#enquiry_form" class="button register-button">Register your interest</a>';
                details_footer += '<a href="' + link_data + '" class="button button--secondary">Learn More</a>';
            }

            $('.service_locator-details_footer').append( details_footer );

        }).done(function(data) {
            $('.service_locator-progress').css('z-index', -1).animate({opacity: 0}, 300);
            $('.service_locator-details').addClass('open');
            $('body').addClass('overflow-hidden');

        });

    }
    $('.service_locator-details_overlay, .service_locator-details_header .back-button, .service_locator-details_header .close-button').click(function (e) {
        e.preventDefault();
        $('.service_locator-details').removeClass('open');
        $('.service_locator-details').addClass('closed');
        $('body').removeClass('overflow-hidden');
        $('.service_locator-details_body').empty();
        $('.service_locator-details_footer').empty();
    });

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
        // for (var x=0; x < centerMarker.length; x++){
        //     centerMarker[x].setMap(map);
        // }
    }

    // Removes the markers from the map, but keeps them in the array.
    function hideMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        hideMarkers();
        markers = [];
    }

    /*
    * Service Locator View Mode
    */
    $('#view-as-list').click(function (e) {
        e.preventDefault();
        $(this).addClass('active');
        $('#view-on-map').removeClass('active');
        $('.service_locator-listing_tabs').show();
        $('.service_locator-map').hide();
    });
    $('#view-on-map').click(function (e) {
        e.preventDefault();
        $(this).addClass('active');
        $('#view-as-list').removeClass('active');
        $('.service_locator-listing_tabs').hide();
        $('.service_locator-map').show();
    });


});