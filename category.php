<?php
/**
* Define the template renderer to use for the archive
*/
define('APP_ARCHIVE_TEMPLATE_RENDERER', 'App\TemplateRenderer\TemplateRenderer_archive_default');

include __DIR__ . '/index.php';
?>
