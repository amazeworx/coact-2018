<?php
define('APP_VERSION', '2021041008');
define('GOOGLE_MAPS_API', 'AIzaSyALa7CVVKAaAPSw9-zopXMh2C7wcn6Zo10');
define('POSTS_PER_PAGE', -1);

define('CRON_BRIDGE_ENTRIES', 'cron_bridge_entries');

// Check if Composer's autoload file is available
$composerAutoload = get_template_directory() . '/vendor/autoload.php';
if (file_exists($composerAutoload)) {
	require $composerAutoload;
} else {
	echo "Couldn't find composer autoload file. Make sure you installed the composer dependencies.";
	die();
}

// Errors handler
if (defined('WP_DEBUG') && WP_DEBUG && !is_admin()) {
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	$whoops = new \Whoops\Run;
	$whoops->pushHandler(new \Whoops\Handler\PrettyPageHandler);
	$whoops->register();
}

$configLoader = new Config\ConfigLoader([
	'\Config\ACF\Colours',
	'\Config\ACF\Init',
	'\Config\Algolia\AdditionalFields',
	'\Config\Algolia\SearchablePosts',
	'\Config\ContactForm\ContactForm',
	'\Config\Wordpress\Admin',
	'\Config\Wordpress\Cron',
	'\Config\Wordpress\Menus',
	'\Config\Wordpress\PostTypes',
	'\Config\Wordpress\Rest',
	'\Config\Wordpress\Taxonomies',
	'\Config\Wordpress\Theme',
]);
$configLoader->loadConfig();

?>
