<?php

namespace Frank\Traits;
/**
 * Display the debug stripe
 */
trait DisplayDebugStripe {

	/**
	 * Display the debug stripe when WP_DEBUG is set and the GET param `debug` is in the URL
	 *
	 * @param string the content for the stripe
	 */
	function displayDebugStripe($content) {
		if (
			defined('WP_DEBUG') &&
			WP_DEBUG &&
			isset($_GET['debug'])
		): ?>
			<div class="debug_stripe">
				<span><?php echo $content; ?></span>
			</div>
			<?php
		endif;
	}
}


?>
