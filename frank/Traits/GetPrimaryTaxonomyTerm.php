<?php

namespace Frank\Traits;
/**
* Helpers to get the primary taxonomy term if Yoast is available, otherwise, pick the first term associated with a post
*/
trait GetPrimaryTaxonomyTerm {

	/**
	 * Get the primary taxonomy term if Yoast is available, otherwise, pick the first term associated with a post.
	 *
	 * Inspired from http://www.joshuawinn.com/using-yoasts-primary-category-in-wordpress-theme/
	 *
	 * @param int the ID of the post
	 * @param string the taxonomy name
	 * @return \WP_Term|null the primary term
	 */
	protected function getPrimaryTaxonomyTerm($postId, $taxonomy) {

		$terms = wp_get_post_terms($postId, $taxonomy);

		if (
			$terms &&
			is_array($terms) &&
			sizeof($terms)
		) {
			// By default use the first term return by Wordpress
			$result = $terms[0];

			// Attempt to use the Yoast SEO primary term class
			if (class_exists('WPSEO_Primary_Term')) {
				$primaryTermClass = new \WPSEO_Primary_Term($taxonomy, $postId);
				$primaryTerm = $primaryTermClass->get_primary_term();
				$term = get_term($primaryTerm);
				if (!is_wp_error($term)) {
					$result = $term;
				}
			}

			return $result;
		}
		return null;
	}
}

?>
