<?php

namespace Frank\Traits;
/**
* Helpers to format the page template
*/
trait FormatPageTemplate {

	/**
	 * Format a post template name
	 *
	 * @param int $postId the post id to query
	 * @return string the page template or "default" if it's the default apge template
	 */
	protected function formatPageTemplate($postId) {
		// Retreive the template name
		$templateName = get_page_template_slug($postId);

		// Remove the 'template-' at the beginning of the template name
		$templateName = str_replace('template-', '', $templateName);
		// Remove the ".php" at the end of the template name
		$templateName = str_replace('.php', '', $templateName);

		if (empty($templateName)) {
			$templateName = 'default';
		}

		return $templateName;
	}
}

?>
