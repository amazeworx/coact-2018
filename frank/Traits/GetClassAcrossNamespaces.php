<?php

namespace Frank\Traits;
/**
* Helpers to get class either in the App namespace or in the Frank namespace
*/
trait GetClassAcrossNamespaces {

	/**
	 * Get class either in the App namespace or in the Frank namespace
	 *
	 * @param string $className the class name to look for (can includes sub-namespaces)
	 * @return string|null the fully namespaced class name or null if none found
	 */
	protected function getClassAcrossNamespaces($className) {

		$appClassName = "\\App\\{$className}";
		$frankClassName = "\\Frank\\{$className}";

		if(class_exists($appClassName)) {
			return $appClassName;
		} else if(class_exists($frankClassName)) {
			return $frankClassName;
		}

		return null;
	}
}

?>
