<?php
namespace Frank\Traits;
/**
 * Generate a button based on a button structure
 */
trait GenerateButton {
	/**
	 * Output a button bases on the ACF button atom
	 *
	 * @param array button structure
	 * @param string classes, the list of classes to apply to the button
	 */
	protected function generateButton($buttonData, $classes = 'button') {

		if (isset(
			$buttonData['button_type'],
			$buttonData['button_label'],
			$buttonData['button_url']
		)) {
			// button attributes
			$url = '';
			$target = '';
			$rel = '';
			$label = $buttonData['button_label'];
			if ($buttonData['button_type'] === 'internal') {
				// Page link
				$url = (isset($buttonData['button_url_page']))? $buttonData['button_url_page'] : '';
			} else if ($buttonData['button_type'] === 'file') {
				// File
				$attachmentId = (isset($buttonData['button_url_file']))? $buttonData['button_url_file'] : '';
				$url = wp_get_attachment_url($attachmentId);
				$target = '_blank';
			} else if ($buttonData['button_type'] === 'custom') {
				// Custom link
				$url = (isset($buttonData['button_url']))? $buttonData['button_url'] : '';
			} else if ($buttonData['button_type'] === 'external') {
				// External link
				$url = (isset($buttonData['button_url']))? $buttonData['button_url'] : '';
				$rel = 'nofollow noopener noreferrer';
				$target = '_blank';
			}

			?>
			<a
				class="<?php echo $classes; ?>"
				href="<?php echo $url; ?>"
				target="<?php echo $target; ?>"
				rel="<?php echo $rel; ?>"
				>
				<?php echo $label; ?>
			</a>
			<?php
		}

	}
}


?>
