<?php
namespace Frank\TemplateRenderer;

use Frank\TemplateRenderer\TemplateRenderer;

/**
 * Template Renderer for a 404 page
 */
class TemplateRenderer_404 extends TemplateRenderer {

	/**
	 * The template sections renderer and their formatter
	 *
	 * The template sections array must be formatted as follow:
	 * [
	 *   ["section_renderer_name", "section_formatter_name"],
	 *   ["section_renderer_name", "section_formatter_name"]
	 * ]
	 *
	 * Please note the section formatter and renderer name "flexible_sections" is reserved for flexible sections
	 *
	 * @var array<array<string>>
	 */
	protected $templateSections = [
		['404_content', '404_content'],
	];


}

?>
