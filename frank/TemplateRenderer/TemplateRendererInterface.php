<?php
namespace Frank\TemplateRenderer;

interface TemplateRendererInterface {

	/**
	 * Render the page sections
	 *
	 * Return the page content as a string
	 *
	 * @return string the page content
	 */
	public function render();
}

?>
