<?php
namespace Frank\TemplateRenderer;

use Exception;
use Frank\TemplateRenderer\TemplateRendererInterface;

/**
 * Abstract class defining a template renderer
 *
 * Provides pre-defined methods to allow faster template renderer creation
 */
abstract class TemplateRenderer implements TemplateRendererInterface {

	use \Frank\Traits\GetClassAcrossNamespaces;

	/**
	 * The post associated to the post to render
	 *
	 * @var \WP_Post $post
	 */
	protected $post;

	/**
	 * The template sections renderer and their formatter
	 *
	 * The template sections array must be formatted as follow:
	 * [
	 *   ["section_renderer_name", "section_formatter_name"],
	 *   ["section_renderer_name", "section_formatter_name"]
	 * ]
	 *
	 * Please note the section formatter and renderer name "flexible_sections" is reserved for flexible sections
	 *
	 * @var array<array<string>>
	 */
	protected $templateSections = [];

	public function __construct(\WP_Post $post = null) {
		// Initialise the post
		$this->post = $post;
	}

	public function render() {
		$result = '';

		// Loop through all the pages sections and render them
		if (sizeof($this->templateSections)) {
			foreach ($this->templateSections as $section) {
				if (
					is_array($section) &&
					sizeof($section) === 2 &&
					is_string($section[0]) &&
					is_string($section[1])
				) {
					$sectionRendererName = $section[0];
					$sectionFormatterName = $section[1];

					// Render the sections
					$sectionResult = $this->renderSection($sectionRendererName, $sectionFormatterName);
					// Add the section to the final result
					if ($sectionResult) {
						$result .= $sectionResult;
					}
				} else {
					if (
						!is_array($section) ||
						sizeof($section) !== 2
					) {
						throw new Exception("Section with invalid format. Must be array with size of 2. Received " . gettype($section));
					} else if (!is_string($section[0])) {
						throw new Exception("Section renderer name not a string. Received {$section[0]}");
					} else if (!is_string($section[1])) {
						throw new Exception("Section formatter name not a string. Received {$section[1]}");
					}
				}
			}
		}

		return $result;
	}

	protected function renderSection($sectionRendererName, $sectionFormatterName) {
		if (!is_string($sectionRendererName)) {
			throw new Exception("Section renderer name not a string. Received {$sectionRendererName}");
		} else if (!is_string($sectionFormatterName)) {
			throw new Exception("Section formatter name not a string. Received {$sectionFormatterName}");
		}
		$sectionRenderer = $this->getSectionRenderer($sectionRendererName);
		$sectionFormatter = $this->getSectionFormatter($sectionFormatterName);

		// Set the data for the formatter
		$sectionFormatter->setPost($this->post);
		// Initialise the content for the section
		$sectionFormatter->initialiseData();
		// Set the formatter for the renderer
		$sectionRenderer->setSectionFormatter($sectionFormatter);

		return $sectionRenderer->render();
	}

	/**
	 * Get the section rendering class
	 *
	 * Will first check if there is a rendering class in app then in frank.
	 * If none is found, will throw an error
	 *
	 * @throws Exception If no section renderer class is found
	 * @param string The name of the renderer
	 * @return \Frank\SectionRenderer\SectionRendererInterface the section renderer
	 */
	protected function getSectionRenderer($rendererName) {
		if (!is_string($rendererName)) {
			throw new Exception("Section renderer name not a string. Received {$rendererName}");
		}
		$partialClassName = "SectionRenderer\\SectionRenderer_{$rendererName}";

		$className = $this->getClassAcrossNamespaces($partialClassName);

		if (is_null($className)) {
			throw new Exception("SectionRenderer {$partialClassName} not found.");
		}

		//Check if the class implements the SectionRendererInterface
		$interfaces = class_implements($className);
		if (isset($interfaces['Frank\SectionRenderer\SectionRendererInterface'])) {
			return new $className();
		} else {
			throw new Exception("SectionRenderer {$className} doesn't implement Frank\SectionRenderer\SectionRendererInterface.");
		}
	}

	/**
	 * Get the section formatter class
	 *
	 * Will first check if there is a formatter class in app then in frank.
	 * If none is found, will throw an error
	 *
	 * @throws Exception If no section formateer class is found
	 * @param string The name of the formatter
	 * @return \Frank\SectionFormatter\SectionFormatterInterface the section formatter
	 */
	protected function getSectionFormatter($formatterName) {
		if (!is_string($formatterName)) {
			throw new Exception("Section formatter name not a string. Received {$formatterName}");
		}
		$partialClassName = "SectionFormatter\\SectionFormatter_{$formatterName}";

		$className = $this->getClassAcrossNamespaces($partialClassName);

		if (is_null($className)) {
			throw new Exception("SectionFormatter {$partialClassName} not found.");
		}

		//Check if the class implements the SectionFormatterInterface
		$interfaces = class_implements($className);
		if (isset($interfaces['Frank\SectionFormatter\SectionFormatterInterface'])) {
			return new $className();
		} else {
			throw new Exception("SectionFormatter {$className} doesn't implement Frank\SectionFormatter\SectionFormatterInterface.");
		}
	}
}

?>
