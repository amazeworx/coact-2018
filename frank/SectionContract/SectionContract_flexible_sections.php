<?php
namespace Frank\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter for the flexible sections
*/
interface SectionContract_flexible_sections extends SectionFormatterInterface {

	/**
	 * Tells if there is flexible sections available
	 *
	 * @return boolean true if there is flexible sections
	 */
	public function hasSections();


	/**
	 * Get the flexible sections
	 *
	 * @return array The flexibles sections array
	 */
	public function getSections();

}

?>
