<?php
namespace Frank\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a cta section
*/
interface SectionContract_cta extends SectionFormatterInterface {

	/**
	 * Is title set
	 *
	 * @return boolean
	 */
	public function hasTitle();

	/**
	 * Get the title
	 *
	 * @return string the title from text field
	 */
	public function getTitle();

	/**
	 * Is description set
	 *
	 * @return boolean
	 */
	public function hasDescription();

	/**
	 * Get the description
	 *
	 * @return string the description from text field
	 */
	public function getDescription();

	/**
	* Is the buttons repeater set
	*
	* @return boolean
	*/

	public function hasButtons();

	/**
	* Get the buttons
	*
	* @return string content wysiwyg
	*/

	public function getButtons();
}

?>
