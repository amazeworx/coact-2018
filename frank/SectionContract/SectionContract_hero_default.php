<?php
namespace Frank\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of page default hero
*/
interface SectionContract_hero_default extends SectionFormatterInterface {

	/**
	 * Check if a hero image is available
	 *
	 * @return boolean true if the hero has a background image
	 */
	public function hasBackgroundImage();

	/**
	 * Get the hero background image url
	 *
	 * @return string background image url
	 */
	public function getBackgroundImage();

	/**
	 * Check if the hero has a title
	 *
	 * @return boolean true if the hero has a title
	 */
	public function hasTitle();

	/**
	 * Get the hero text
	 *
	 * @return string the title
	 */
	public function getTitle();
}

?>
