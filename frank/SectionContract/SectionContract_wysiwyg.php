<?php
namespace Frank\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of an WYSIWYG section
*/
interface SectionContract_wysiwyg extends SectionFormatterInterface {

	/**
	 * Tells if the section has content or not
	 *
	 * @return boolean true if the section has content
	 */
	public function hasContent();

	/**
	 * Get the content
	 *
	 * @return string HTML content from the WYSIWYG editor
	 */
	public function getContent();

}

?>
