<?php
namespace Frank\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a introduction section
*/
interface SectionContract_introduction extends SectionFormatterInterface {

	/**
	 * Is content set
	 *
	 * @return boolean
	 */
	public function hasContent();

	/**
	 * Get the page content
	 *
	 * @return string the page content coming from the WYSIWYG
	 */
	public function getContent();

	/**
	* Is the buttons repeater set
	*
	* @return boolean
	*/

	public function hasButtons();

	/**
	* Get the buttons
	*
	* @return string content wysiwyg
	*/

	public function getButtons();

}

?>
