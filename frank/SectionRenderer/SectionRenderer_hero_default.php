<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\SectionContract\SectionContract_hero_default;

/**
* Class defining a default hero
*/
class SectionRenderer_hero_default extends SectionRenderer {

	/**
	* The section formatter
	*
	* @var \Frank\SectionContract\SectionContract_hero_default
	*/
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_hero_default';

	public function generateSection() {
		if ($this->formatter->hasBackgroundImage()):
			?>
			<div class="hero">
				<div class="hero-background">
					<?php echo $this->formatter->getBackgroundImage(); ?>
				</div>
				<?php if ($this->formatter->hasTitle()): ?>
					<div class="hero-title">
						<?php echo $this->formatter->getTitle(); ?>
					</div>
				<?php endif; ?>
			</div>
			<?php
		endif;
	}

}

?>
