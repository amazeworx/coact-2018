<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRendererInterface;

/**
 * Abstract class defining a section renderer
 *
 * Provides pre-defined methods to allow faster section renderer creation
 */
abstract class SectionRenderer implements SectionRendererInterface {
	use \Frank\Traits\DisplayDebugStripe;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionFormatter\SectionFormatterInterface
	 */
	protected $formatter = null;

	/**
	 * The contract the section formatter must implements
	 *
	 * @var string
	 */
	protected $sectionFormatterContract = null;

	/**
	 * The section template path
	 *
	 * @var string path to the section template
	 */
	protected $sectionTemplatePath = null;

	/**
	 * Output the section content
	 *
	 */
	protected abstract function generateSection();

	public function setSectionFormatter(\Frank\SectionFormatter\SectionFormatterInterface $dataFormatter) {
		if ($this->sectionFormatterContract) {
			// Check if the section formatter is implementing the contract for the section
			$interfaces = class_implements($dataFormatter);
			if (!isset($interfaces[$this->sectionFormatterContract])) {
				$className = get_class($dataFormatter);
				throw new \Exception("SectionFormatter {$className} must implement {$this->sectionFormatterContract}.");
			}
		}
		$this->formatter = $dataFormatter;
	}

	public function render() {
		$result = null;

		if ($this->formatter->shouldDisplaySection()) {
			// Buffer the output to return it as a string
			ob_start();
			$this->displayDebugStripe(get_class($this));
			// Output the section
			$this->generateSection();
			// Get the buffer content
			$result = ob_get_clean();

			if (empty($result)) {
				return null;
			}
		}

		return $result;
	}

}


?>
