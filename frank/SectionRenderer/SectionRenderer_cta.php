<?php
namespace Frank\SectionRenderer;

use Frank\SectionContract\SectionContract_cta;
use Frank\SectionRenderer\SectionRenderer;

/**
 * Class defining a section renderer for an cta section
 */
class SectionRenderer_cta extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_cta
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_cta';

	public function generateSection() {
		if ($this->formatter->hasTitle()) :
			?>
			<div class="cta">
				<?php echo  $this->formatter->getTitle(); ?>
				<?php if ($this->formatter->hasDescription()) : ?>
					<?php echo  $this->formatter->getDescription(); ?>
				<?php endif; ?>
				<?php
				if ($this->formatter->hasButtons()) {
					foreach ($this->formatter->getButtons() as $buttonData) {
						$this->generateButton($buttonData);
					}
				}
				?>
			</div>
			<?php
		endif;
	}

}

?>
