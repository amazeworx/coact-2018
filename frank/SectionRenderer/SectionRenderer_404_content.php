<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;

/**
 * Class defining a 404 content
 */
class SectionRenderer_404_content extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_404_content
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_404_content';

	public function generateSection() {
		?>
		<h1>Page not found</h1>
		<?php
	}

}

?>
