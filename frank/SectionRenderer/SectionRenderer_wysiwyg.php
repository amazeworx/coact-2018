<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\SectionContract\SectionContract_wysiwyg;

/**
 * Class defining a WYSIWYG section renderer
 */
class SectionRenderer_wysiwyg extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_wysiwyg
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_wysiwyg';

	public function generateSection() {

	if ($this->formatter->hasContent()):
		?>
		<div class="wysiwyg">
			<?php echo $this->formatter->getContent(); ?>
		</div>
		<?php
	endif;

	}

}

?>
