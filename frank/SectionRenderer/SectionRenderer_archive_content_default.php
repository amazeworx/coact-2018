<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;


/**
 * Class defining a default archive content
 */
class SectionRenderer_archive_content_default extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_archive_content_default
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_archive_content_default';

	public function generateSection() {
		if ($this->formatter->hasPosts()):
			?>
			<div class="archive_content_default">
				<div class="archive_content_default-tiles_wrapper">
					<?php $this->generatePostsTiles(); ?>
				</div>
				<?php if ($this->formatter->hasPagination()): ?>
					<div class="archive_content_default-pagination_wrapper">
						<?php echo $this->formatter->getPagination(); ?>
					</div>
				<?php endif; ?>
			</div>
			<?php
		else:
			?>
			No posts are available.
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasPosts()) {
			foreach ($this->formatter->getPosts() as $post) {
				// Dynamically call the tiles renderer
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}
		}
	}

}

?>
