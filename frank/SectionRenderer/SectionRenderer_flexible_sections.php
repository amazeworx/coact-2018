<?php
namespace Frank\SectionRenderer;

use Frank\SectionFormatter\SectionFormatterInterface;
use Frank\SectionRenderer\SectionRenderer;
use Frank\SectionContract\SectionContract_flexible_sections;

/**
 * Class defining the flexible sections
 */
class SectionRenderer_flexible_sections extends SectionRenderer {

	use \Frank\Traits\DisplayDebugStripe;
	use \Frank\Traits\GetClassAcrossNamespaces;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_flexible_sections
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_flexible_sections';

	public function generateSection() {
		$this->displayDebugStripe('Start flexible sections');
		if ($this->formatter->hasSections()) {
			foreach ($this->formatter->getSections() as $section) {
				if (isset($section['acf_fc_layout'])) {
					$sectionRenderer = $this->getSectionRenderer($section['acf_fc_layout']);
					$sectionFormatter = $this->getFlexibleSectionFormatter($section['acf_fc_layout']);

					// Set the formatter's data
					$sectionData = $section;
					//remove the acf_fc_layout key
					unset($sectionData['acf_fc_layout']);

					// Initialise the section formatter
					$sectionFormatter->setPost($this->formatter->getPost());
					$sectionFormatter->setFlexibleSectionData($sectionData);
					$sectionFormatter->initialiseFlexibleData();

					// Set the section formatter
					$sectionRenderer->setSectionFormatter($sectionFormatter);

					$this->displayDebugStripe(get_class($sectionRenderer));
					// Generate the section
					$sectionRenderer->generateSection();
				}
			}
		}
		$this->displayDebugStripe('End flexible sections');
	}

	/**
	 * Get the section rendering class
	 *
	 * Will first check if there is a rendering class in app then in frank.
	 * If none is found, will throw an error
	 *
	 * @throws \Exception If no section renderer class is found
	 * @param string The name of the renderer
	 * @return \Frank\SectionRenderer\SectionRendererInterface the section renderer
	 */
	protected function getSectionRenderer($rendererName) {
		if (!is_string($rendererName)) {
			throw new \Exception("Section renderer name not a string. Received {$rendererName}");
		}
		$partialClassName = "SectionRenderer\\SectionRenderer_{$rendererName}";

		$className = $this->getClassAcrossNamespaces($partialClassName);

		if (is_null($className)) {
			throw new \Exception("SectionRenderer {$partialClassName} not found.");
		}

		//Check if the class implements the SectionRendererInterface
		$interfaces = class_implements($className);
		if (isset($interfaces['Frank\SectionRenderer\SectionRendererInterface'])) {
			return new $className();
		} else {
			throw new \Exception("SectionRenderer {$className} doesn't implement Frank\SectionRenderer\SectionRendererInterface.");
		}
	}

	/**
	 * Get the flexible section formatter class
	 *
	 * Will first check if there is a formatter class in app then in frank.
	 * If none is found, will throw an error
	 *
	 * @throws \Exception If no section formateer class is found
	 * @param string The name of the formatter
	 * @return \Frank\SectionFormatter\SectionFormatterInterface the section formatter
	 */
	protected function getFlexibleSectionFormatter($formatterName) {
		if (!is_string($formatterName)) {
			throw new \Exception("Section formatter name not a string. Received {$formatterName}");
		}
		$partialClassName = "SectionFormatter\\SectionFormatter_{$formatterName}";

		$className = $this->getClassAcrossNamespaces($partialClassName);

		if (is_null($className)) {
			throw new \Exception("SectionFormatter {$partialClassName} not found.");
		}

		//Check if the class implements the SectionFormatterInterface
		$interfaces = class_implements($className);
		if (isset($interfaces['Frank\SectionFormatter\SectionFormatterInterface'])) {
			return new $className();
		} else {
			throw new \Exception("SectionFormatter {$className} doesn't implement Frank\SectionFormatter\SectionFormatterInterface.");
		}
	}

}

?>
