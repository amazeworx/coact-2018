<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\SectionContract\SectionContract_introduction;

/**
 * Class defining a section renderer for an introduction section
 */
class SectionRenderer_introduction extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_introduction
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_introduction';

	public function generateSection() {
		?>
		<?php if ($this->formatter->hasContent()) : ?>
			<div class="introduction">
				<div class="introduction-content">
					<?php echo $this->formatter->getContent();  ?>
				</div>
				<?php if ($this->formatter->hasButtons()) : ?>
					<div class="introduction-buttons">
						<?php
						foreach ($this->formatter->getButtons() as $buttonData) {
							$this->generateButton($buttonData);
						}
						?>
					</div>
				</div>
			<?php endif; ?>
		<?php endif; ?>
		<?php
	}

}

?>
