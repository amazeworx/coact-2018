<?php
namespace Frank\SectionRenderer;

interface SectionRendererInterface {

	/**
	 * Set the data formatter for the section
	 *
	 * @param \Frank\SectionFormatter\SectionFormatterInterface $dataFormatter the section data formatter
	 */
	public function setSectionFormatter(\Frank\SectionFormatter\SectionFormatterInterface $dataFormatter);

	/**
	 * Render a sections of the page
	 *
	 * Return the section content as a string
	 *
	 * @return string|null the section content or null if no content
	 */
	public function render();
}

?>
