<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\SectionContract\SectionContract_image_and_content;

/**
 * Class defining a section renderer
 */
class SectionRenderer_image_and_content extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_image_and__content
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_image_and_content';

	public function generateSection() {
		?>
		<div class="page_content">
			<?php echo $this->formatter->getAlignment(); ?>
			<?php if ($this->formatter->hasImage()) : ?>
				<?php echo $this->formatter->getImage(); ?>
			<?php endif; ?>
			<?php if ($this->formatter->hasTitle()) : ?>
				<?php echo $this->formatter->getTitle(); ?>
			<?php endif; ?>
			<?php if ($this->formatter->hasContent()) : ?>
				<?php echo $this->formatter->getContent(); ?>
			<?php endif; ?>
			<?php
			if ($this->formatter->hasButtons()) {
				foreach ($this->formatter->getButtons() as $buttonData) {
					$this->generateButton($buttonData);
				}
			}
			?>
		</div>
		<?php
	}

}

?>
