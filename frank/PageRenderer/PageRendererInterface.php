<?php
namespace Frank\PageRenderer;

interface PageRendererInterface {

	/**
	 * Set if the page renderer should be configured for a single page or a listing page
	 *
	 * @param boolean $isSingular true if a single page, false otherwise
	 *
	 * @return void
	 */
	public function setSingular($isSingular);

	/**
	 * Set if the page is a 404
	 *
	 * @param boolean $is404 true if a 404 page
	 *
	 * @return void
	 */
	public function setIs404($is404);

	/**
	 * Add a post to the renderer
	 *
	 * @param \WP_Post $post The post to add to the renderer
	 *
	 * @return void
	 */
	public function addPost(\WP_Post $post);

	/**
	 * Render the page sections
	 *
	 * Output the string
	 *
	 * @throws Exception if no TemplateRenderer class is found for the post
	 * @throws Exception if the TemplateRenderer class doesn't implement the TemplateRendererInterface
	 * @return void
	 */
	public function render();
}

?>
