<?php
namespace Frank\PageRenderer;

use Frank\PageRenderer\PageRendererInterface;
/**
* Dynamically pick the correct page template to render, based on the post type
*/
class PageRenderer implements PageRendererInterface {

	use \Frank\Traits\FormatPageTemplate;
	use \Frank\Traits\GetClassAcrossNamespaces;

	/**
	 * The template renderer to use in order to display the page
	 *
	 * @var Frank\TemplateRenderer\TemplateRendererInterface the template renderer
	 */
	protected $templateRenderer = null;

	/**
	 * Determine if the page is a single page or a listing page
	 *
	 * @var boolean
	 */
	protected $isSingular = true;
	/**
	 * Determine if the page is a 404
	 *
	 * @var boolean
	 */
	protected $is404 = false;

	/**
	 * The list of posts to process
	 *
	 * @var array<WP_Post>
	 */
	protected $posts = [];

	public function setSingular($isSingular) {
		$this->isSingular = !!($isSingular);
	}

	public function setIs404($is404) {
		$this->is404 = !!($is404);
	}

	public function setTemplateRenderer(\Frank\TemplateRenderer\TemplateRendererInterface $templateRenderer) {
		$this->templateRenderer = $templateRenderer;
	}

	public function addPost(\WP_Post $post) {
		$this->posts[] = $post;
	}

	public function render() {

		if ($this->is404) {
			// Handle the 404 template case
			$template404 =  $this->getClassAcrossNamespaces('TemplateRenderer\TemplateRenderer_404');
			$this->templateRenderer = new $template404();
		} else if ($this->isSingular) {
			if (count($this->posts) > 0) {
				// Automatically assign a template renderer when singular page
				$className = defined('APP_TEMPLATE_RENDERER') ? APP_TEMPLATE_RENDERER : $this->getTemplateRendererClass($this->posts[0]);

				//Check if the class implements the TemplateRendererInterface
				$interfaces = class_implements($className);
				if (isset($interfaces['Frank\TemplateRenderer\TemplateRendererInterface'])) {
					$this->templateRenderer = new $className($this->posts[0]);
				}else{
					throw new \Exception("TemplateRenderer {$className} doesn't implement Frank\TemplateRenderer\TemplateRendererInterface.");
				}
			} else {
				throw new \Exception("No post available for PageRenderer.");
			}
		} else {
			// Check if a template renderer is set, otherwise, assign one automatically
			if (defined('APP_ARCHIVE_TEMPLATE_RENDERER')) {
				$className = APP_ARCHIVE_TEMPLATE_RENDERER;
				$interfaces = class_implements($className);
				if (isset($interfaces['Frank\TemplateRenderer\TemplateRendererInterface'])) {
					$this->templateRenderer = new $className();
				}else{
					throw new \Exception("TemplateRenderer {$className} doesn't implement Frank\TemplateRenderer\TemplateRendererInterface.");
				}
			} else if (is_null($this->templateRenderer)) {
				$this->templateRenderer = new \Frank\TemplateRenderer\TemplateRenderer_archive_default();
			}
		}

		if ($this->templateRenderer) {
			$result = $this->templateRenderer->render();
			echo $result;
		} else {
			throw new \Exception("No TemplateRenderer available for PageRenderer.");
		}
	}

	/**
	 * Get the template rendering class
	 *
	 * Will first check if there is a rendering class in app then in frank.
	 * If none is found, will throw an error
	 *
	 * @throws \Exception If no template renderer class is found
	 * @param \WP_Post the post
	 * @return string the class
	 */
	protected function getTemplateRendererClass(\WP_Post $post) {

		$postType = str_replace('-', '_', $post->post_type);
		$templateName = $this->formatPageTemplate($post->ID);
		$partialClassName = "TemplateRenderer\\TemplateRenderer_{$postType}_{$templateName}";

		$className = $this->getClassAcrossNamespaces($partialClassName);

		if (is_null($className)) {
			throw new \Exception("TemplateRenderer {$partialClassName} type not found.");
		}

		return $className;
	}

}

?>
