<?php
namespace Frank\TileRenderer;

use Frank\TileRenderer\TileRendererInterface;

/**
 * Dynamically use a tile renderer based on the post type
 */
class TileRendererFactory implements TileRendererInterface {

	use \Frank\Traits\GetClassAcrossNamespaces;

	/**
	 * The post the tile should be generated from
	 *
	 * @var WP_Post|null
	 */
	protected $post = null;

	public function setPost(\WP_Post $post = null) {
		$this->post = $post;
	}

	public function render() {
		// Try to get the correct tile renderer
		if ($this->post) {
			// Automatically assign a template renderer when singular page
			$className = $this->getTileRendererClass($this->post);

			//Check if the class implements the TemplateRendererInterface
			$interfaces = class_implements($className);
			if (isset($interfaces['Frank\TileRenderer\TileRendererInterface'])) {
				$tileRenderer = new $className();
				$tileRenderer->setPost($this->post);
				$tileRenderer->render();
			}else{
				throw new \Exception("TileRenderer {$className} doesn't implement Frank\TileRenderer\TileRendererInterface.");
			}
		} else {
			throw new \Exception("No post available for TileRendererFactory.");
		}
	}

	/**
	 * Get the tile rendering class
	 *
	 * Will first check if there is a rendering class in app then in frank.
	 * If none is found, will throw an error
	 *
	 * @throws \Exception If no tile renderer class is found
	 * @param \WP_Post the post
	 * @return string the class
	 */
	protected function getTileRendererClass(\WP_Post $post) {

		$postType = str_replace('-', '_', $post->post_type);
		$partialClassName = "TileRenderer\\TileRenderer_{$postType}";

		$className = $this->getClassAcrossNamespaces($partialClassName);

		if (is_null($className)) {
			throw new \Exception("TileRenderer {$partialClassName} type not found.");
		}

		return $className;
	}
}

?>
