<?php
namespace Frank\TileRenderer;

interface TileRendererInterface {

	/**
	 * Set the post for the tile
	 *
	 * @param \WP_Post|null the post
	 */
	public function setPost(\WP_Post $post = null);

	/**
	 * Render a post tile
	 *
	 * Output the tile html
	 *
	 * @throws Exception if no TileRenderer class is found for the post
	 * @throws Exception if the TileRenderer class doesn't implement the TileRendererInterface
	 * @return void
	 */
	public function render();
}

?>
