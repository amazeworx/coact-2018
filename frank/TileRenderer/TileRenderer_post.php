<?php
namespace Frank\TileRenderer;

use Frank\TileRenderer\TileRenderer;

/**
 * Render the tile for a specific post
 */
class TileRenderer_post extends TileRenderer {

	public function render() {
		if ($this->post) {
			?>
			<div class="tile_post">
				<h2>
					<a href="<?php echo get_permalink($this->post->ID); ?>">
						<span svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/misc-frank.svg"></span>
						<span><?php echo $this->post->post_title; ?></span>
					</a>
				</h2>
				<p><?php echo $this->getExcerpt(); ?></p>
				<example></example>
			</div>
			<?php
		} else {
			throw new \Exception("No post available for TileRenderer_post.");
		}
	}

	protected function getExcerpt() {
		$excerpt = $this->post->post_excerpt;

		if (!$excerpt) {
			$excerpt = $this->post->post_content;
		}

		// Trim the number of words
		$excerpt = wp_trim_words($excerpt, 20);

		return wp_strip_all_tags(apply_filters('get_the_excerpt', $excerpt, $this->post), true);
	}

}

?>
