<?php
namespace Frank\TileRenderer;

use Frank\TileRenderer\TileRendererInterface;

/**
 * Abstract class defining a tile renderer
 *
 * Provides pre-defined methods to allow faster tile renderer creation
 */
abstract class TileRenderer implements TileRendererInterface {

	/**
	 * The post the tile should be generated from
	 *
	 * @var WP_Post|null
	 */
	protected $post = null;

	public function setPost(\WP_Post $post = null) {
		$this->post = $post;
	}

}

?>
