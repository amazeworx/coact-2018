<?php
namespace Frank\SectionFormatter;

interface SectionFormatterInterface {

	/**
	 * Set the post for the section
	 *
	 * @param \WP_Post|null the post
	 */
	public function setPost(\WP_Post $post = null);

	/**
	 * Get the post for the section
	 *
	 * @return \WP_Post|null the post
	 */
	public function getPost();

	/**
	 * Initialise the data for the formatter in the case of a static section of the template
	 *
	 */
	public function initialiseData();

	/**
	 * Initialise the data for the formatter in the case of a flexible section
	 *
	 */
	public function initialiseFlexibleData();

	/**
	 * Set the flexible section's data
	 *
	 * @param array the section's data
	 */
	public function setFlexibleSectionData(array $sectionData);

	/**
	 * Determine if the section should be displayed or not
	 *
	 * @return boolean true if the section should be displayed
	 */
	public function shouldDisplaySection();

}

?>
