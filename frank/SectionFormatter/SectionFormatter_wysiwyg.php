<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_wysiwyg;

/**
 * Class defining a section renderer
 */
class SectionFormatter_wysiwyg extends SectionFormatter implements SectionContract_wysiwyg {

	/**
	 * HTML string representing the content output from the WYSIWYG editor
	 *
	 * @var string
	 */
	protected $content = null;

	public function initialiseData() {
		$this->content = $this->post->post_content;
	}

	public function initialiseFlexibleData() {
		$this->content = $this->getFlexibleSectionDataByKey('content');
	}

	public function hasContent() {
		return !!($this->content);
	}

	public function getContent() {
		return $this->content;
	}

}

?>
