<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_flexible_sections;


/**
 * Class defining the flexible sections
 */
class SectionFormatter_flexible_sections extends SectionFormatter implements SectionContract_flexible_sections {

	/**
	 * The flexible sections
	 */
	protected $sections = null;

	public function initialiseData() {
		$this->sections = get_field('flexible_sections', $this->post->ID);
	}

	public function hasSections() {
		return ($this->sections && count($this->sections));
	}

	public function getSections() {
		return $this->sections;
	}
}

?>
