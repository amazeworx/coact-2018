<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_archive_content_default;

/**
 * Class defining a default archive content
 */
class SectionFormatter_archive_content_default extends SectionFormatter implements SectionContract_archive_content_default {

	/**
	 * The posts present in the archive
	 *
	 * @var array<WP_Post>
	 */
	protected $posts = null;
	/**
	 * The pagination data
	 *
	 * @var string|null
	 */
	protected $pagination = null;


	public function initialiseData() {
		// get the posts from the Wordpress loop and fetch them
		global $post;
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				$this->posts[] = $post;
			}
		}

		// Use the default Wordpress posts pagination
		$this->pagination = paginate_links(array());
	}

	public function hasPosts() {
		return ($this->posts && count($this->posts));
	}

	public function getPosts() {
		return $this->posts;
	}

	public function hasPagination() {
		return !!($this->pagination);
	}

	public function getPagination() {
		return $this->pagination;
	}

}

?>
