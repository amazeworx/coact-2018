<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_cta;

/**
* Class defining a section renderer
*/
class SectionFormatter_cta extends SectionFormatter implements SectionContract_cta {

	protected $title = null;
	protected $description = null;
	protected $buttons = null;

	public function initialiseData() {
		$this->buttons     = get_field('buttons', $this->post->ID);
		$this->description = get_field('cta_description', $this->post->ID);
		$this->title       = get_field('cta_title', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->title       = $this->getFlexibleSectionDataByKey('cta_title');
		$this->description = $this->getFlexibleSectionDataByKey('cta_description');
		$this->buttons     = $this->getFlexibleSectionDataByKey('buttons');
	}

	public function hasTitle() {
		return !!($this->title);
	}

	public function getTitle() {
		return $this->title;
	}

	public function hasDescription() {
		return !!($this->description);
	}

	public function getDescription() {
		return $this->description;
	}

	public function hasButtons() {
		return !!($this->buttons);
	}

	public function getButtons() {
		return $this->buttons;
	}

}

?>
