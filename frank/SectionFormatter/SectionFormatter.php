<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Abstract class defining a section formatter
 *
 * Provides pre-defined methods to allow faster section formatter creation
 */
abstract class SectionFormatter implements SectionFormatterInterface {

	/**
	 * The post
	 *
	 * @var \WP_Post
	 */
	protected $post = null;

	/**
	 * The flexible section data
	 *
	 * @var array
	 */
	protected $flexibleSectionData = null;

	/**
	 * Defines if the section is currently a flexible section or not
	 *
	 * @var boolean
	 */
	protected $isFlexibleSection = false;

	/**
	 * Get the post for the section
	 *
	 * @return \WP_Post|null the post
	 */
	public function getPost() {
		return $this->post;
	}

	/**
	 * Set the post for the section
	 *
	 * @param \WP_Post the post
	 */
	public function setPost(\WP_Post $post = null) {
		$this->post = $post;
	}

	public function initialiseData() {
		$className = get_class($this);
		// Throw error, not intended to be a static section
		throw new \Exception("{$className} not intended to be a static section.");
	}

	/**
	 * Set the flexible section's data
	 *
	 * @param array the flexible section's data
	 */
	public function setFlexibleSectionData(array $flexibleSectionData) {
		// Automatically set the section as flexible if the flexible section data is initialised
		$this->isFlexibleSection = true;

		$this->flexibleSectionData = $flexibleSectionData;
	}

	public function initialiseFlexibleData() {
		$className = get_class($this);
		// Throw error, not intended to be a flexible section
		throw new \Exception("{$className} not intended to be a flexible section.");
	}

	/**
	 * Get a section data property or null if the property doesn't exists
	 *
	 * @param string $key the section data key to get
	 * @return mixed|null
	 */
	protected function getFlexibleSectionDataByKey($key) {
		return (isset($this->flexibleSectionData[$key]))? $this->flexibleSectionData[$key] : null;
	}

	/**
	 * Determine if the section should be displayed or not
	 *
	 * @return boolean true if the section should be displayed
	 */
	public function shouldDisplaySection() {
		return true;
	}

}

?>
