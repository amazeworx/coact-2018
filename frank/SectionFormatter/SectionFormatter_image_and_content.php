<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_image_and_content;

/**
 * Class defining a section renderer
 */
class SectionFormatter_image_and_content extends SectionFormatter implements SectionContract_image_and_content {

	protected $alignment = null;
	protected $image = null;
	protected $title = null;
	protected $content = null;
	protected $buttons = null;

	public function initialiseData() {
		$this->$alignment = get_field('image_and_content_alignment', $this->post->ID);
		$this->$image     = get_field('image_and_content_image', $this->post->ID);
		$this->$title     = get_field('image_and_content_title', $this->post->ID);
		$this->$content   = get_field('image_and_content_content', $this->post->ID);
		$this->$buttons   = get_field('buttons', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->alignment = $this->getFlexibleSectionDataByKey('image_and_content_alignment');
		$this->image     = $this->getFlexibleSectionDataByKey('image_and_content_image');
		$this->title     = $this->getFlexibleSectionDataByKey('image_and_content_title');
		$this->content   = $this->getFlexibleSectionDataByKey('image_and_content_content');
		$this->buttons   = $this->getFlexibleSectionDataByKey('buttons');
	}

	public function getAlignment() {
		return $this->alignment;
	}

	public function hasImage() {
		return !!($this->image);
	}

	public function getImage() {
		return $this->image;
	}

	public function hasTitle() {
		return !!($this->title);
	}

	public function getTitle() {
		return $this->title;
	}

	public function hasContent() {
		return !!($this->content);
	}

	public function getContent() {
		return $this->content;
	}

	public function hasButtons() {
		return !!($this->buttons);
	}

	public function getButtons() {
		return $this->buttons;
	}

}

?>
