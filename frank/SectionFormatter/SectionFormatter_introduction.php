<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_introduction;
/**
* Class defining a section renderer
*/
class SectionFormatter_introduction extends SectionFormatter implements SectionContract_introduction {
	/**
	 * HTML string representing the content output from the WYSIWYG editor
	 *
	 * @var string
	 */
	protected $content = null;
	/**
	 * HArray of buttons
	 *
	 * @var array
	 */
	protected $buttons = null;

	public function initialiseData() {
		$this->content = get_field('introduction_content', $this->post->ID);
		$this->buttons = get_field('buttons', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->content = $this->getFlexibleSectionDataByKey('introduction_content');
		$this->buttons = $this->getFlexibleSectionDataByKey('buttons');
	}

	public function hasContent() {
		return !!($this->content);
	}

	public function getContent() {
		return $this->content;
	}

	public function hasButtons() {
		return !!($this->buttons);
	}

	public function getButtons() {
		return $this->buttons;
	}
}

?>
