<?php
namespace Frank\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_hero_default;

/**
* Class defining a default hero
* https://projects.invisionapp.com/d/main#/console/11103433/234885701/preview
*/
class SectionFormatter_hero_default extends SectionFormatter implements SectionContract_hero_default {

	protected $title = null;
	protected $backgroundImage = null;

	public function initialiseData() {
		$this->title           = $this->post->post_title;
		$this->backgroundImage = get_field('hero_background', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->title           = $this->post->post_title;
		$this->backgroundImage = $this->getFlexibleSectionDataByKey('hero_background');
	}

	public function hasTitle() {
		return !!($this->title);
	}

	public function getTitle() {
		return $this->title;
	}

	public function hasBackgroundImage() {
		return !!($this->backgroundImage);
	}

	public function getBackgroundImage() {
		return $this->backgroundImage;
	}

}

?>
