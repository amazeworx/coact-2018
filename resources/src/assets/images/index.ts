/**
 * This file declares all the SVG icons that will be used in the project (alphabetically ordered)
 *
 * Please note: If the icon isn't declared in this file, webpack won't process it
 */

import './common-coact_logo_employment.svg';
import './common-location-purple.png';
import './bluedot48.png';
