/**
 * This file declares all the SVG icons that will be used in the project (alphabetically ordered)
 *
 * Please note: If the icon isn't declared in this file, webpack won't process it
 */

import './article-header-icons/article-header-facebook-icon.svg';
import './article-header-icons/article-header-link-icon.svg';
import './article-header-icons/article-header-linkedin-icon.svg';
import './article-header-icons/article-header-mail-icon.svg';
import './article-header-icons/article-header-twitter-icon.svg';
import './common-arrow.svg';
import './common-checkbox-selected.svg';
import './common-checkmark_tick.svg';
import './common-clock-white.svg';
import './common-close-quotation.svg';
import './common-close.svg';
import './common-cluster.png';
import './common-cluster.svg';
import './common-dropdown.svg';
import './common-expand-purple.svg';
import './common-expand.svg';
import './common-hamburger.svg';
import './common-location-purple.svg';
import './common-location-white.svg';
import './common-location.svg';
import './common-next-arrow.svg';
import './common-next.svg';
import './common-open-quotation.svg';
import './common-phone-white.svg';
import './common-phone.svg';
import './common-previous-arrow.svg';
import './common-radio-checked.svg';
import './common-radio-unchecked.svg';
import './common-search.svg';
import './common-share.svg';
import './common-tick.svg';
import './misc-coact.svg';
import './misc-frank.svg';
import './misc-shape-one.svg';
import './misc-shape-three.svg';
import './misc-shape-two.svg';
import './social-facebook.svg';
import './social-instagram.svg';
import './social-linkedin.svg';
import './social-twitter.svg';
import './social-youtube.svg';
