/**
 * This file declares all the fonts that will be used in the project as well as use webfontloader to load them
 *
 * Please note: If the font isn't declared in this file, webpack won't process it
 */
import * as WebFont from 'webfontloader';
import './assets/fonts/fonts.scss';

// Loading fonts example
WebFont.load({
	google: {
		families: ['Montserrat:400,500,700'],
	},
});
