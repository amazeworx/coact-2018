// App assets
import './assets/css/styles.scss';
import './assets/icons';
import './assets/images';

// The javascript application
import './app/app.tsx';
