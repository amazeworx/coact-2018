interface IWebsiteData {
	/** Url of the website */
	urlWebsite: string;
	/** Url of the theme */
	urlTheme: string;
	/** Url to query the posts via AJAX */
	urlFiltersAjax?: string;
	/** Url to current page */
	urlPage?: string;
	/** Currently queried page */
	currentPage?: number;
	/** Total number of pages */
	totalPages?: number;
	/** Filters to list brands */
	brandFilters: any[];
	/** Data to dsiplay graphs */
	graphs: any[];
	/** Data to dsiplay service partners */
	serviceTypes: string;
	/** Data to dsiplay service partners */
	servicePartners: string;
	/** Google Maps API Key */
	googleMapsApi: string;
	/** the user IP */
	userIP: string;
}

interface IAlgoliaData {
	/** The algolia application ID */
	application_id: string;
	/** The search API key */
	search_api_key: string;
	/** The search API key */
	indices: any;
}

/** Global variable to pass backend vars to javascript */
declare const websiteData: IWebsiteData;

/** Global variable to pass algolia data */
declare const algolia: IAlgoliaData;

declare const instantsearch: any;
declare const wp: any;
declare const _: any;

/** Modules declarations */
declare module 'react-svg';
