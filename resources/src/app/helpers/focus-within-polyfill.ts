// Focus Within polyfill
// Inspired by https://gist.github.com/aFarkas/a7e0d85450f323d5e164
((window, document) => {
	'use strict';
	let previousFocusWithinStack: Element[] = [];
	const slice = [].slice;

	interface IStackDiff {
		/** The new elements stack */
		newStack: Element[];
		/** The elements removed from the stack */
		removedStackElements: Element[];
		/** The elements added in the stack */
		addedStackElements: Element[];
	}

	function stackDiff(currentElement: Element, previousStack: Element[]): IStackDiff {
		// Build the new stack
		const newStack: Element[] = [];
		let element = currentElement;
		while (element && element.classList) {
			newStack.push(element);
			element = element.parentNode as Element;
		}
		// Reverse the stacks and find the index where both are different
		const reversePreviousStack = previousStack.slice().reverse();
		const reverseNewStack = newStack.slice().reverse();
		const minIndex = Math.min(reversePreviousStack.length, reverseNewStack.length);
		let index = 0;

		// Go through the elements until finding a diff in the stack
		for (index = 0; index < minIndex; index++) {
			if (reversePreviousStack[index] !== reverseNewStack[index]) {
				break;
			}
		}

		return {
			addedStackElements: reverseNewStack.slice(index, reverseNewStack.length),
			newStack,
			removedStackElements: reversePreviousStack.slice(index, reversePreviousStack.length),
		};
	}

	function removeClass(elem: Element) {
		elem.classList.remove('focus-within');
	}
	function addClass(elem: Element) {
		elem.classList.add('focus-within');
	}

	const update = (() => {
		let running: boolean;
		let last: Element;
		const action = () => {
			const element = document.activeElement;
			running = false;
			if (last !== element) {
				last = element;
				// Perform a difference of the elements
				const currentStackDiff = stackDiff(element, previousFocusWithinStack);

				// Update the classes
				currentStackDiff.addedStackElements.forEach(addClass);
				currentStackDiff.removedStackElements.forEach(removeClass);

				// Update the focus within stack
				previousFocusWithinStack = currentStackDiff.newStack;
			}
		};
		return () => {
			if (!running) {
				requestAnimationFrame(action);
				running = true;
			}
		};
	})();
	document.addEventListener('focus', update, true);
	document.addEventListener('blur', update, true);
	update();
})(window, document);
