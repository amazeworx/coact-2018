import './helpers/focus-within-polyfill';
import './helpers/object-assign-polyfill.js';

// Javascript/React Logic
import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as smoothscroll from 'smoothscroll-polyfill';

// The react renderer
import { ReactRenderersManager } from './frank-react-renderer';

// The components (alphabetically ordered)
import { AccessibiltyModuleRenderer }     from './components/accessibility-module';
import { TestimonialSliderRenderer }      from './components/coact-testimonials';
import {
	CategorySelectRenderer
} from './components/contenthub-categories-select/contenthub-categories-select.renderer';

import { FormSelectRenderer }             from './components/form-select';
import { LocationMarkerRenderer }         from './components/location-marker';
import { LockedinHeaderRenderer }         from './components/lockedin-header';
import { MenuButtonRenderer }             from './components/menu-button/menu-button.renderer';
import { MenuSearchRenderer }             from './components/menu-search/';
import { MobileMenuRenderer }             from './components/mobile-menu';
import { NumberTickerRenderer }           from './components/number-ticker';
import { PromotionalCtaRenderer }        from './components/promotional-cta';
import { SearchResultsRenderer }          from './components/search-results';
import { ServiceLocatorRenderer }         from './components/service-locator/service-locator.renderer';
import { SubscribeCtaRenderer }           from './components/subscribe-cta';
import { SvgIconRenderer }                from './components/svg-icon';

/**
 * Declares all the React renderer classes
 */
const renderers = [
	AccessibiltyModuleRenderer,
	LockedinHeaderRenderer,
	CategorySelectRenderer,
	LocationMarkerRenderer,
	FormSelectRenderer,
	MenuButtonRenderer,
	MenuSearchRenderer,
	MobileMenuRenderer,
	NumberTickerRenderer,
	PromotionalCtaRenderer,
	SearchResultsRenderer,
	ServiceLocatorRenderer,
	SubscribeCtaRenderer,
	SvgIconRenderer,
	TestimonialSliderRenderer,
];

const reactRenderers = new ReactRenderersManager();

// kick of the polyfills
smoothscroll.polyfill();
// console.log(smoothscroll)

// Initialise the renderers
renderers.forEach((renderingClass) => {
	const renderer = new renderingClass();
	reactRenderers.addRenderer(renderer);
});

reactRenderers.bootstrap();

// Fades out contact form when the form has submitted siccesfullyw
const fadeOutElement = (el) => {
	el.style.opacity = 1;

	const fadeOut = () => {
		// tslint:disable-next-line
		if (el.style.opacity = 1) {
			el.style.display = 'none';
		}
	};

	fadeOut();
};

// Listen to the contact form 7 submit event
document.addEventListener( 'wpcf7mailsent', (event: any) => {
	// IDs on staging sites that need the action below

	// Local (Sam): 1089
	// staging staging.coact.org.au : 131
	// production coact.org.au : 131

	const contactFormElement = document.querySelector('.contact_form');
	const contactFormDetailElement = document.querySelector('.subscribe_section-detail');
	const contactFormTitleElement = document.querySelector('.subscribe_section-title');
	const successResponse = document.querySelector('.subscribe_section-title');

	if (
		event.detail.contactFormId === '1089' ||
		event.detail.contactFormId === '131'
	) {
		fadeOutElement(contactFormElement);
		fadeOutElement(contactFormDetailElement);
		fadeOutElement(contactFormTitleElement);
	} else {
		fadeOutElement(contactFormTitleElement);
	}
}, false);

// If you are el has parent name in it
const checkUrlForParent = () => {
	const pathNameKeys = [
		'individuals',
		'employers',
		'service-partners',
		'referral-partners',
		'about-us',
		'innovations',
	];

	const pathName = location.pathname;

	const containsUrl = pathNameKeys.filter((name) => {
		const regex = new RegExp(name);
		const result = regex.test(pathName);

		return result;
	});

	if (containsUrl.length) {
		const filterResult = containsUrl[0].toLowerCase();
		const resultOfUrl = 'menu-item-' + filterResult;

		const elementToQuery = document.querySelector('.' + resultOfUrl);

		if (elementToQuery) {
			elementToQuery.classList.add('current-menu-item');
		}
	}
};

checkUrlForParent();

// Listen to a scroll top event from an iFrame
// iframeScrollTop
window.addEventListener('message', receiveMessage, false);

function receiveMessage(event) {
	if (event.data === 'iframeScrollTop') {
		// Select the iframe
		const element: HTMLElement = document.querySelector('#jobIframe');
		// Select the header
		const header: HTMLElement = document.querySelector('.header');
		window.scroll({
			behavior: 'smooth',
			left: 0,
			top: element.offsetTop - header.offsetHeight,
		});
	}

}