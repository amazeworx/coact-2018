import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';

const shortenContent = (str, maxLen, separator = ' ') => {
	if (str.length <= maxLen) {
		return str;
	}
	return str.substr(0, str.lastIndexOf(separator, maxLen));
};

export class SearchResultsRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'search-results';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {

		/* Instantiate instantsearch.js */
		const search = instantsearch({
			apiKey: algolia.search_api_key,
			appId: algolia.application_id,
			indexName: algolia.indices.searchable_posts.name,
			searchParameters: {
				facetingAfterDistinct: true,
				highlightPostTag: '__/ais-highlight__',
				highlightPreTag: '__ais-highlight__',
			},
			urlSync: {
				mapping: {
					q: 's',
				},
				trackedParameters: ['query'],
			},
		});

		/* Search box widget */
		search.addWidget(
			instantsearch.widgets.searchBox({
				container: '#search_form',
				placeholder: 'Search for...',
				poweredBy: false,
				wrapInput: false,
			})
		);

		/* Hits widget */
		search.addWidget(
			instantsearch.widgets.hits({
				container: '#algolia-hits',
				hitsPerPage: 10,
				templates: {
					empty: 'Sorry, we didn\'t find anything matching your search query',
					item: wp.template('instantsearch-hit'),
				},
				transformData: {
					item: (hit) => {
						function replace_highlights_recursive(item) {
							if (item instanceof Object && item.hasOwnProperty('value')) {
								item.value = _.escape(item.value);
								item.value = item.value.replace(/__ais-highlight__/g, '<em>').replace(/__\/ais-highlight__/g, '</em>');
							} else {
								for (const key in item) {
									if (item.hasOwnProperty(key)) {
										item[key] = replace_highlights_recursive(item[key]);
									}
								}
							}
							return item;
						}

						hit.content = shortenContent(hit.content, 180);

						hit._highlightResult = replace_highlights_recursive(hit._highlightResult);
						hit._snippetResult = replace_highlights_recursive(hit._snippetResult);

						return hit;
					},
				},
			})
		);

		/* Pagination widget */
		search.addWidget(
			instantsearch.widgets.pagination({
				container: '#algolia-pagination',
				labels: {
					first: 'First',
					last: 'Last',
					next: 'Next',
					previous: 'Prev',
				},
			})
		);

		search.addWidget(
			instantsearch.widgets.stats({
				container: '#stats-container',
				templates: {
					body: `Showing {{resultsOnPage}} of {{nbHits}} Results for {{query}}`,
				},
				transformData: {
					body: (data) => {
					  data.resultsOnPage = (data.nbHits < data.hitsPerPage ? data.nbHits : data.hitsPerPage );
					  return data;
					},
				},
			})
		);

		/* Start */
		search.start();

		// TODO: focus the search input

		// jQuery('#algolia-search-box input').attr('type', 'search').select();

		// ReactDOM.render(
		// 	<SearchResults />,
		// 	element
		// );
	}
}
