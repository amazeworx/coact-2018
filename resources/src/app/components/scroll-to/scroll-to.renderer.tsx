import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { ScrollTo } from './scroll-to.component';

export class ScrollToRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'scroll-to';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<ScrollTo />,
			element
		);

	}

}
