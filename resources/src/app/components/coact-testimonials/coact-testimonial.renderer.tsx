import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';
import {
	ITestimonialSliderProps,
	TestimonialSlider,
} from './coact-testimonial.component';

/**
 * Attributes expected for the component initialisation
 */

interface IAttributes {
	testimonialData: string;
}

export class TestimonialSliderRenderer extends ReactRenderer {
	protected _attributeSelector = 'testimonial-slider';
	protected _attributeProperties: Array<[string, string]> = [
		['testimonialData', 'testimonial-slider'],
	];

	public initReact(element: Element, attributes: ITestimonialSliderProps) {
		ReactDOM.render(
			<TestimonialSlider testimonialData={attributes.testimonialData}/>,
			element
		);
	}
}
