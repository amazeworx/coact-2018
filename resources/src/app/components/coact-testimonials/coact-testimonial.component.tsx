import * as React from 'react';
import Slider from 'react-slick';
import * as ReactSVG from 'react-svg';
import * as VisibilitySensor from 'react-visibility-sensor';

import 'slick-carousel/slick/slick-theme.css';
import 'slick-carousel/slick/slick.css';

export interface ITestimonialSliderProps {
	testimonialData: string;
}

interface IState {
	sliderData: any[];
}

import './coact-testimonial-slick-override.component.scss';
import './coact-testimonial.component.scss';

function SampleNextArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div className={className} >
			<span onClick={onClick} className="svg_icon next_arrow">
				<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-next-arrow.svg'} />
			</span>
		</div>
	);
}

function SamplePrevArrow(props) {
	const { className, style, onClick } = props;
	return (
		<div className={className} >
			<span onClick={onClick} className="svg_icon prev_arrow">
				<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-previous-arrow.svg'} />
			</span>
		</div>
	);
}

export class TestimonialSlider extends React.Component<ITestimonialSliderProps, IState> {

	constructor(props: ITestimonialSliderProps) {
		super(props);

		this.state = {
			sliderData: this.getSliderData(props.testimonialData),
		};
	}

	public render() {
		const carouselSettings = this.setSliderSettings();

		return (
			<div className="testimonial_slider-container">
				<Slider {...carouselSettings}>
				{this.state.sliderData.map((data, i) => {
					return (
						<div className="testimonial_slider-testimonail_container" key={i}>
							<div className="testimonial_slider-inner_container">
								<span className="svg_icon open_quotation">
									<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-open-quotation.svg'} />
								</span>

								<p className="testimonial_slider-testimonial_content">{data.testimonial_text}</p>
								<h4 className="testimonial_slider-testimonial_source" >{data.testimonial_source}</h4>

								<span className="svg_icon close_quotation">
									<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-close-quotation.svg'} />
								</span>
							</div>
						</div>
					);
				})}
				</Slider>
			</div>
		);
	}

	protected getSliderData(dataKey): any[]|null {
		if (websiteData.hasOwnProperty(dataKey)) {
			return websiteData[dataKey];
		}
		// tslint:disable-next-line
		console.warn(`Error while loading data for TestimonialSlider: Invalid data for ${dataKey}.`);
		return null;
	}

	protected setSliderSettings(): any {
		return {
			arrows: true,
			autoplay: true,
			autoplaySpeed: 15000,
			dots: true,
			infinite: false,
			nextArrow: <SampleNextArrow />,
			pauseOnFocus: true,
			pauseOnHover: true,
			prevArrow: <SamplePrevArrow />,
			slidesToScroll: 1,
			speed: 500,
		};
	}
}
