import * as _ from 'lodash';
import * as React from 'react';

import {
	GoogleMap,
	Marker,
	withGoogleMap,
	withScriptjs
} from 'react-google-maps';
import MarkerClusterer from 'react-google-maps/lib/components/addons/MarkerClusterer';
import { compose, lifecycle, withHandlers, withProps } from 'recompose';

import mapStyles from './service-locator-map-styles.js';
import './service-locator-map.component.scss';

const googleMapURL = [
	`https://maps.googleapis.com/maps/api/js?key=${websiteData.googleMapsApi}`,
	'&v=3.exp&libraries=geolocation,geometry,drawing,places',
].join('');

let map;
let clusters;

export default compose(
	// Disabling tslint here to allow properties to be unordered
	// tslint:disable
	withProps({
		googleMapURL,
		containerElement: <div style={{ height: `100%` }} />,
		loadingElement: <div style={{ height: `100%` }} />,
		mapElement: <div style={{ height: `100%` }} />,
	}),
	// tslint:enable
	lifecycle({
		componentDidUpdate(props, state) {
			if (map) {
				const newClusters = map.props.children.props.children;

				if (JSON.stringify(clusters) !== JSON.stringify(newClusters)) {
					clusters = newClusters;

					if (clusters.length) {
						this.state.zoomToMarkers();
					}
				}
			}
		},
		componentDidMount(props) {
			this.setState({
				initMap: (mapRef) => {
					if (!map) {
						map = mapRef;
					}
					this.state.zoomToMarkers();
				},
				onViewportUpdate: (cb) => {
					return () => {
						cb({
							lat: map.getCenter().lat(),
							lng: map.getCenter().lng(),
						});
					};
				},
				zoomToMarkers: () => {
					if (!map) {
						return;
					}
					const bounds = new google.maps.LatLngBounds();
					const markers = map.props.children.props.children;
					markers.forEach((child) => {
						if (child && child.type === Marker) {
							const marker = child.props;
							bounds.extend(new google.maps.LatLng(marker.position.lat, marker.position.lng));
						}
					});
					map.fitBounds(bounds);

					// Fix the issue with the markers not refreshing
					window.setTimeout(() => {
						map.fitBounds(bounds);
					}, 200);
				},
			});
		},
	}),
	withScriptjs,
	withGoogleMap
)((props) =>
	<GoogleMap
		ref={props.initMap}
		defaultZoom={1}
		defaultCenter={{ lat: parseFloat(props.userLatLng.lat), lng: parseFloat(props.userLatLng.lng) }}
		defaultOptions={{
			fullscreenControl: false,
			mapTypeControl: false,
			maxZoom: 16,
			minZoom: 1,
			rotateControl: false,
			streetViewControl: false,
			styles: mapStyles,
		}}
		onDragEnd={props.onViewportUpdate(props.onMapCenterUpdate)}
		onZoomChanged={props.onViewportUpdate(props.onMapCenterUpdate)}
	>
		<MarkerClusterer
			averageCenter
			enableRetinaIcons
			gridSize={60}
			imagePath={websiteData.urlTheme + '/assets/icons/common-cluster.svg'}
			styles={[{
				height: 40,
				textColor: 'white',
				textSize: 14,
				url: websiteData.urlTheme + '/assets/icons/common-cluster.png',
				width: 40,
			}]}
		>
			{props.partners.map((partner) => {
				if (partner.hasOwnProperty('_geoloc')) {
					return (
						<Marker
							key={partner.post_id}
							position={{ lat: parseFloat(partner._geoloc.lat), lng: parseFloat(partner._geoloc.lng) }}
							onClick={props.onSelect(partner)}
							icon={{ url: websiteData.urlTheme + '/assets/images/common-location-purple.png' }}
						/>
					);
				}
			})}
		</MarkerClusterer>
	</GoogleMap>
);
