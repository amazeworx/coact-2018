import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { MobileMenuSearch } from './mobile-menu-search.component';

export class MobileMenuSearchRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'menu-search';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<MobileMenuSearch />,
			element
		);
	}
}
