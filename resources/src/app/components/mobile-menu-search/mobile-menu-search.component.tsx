import * as React from 'react';
import * as ReactSVG from 'react-svg';

export class MobileMenuSearch extends React.Component<any, any> {
	constructor(props: any) {
		super(props);

		this.state = {
			searchQuery: '',
		};

		this.handleClick = this.handleClick.bind(this);
	}

	public render() {
		return (
			<div>
				<div className="search_box">
					<div className="search_box-container">
						<form onSubmit={this.submit}>
							<div className="search_box-search_query">
								<span><ReactSVG path={websiteData.urlTheme + '/assets/icons/common-search.svg'} /></span>
								<input
									onChange={this.addSearchValueToState}
									value={this.state.searchQuery}
									placeholder="What are you searching for?" />
							</div>

							<div className="header-get_in_touch_button">
								<button onClick={this.submit} type="" className="button button--primary">
									Search
								</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		);
	}

	protected handleClick = () => {
		this.setState({
			isSearchOpen: !this.state.isSearchOpen,
		});
	}

	protected addSearchValueToState = (event) => {
		this.setState({
			searchQuery: event.target.value,
		});
	}

	protected submit = (event) => {
		event.preventDefault();
		const searchQuery = `${websiteData.urlWebsite}/?s=${this.state.searchQuery}`;

		location.href = searchQuery;
	}
}
