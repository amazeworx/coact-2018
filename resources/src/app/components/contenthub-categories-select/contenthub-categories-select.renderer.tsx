import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { CategorySelect } from './contenthub-categories-select.component';

export class CategorySelectRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'category-select';
	protected _attributeProperties: Array<[string, string]> = [
		['uniqueParameter', 'unique-parameter'],
	];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<CategorySelect uniqueParameter={attributes.uniqueParameter}/>,
			element
		);
	}

}
