import * as React from 'react';

import Dropdown from 'react-dropdown';

import './contenthub-categories-select.component.scss';

const option = [
	{
		label: 'Filter articles by area of interest',
		value: 'Filter articles by area of interest',
	},
];

export class CategorySelect extends React.Component<any, any> {
	constructor(props) {
		super(props);

		const data = websiteData[props.uniqueParameter];

		this.state = {
			data,
		};
	}

	public onSelect(selection) {
		location.href = selection.value;
	}

	public render() {
		const defaultOption = option[0];
		return (
			<div className="">
				<Dropdown
					options={this.state.data}
					onChange={this.onSelect}
					value={defaultOption}
					placeholder="Filter articles by area of interest"/>
			</div>
		);
	}
}
