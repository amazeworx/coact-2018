import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';

import { FormSelect } from './form-select.component';

/**
 * Attributes expected for the component initialisation
 */

interface IAttributes {
	mapAddress: string;
	mapLat: string;
	mapLng: string;
}

function handleChange(data, select) {
	select.value = data.value;

	if ('createEvent' in document) {
		const evt = document.createEvent('HTMLEvents');
		evt.initEvent('change', false, true);
		select.dispatchEvent(evt);
	} else {
		select.fireEvent('onchange');
	}
}

export class FormSelectRenderer extends ReactRenderer {
	protected _attributeSelector = 'form-select';
	protected _attributeProperties: Array<[string, string]> = [
		['mapAddress', 'map-address'],
		['mapLat', 'map-lat'],
		['mapLng', 'map-lng'],
	];

	protected _select: HTMLSelectElement;

	public initReact(element: Element, attributes: IAttributes) {
		const select = this.querySelect(element);
		if (!select) {
			// tslint:disable-next-line
			console.warn(`Couldn't find select element to initialise:`, element);
			return;
		}

		const selectOptions = this.getSelectOptions(select);

		// Hide the select element
		select.classList.add('visually-hidden');

		ReactDOM.render(
			<FormSelect
				options={selectOptions.options}
				selected={selectOptions.selected}
				onChange={(data) => {
					handleChange(data, select);
				}}
			/>,
			element
		);
	}

	protected querySelect(element: Element) {
		const select = element.parentElement.querySelector('select');
		if (select) {
			return select as HTMLSelectElement;
		}
		return null;
	}

	protected getSelectOptions(select: HTMLSelectElement) {
		const result = [];
		let selected = null;
		if (select.options.length) {
			Array.prototype.forEach.call(select.options, (singleOption: HTMLOptionElement) => {
				result.push({
					label: singleOption.label,
					value: singleOption.value,
				});
				if (singleOption.selected) {
					selected = {
						label: singleOption.label,
						value: singleOption.value,
					};
				}
			});
		}

		return {
			options: result,
			selected,
		};
	}
}
