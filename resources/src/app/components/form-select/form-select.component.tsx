import * as React from 'react';
import Select from 'react-select';
import SelectStyles from '../coact-select/coact-select-styles';

// import './service-locator-search.component.scss';

interface IProps {
	options: any[];
	selected: any;

	onChange(data: any);
}

interface IState {
	selected: any;
}

export class FormSelect extends React.Component<IProps, IState> {
	constructor(props: any) {
		super(props);

		this.state = {
			selected: props.selected,
		};

		this.onChange = this.onChange.bind(this);
	}

	public render() {
		return (
			<div>
				<Select
					classNamePrefix="form_select"
					placeholder="Select a service type"
					isClearable={false}
					styles={SelectStyles}
					options={this.props.options}
					value={this.state.selected}
					onChange={this.onChange}
				/>
			</div>
		);
	}

	protected onChange(data) {
		this.setState({
			selected: data,
		});
		this.props.onChange(data);
	}
}
