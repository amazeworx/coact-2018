import * as React from 'react';
import * as ReactSVG from 'react-svg';

export class SubscribeCta extends React.Component<any, any> {
	protected wrapperRef = null;

	constructor(props: any) {
		super(props);

		const data = websiteData;

		this.state = {
			data,
			isCtaActive: false,
		};

		this.handleClick = this.handleClick.bind(this);
		this.onPageScroll = this.onPageScroll.bind(this);
	}

	public componentDidMount() {
		window.addEventListener('scroll', this.onPageScroll);
	}

	public render() {
		return (
			<div onClick={this.handleClick} className={'subscribe_cta-callout' + (this.state.isCtaActive ? ' active' : '')}>
				<ReactSVG path={`${this.state.data.urlTheme}/assets/icons/article-header-mail-icon.svg`} />
				<span>
					Subscribe
				</span>
			</div>
		);
	}

	protected handleClick() {
		const element = document.querySelector('.subscribe_section');

		element.scrollIntoView({behavior: 'smooth'});
	}

	protected onPageScroll() {
		const element = document.querySelector('.subscribe_section');
		const scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;

		if (this.isElementInView(element)) {
			this.setState({isCtaActive: false});
		} else {
			if (scrollTop > 98) {
				if (!this.state.isCtaActive) {
					this.setState({isCtaActive: true});
				}
			} else {
				if (this.state.isCtaActive) {
					this.setState({isCtaActive: false});
				}
			}
		}
	}

	protected isElementInView(element) {
		const rect = element.getBoundingClientRect();

		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
			rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
		);
	}
}
