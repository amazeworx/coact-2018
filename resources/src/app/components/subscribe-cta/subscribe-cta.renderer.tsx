import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';
import { SubscribeCta } from './subscribe-cta.component';

export class SubscribeCtaRenderer extends ReactRenderer {
	protected _attributeSelector: string = 'subscribe-cta';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<SubscribeCta />,
			element
		);
	}
}
