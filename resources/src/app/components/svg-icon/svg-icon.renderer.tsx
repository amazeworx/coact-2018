import * as React from 'react';
import * as ReactDOM from 'react-dom';
import * as ReactSVG from 'react-svg';

import { ReactRenderer } from '../../frank-react-renderer';

/**
 * Attributes expected for the component initialisation
 */
interface IAttributes {
	path: string;
}

export class SvgIconRenderer extends ReactRenderer {
	protected _attributeSelector = 'svg-icon';
	protected _attributeProperties: Array<[string, string]> = [
		['path', 'svg-icon'],
	];

	public initReact(element: Element, attributes: IAttributes) {
		ReactDOM.render(
			<div className="svg_icon msie11">
				<ReactSVG path={attributes.path} />
			</div>,
			element
		);
	}

}
