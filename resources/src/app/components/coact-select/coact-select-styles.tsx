
const inputStyles = {
	borderRadius: '2px',
	dropDownIconSize: '18px',
	fontSize: '12px',
};

const colors = {
	black: '#000000',
	lightGrey: 'rgba(116,116,118,0.2)',
	purple: '#B2519E',
	white: '#FFFFFF',
};

export default {
	control: (base, { isFocused }) => {
		return {
			...base,
			':hover': {
				borderColor: isFocused ? colors.purple : colors.lightGrey,
			},
			'backgroundColor': colors.white,
			'borderColor': isFocused ? colors.purple : colors.lightGrey,
			'borderRadius': inputStyles.borderRadius,
			'boxShadow': 'none',
			'fontSize': inputStyles.fontSize,
		};
	},
	dropdownIndicator: (base) => {
		return {
			...base,
			svg: {
				fill: colors.purple,
				height: inputStyles.dropDownIconSize,
				width: inputStyles.dropDownIconSize,
			},
		};
	},
	indicatorSeparator: () => {
		return {
			display: 'none',
		};
	},
	menuList: (base) => {
		return {
			...base,
		};
	},
	noOptionsMessage: (base) => {
		return {
			...base,
			fontSize: inputStyles.fontSize,
		};
	},
	option: (base, { isFocused }) => {
		return {
			backgroundColor: isFocused ? colors.purple : colors.white,
			color: isFocused ? colors.white : colors.black,
			fontSize: inputStyles.fontSize,
			padding: '10px 20px',
		};
	},
};
