import * as React from 'react';

const body = document.getElementsByTagName('body')[0];
const fontSizeFromWindow = window.getComputedStyle(body, null).getPropertyValue('font-size');
const fontSizeWindow = parseInt(fontSizeFromWindow.replace( /\D+/g, ''), 10);

// const minFont 100
// const maxFont 200
// const inc 20

const arrayPercentages = [
	100, 120, 140, 160, 180, 200,
];
let currentVal = arrayPercentages[0];

const fontSizeMap = {};
arrayPercentages.forEach(((val) => {
	fontSizeMap[val] = (val * fontSizeWindow) / 100;
}));

export default class AccessibiltyTextSize extends React.Component<any, any> {
	constructor(props) {
		super(props);

		this.state = {
			fontSize: fontSizeWindow,
			valueToChange: 3.2,
		};

		if (sessionStorage.fSize) {
			document.documentElement.style.fontSize = this.state.fontSize + 'px';
		}

		this.updateTextSize = this.updateTextSize.bind(this);
		this.getPercentage = this.getPercentage.bind(this);
	}

	public componentDidMount() {
		this.setState({
			fontSize: parseInt(sessionStorage.fSize, 10),
		});
	}

	public componentDidUpdate(prevState) {
		if (prevState.fontSize !== this.state.fontSize) {
			const sessionFontSize = this.state.fontSize;

			sessionStorage.setItem('fSize', sessionFontSize);
			document.documentElement.style.fontSize = this.state.fontSize + 'px';
		} else {
			sessionStorage.setItem('fSize', '16');
		}
	}

	public render() {
		return (
			<div>
				<button
					className="accessibilty_text_size-button accessibilty_text_size-button--decrease"
					onClick={this.updateTextSize('decrease')}>A-</button>
					<span className="accessibilty_text_size-percentage">{this.getPercentage()}%</span>
				<button
					className="accessibilty_text_size-button accessibilty_text_size-button--decrease"
					onClick={this.updateTextSize('increase')}>A+</button>
			</div>
		);
	}

	protected getPercentage() {
		return currentVal;
	}

	protected updateTextSize(op) {
		return () => {
			const idx = arrayPercentages.indexOf(currentVal);
			let nextIdx;

			if (op === 'increase') {
				nextIdx = idx + 1;
			} else if (op === 'decrease') {
				nextIdx = idx - 1;
			}

			if (nextIdx < 0) {
				return 0;
			} else if (nextIdx > arrayPercentages.length - 1) {
				return arrayPercentages.length - 1;
			}
			currentVal = arrayPercentages[nextIdx];
			this.setState({
				fontSize: fontSizeMap[currentVal],
			});
		};
	}
}
