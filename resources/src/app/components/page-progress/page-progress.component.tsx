import * as React from 'react';

interface IPageProgressProps {
}

interface IPageProgressState {
	progressStyle: {};
}

export class PageProgress extends React.Component<IPageProgressProps, IPageProgressState> {
	protected article: HTMLElement;

	constructor(props: any) {
		super(props);

		this.state = {
			progressStyle: {
				width: '0',
			},
		};

		this.onPageScroll = this.onPageScroll.bind(this);
	}

	public componentDidMount() {
		window.addEventListener('scroll', this.onPageScroll);
		this.article = document.querySelector('.js-article');
	}

	public onPageScroll() {
		const pageHeight = document.body.scrollHeight;
		const currentScrollHeight = (
			document.documentElement &&
			document.documentElement.scrollTop
		) || document.body.scrollTop;

		const windowHeight = Math.max(window.innerHeight, 0);

		const articleHeight = this.article.offsetTop + this.article.offsetHeight;

		let scrollPercentage = (currentScrollHeight / (articleHeight - windowHeight)) * 100;
		if (scrollPercentage <= 0) {
			scrollPercentage = 0;
		}
		if (scrollPercentage >= 100) {
			scrollPercentage = 100;
		}
		this.setState({progressStyle: {width: scrollPercentage + '%'}});

	}

	public render() {

		return (
			<div  className="page_progress">
				<div className="page_progress-bar" style={this.state.progressStyle}></div>
			</div>
		);
	}
}
