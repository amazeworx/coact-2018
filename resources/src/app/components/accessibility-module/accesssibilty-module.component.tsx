import * as React from 'react';
import Switch from 'react-toggle-switch';
import AccessibiltyTextSize from './../accessibilty-text-size/accessibility-text-size.component';

export class AccessibiltyModule extends React.Component<any, any> {
	protected localStorageKey = 'high-contrast';
	protected bodyClassHighContrast = 'high_contrast';

	constructor(props: any) {
		super(props);

		this.state = {
			switched: this.initHighContrast(),
		};
	}

	public render() {
		return (
			<div className="accesssibilty_module" tabIndex={0}>
				<div className="accesssibilty_module-container">
					<div className="accesssibilty_module-contrast">
						<span className="accesssibilty_module-title">High Contrast</span>

						<div className="accesssibilty_module-toggle">
							<Switch onClick={this.toggleSwitch} on={this.state.switched}/>
						</div>
					</div>

					<div className="accesssibilty_module-text_size">
						<span className="accesssibilty_module-title">Text Size</span>

						<AccessibiltyTextSize />
					</div>
				</div>
			</div>
		);
	}

	protected toggleSwitch = () => {
		this.updateHighContrast(!this.state.switched);
	}

	protected initHighContrast() {
		// get local storage value
		const switched = (
			window.localStorage.hasOwnProperty(this.localStorageKey) &&
			window.localStorage.getItem(this.localStorageKey) === '1'
		);

		this.handleContrastChange(switched);

		return switched;
	}

	protected updateHighContrast(newValue: boolean) {
		this.setState({
			switched: newValue,
		}, () => {
			this.handleContrastChange(this.state.switched);
		});
	}

	protected handleContrastChange(switched: boolean) {
		// Perform DOM modification
		if (switched) {
			document.body.classList.add(this.bodyClassHighContrast);
		} else {
			document.body.classList.remove(this.bodyClassHighContrast);
		}
		// Set local storage
		window.localStorage.setItem(this.localStorageKey, (switched) ? '1' : '0');
	}
}
