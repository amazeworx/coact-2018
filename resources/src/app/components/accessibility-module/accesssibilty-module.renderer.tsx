import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { AccessibiltyModule } from './accesssibilty-module.component';

export class AccessibiltyModuleRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'accessibilty-module';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<AccessibiltyModule />,
			element
		);
	}
}
