import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { MobileMenu } from './mobile-menu.component';

export interface IArticleHeaderProps {
	uniqueParameter: string;
}

export class MobileMenuRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'mobile-menu';
	protected _attributeProperties: Array<[string, string]> = [
		['uniqueParameter', 'unique-parameter'],
	];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<MobileMenu uniqueParameter={attributes.uniqueParameter} />,
			element
		);
	}
}
