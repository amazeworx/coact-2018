import * as React from 'react';
import * as ReactSVG from 'react-svg';

interface IProps {
	item: any;
	key: number;
}

interface IState {
	isOpen: boolean;
}

export class MobileMenuItem extends React.Component<IProps, IState> {
	constructor(props: IProps) {
		super(props);

		this.state = {
			isOpen: false,
		};

		this.handleClick = this.handleClick.bind(this);
	}

	public render() {
		const itemClasses = [
			'mobile_menu_item',
		];
		if (this.state.isOpen) {
			itemClasses.push('mobile_menu_item--open');
		} else {
			itemClasses.push('mobile_menu_item--closed');
		}

		return (
			<li key={this.props.key} className={itemClasses.join(' ')}>
				<span>
					<a href={this.props.item.url}>{this.props.item.title}</a>

				{this.props.item.children ? (
					<button
						onClick={this.handleClick}
					>
						<ReactSVG path={`${websiteData.urlTheme}/assets/icons/common-expand.svg`} />
					</button>
				) : (
					''
				)}
				</span>
				{
					this.props.item.children &&
					this.state.isOpen &&
					<div className="submenu">
						<ul>
							{
								this.props.item.children.map((item, key) => {
									return (
										<MobileMenuItem item={item} key={key} />
									);
								})
							}
						</ul>
					</div>
				}
			</li>
		);
	}

	protected handleClick() {
		this.setState({
			isOpen: !this.state.isOpen,
		});
	}

}
