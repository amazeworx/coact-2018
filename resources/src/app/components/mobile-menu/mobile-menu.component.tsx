import * as React from 'react';

import { AccessibiltyModule }  from '../accessibility-module';
import { MobileMenuSearch }    from '../mobile-menu-search';
import { MobileMenuItem }      from './mobile-menu-item.component';
import { IArticleHeaderProps } from './mobile-menu.renderer';

interface IMobileMenuState {
	data: any;
	showChild: any;
}

export class MobileMenu extends React.Component<IArticleHeaderProps, IMobileMenuState> {
	constructor(props: any) {
		super(props);

		const data = websiteData[props.uniqueParameter];

		this.state = {
			data,
			showChild: {},
		};
	}

	public render() {
		return (
			<div className="mobile_menu">
				<MobileMenuSearch />
				<ul>
					{
					this.state.data.map((item, key) => {
						return (
							<MobileMenuItem item={item} key={key} />
						);
					})
					}
				</ul>

				<ul className="access-menu">
					<li>
						<div>
							<span className="title">Accessibilty</span>
							<AccessibiltyModule />
						</div>
					</li>
				</ul>
			</div>
		);
	}
}
