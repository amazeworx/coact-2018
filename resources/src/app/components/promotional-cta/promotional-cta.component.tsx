import * as React from 'react';
import * as ReactSVG from 'react-svg';

interface IProps {
	uniqueIdentifier: string;
}

interface IState {
	data: {
		promotional_image: string;
		promotional_link_label: string;
		promotional_text: string;
		promotional_link: {
			target: string;
			title: string;
			url: string;
		};
	};
	isOpen: boolean;
	hasBeenClosed: boolean;
}

class PromotionalCta extends React.Component<IProps, IState> {
	constructor(props) {
		super(props);

		this.state = {
			data: websiteData[this.props.uniqueIdentifier],
			hasBeenClosed: false,
			isOpen: false,
		};

		this.handleClick = this.handleClick.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.onPageScroll = this.onPageScroll.bind(this);
	}

	public componentDidMount() {
		window.addEventListener('scroll', this.onPageScroll);
	}

	public render() {
		// const anchor = '#subscribe';

		return (
			<div className={'promotional_cta-container '  + (this.state.isOpen ? 'promotional_cta-container--active' : '')}>
				<div className="promotional_cta">
					<button className="promotional_cta-anchor" onClick={this.handleClick}></button>

					<div className="promotional_cta-close" onClick={this.handleClose}>
						<span><ReactSVG path={websiteData.urlTheme + '/assets/icons/common-close.svg'} /></span>
					</div>
					<div className="promotional_cta-image" style={{backgroundImage: `url(${this.state.data.promotional_image})`}}>

					</div>

					<div className="promotional_cta-detail">
						<span className="promotional_cta-promotional_text">{this.state.data.promotional_text}</span>
						<span className="promotional_cta-promotional_link_label">{this.state.data.promotional_link_label}</span>
					</div>
				</div>
			</div>
		);
	}

	protected handleClick() {
		if (
			this.state.data.promotional_link &&
			'target' in this.state.data.promotional_link &&
			'title' in this.state.data.promotional_link &&
			'url' in this.state.data.promotional_link
		) {
			window.location.href = this.state.data.promotional_link.url;
		} else {
			const element = document.querySelector('.subscribe_section');

			element.scrollIntoView({
				behavior: 'smooth',
			});
		}
	}

	protected handleClose() {
		this.setState({
			hasBeenClosed: true,
			isOpen: !this.state.isOpen,
		});
	}

	protected onPageScroll() {
		const element = document.querySelector('.subscribe_section');
		const scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;

		if (this.isElementInView(element)) {
			this.setState({isOpen: false});
		} else {
			if (
				scrollTop > 98
			) {
				if (
					!this.state.isOpen &&
					!this.state.hasBeenClosed
				) {
					this.setState({
						isOpen: true,
					});
				}
			} else {
				if (
					this.state.isOpen
				) {
					this.setState({
						isOpen: false,
					});
				}
			}
		}
	}

	protected isElementInView(element) {
		const rect = element.getBoundingClientRect();

		return (
			rect.top >= 0 &&
			rect.left >= 0 &&
			rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
			rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
		);
	}
}

export default PromotionalCta;
