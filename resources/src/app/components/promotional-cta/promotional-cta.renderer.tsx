import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';
import PromotionalCta from './promotional-cta.component';

/**
 * Attributes expected for the component initialisation
 */
interface IAttributes {
	uniqueIdentifier: string;
}

export class PromotionalCtaRenderer extends ReactRenderer {
	protected _attributeSelector = 'promotional-cta';
	protected _attributeProperties: Array<[string, string]> = [
		['uniqueIdentifier', 'unique-identifier'],
	];

	public initReact(element: Element, attributes: IAttributes) {
		ReactDOM.render(
			<PromotionalCta uniqueIdentifier={attributes.uniqueIdentifier} />,
			element
		);
	}

}
