import * as React from 'react';
import CountUp from 'react-countup';
import * as VisibilitySensor from 'react-visibility-sensor';

export interface INumberTickerProps {
	targetNumber: string;
	targetFormatter: string;
}

export class NumberTicker extends React.Component<INumberTickerProps, any> {

	constructor(props: INumberTickerProps) {
		super(props);
		this.state = {
			formatter: 'none',
			isNumber: true,
			isVisible: false,
			targetNumber: 0,
		};

		this.onChange = this.onChange.bind(this);
		this.formatting = this.formatting.bind(this);
	}

	public componentWillMount() {
		const isNumber = this.props.targetNumber.match(/^[0-9\.]*$/);
		const parsedNumber = parseFloat(this.props.targetNumber.replace(/,/g, ''));

		if (this.props.targetFormatter === 'percentage') {
			this.setState({
				formatter: this.props.targetFormatter,
			});
		} else if (this.props.targetFormatter === 'dollar') {
			this.setState({
				formatter: this.props.targetFormatter,
			});
		}

		this.setState({
			isNumber,
			targetNumber: (isNumber) ? parsedNumber : this.props.targetNumber,
		});
	}

	public render() {
		// Check if the target number should be treated as a number
		if (this.state.isNumber && this.state.isVisible) {
			const formatter = this.state.formatter;

			return (
				<span>
					{ formatter === 'dollar' &&
						<span className="formatter">$</span>
					}

					<CountUp
						start={(this.state.targetNumber / 100) * 99}
						end={this.state.targetNumber}
						duration={8}
						useEasing={false}
						useGrouping={true}
						formattingFn={this.formatting} />

					{ formatter === 'percentage' &&
						<span className="formatter">%</span>
					}
				</span>
			);
		} else {
			return (
				<VisibilitySensor onChange={this.onChange}>
					<span>{this.state.targetNumber}</span>
				</VisibilitySensor>
			);
		}
	}

	protected onChange(isVisible) {
		this.setState({
			isVisible,
		});
	}

	protected formatting(data) {
		return data.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
	}
}
