import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';
import { NumberTicker } from './number-ticker.component';

/**
 * Attributes expected for the component initialisation
 */
interface IAttributes {
	targetNumber: string;
	targetFormatter: string;
}

export class NumberTickerRenderer extends ReactRenderer {
	protected _attributeSelector = 'number-ticker';
	protected _attributeProperties: Array<[string, string]> = [
		['targetNumber', 'number-value'],
		['targetFormatter', 'number-formatter'],
	];

	public initReact(element: Element, attributes: IAttributes) {
		ReactDOM.render(
			<NumberTicker targetNumber={attributes.targetNumber} targetFormatter={attributes.targetFormatter} />,
			element
		);
	}

}
