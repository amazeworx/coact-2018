import * as React from 'react';
import * as ReactSVG from 'react-svg';

import './menu-button.component.scss';

export class MenuButton extends React.Component<any, any> {

	protected MENU_OPEN_BODY_CLASS = 'menu-open';

	constructor(props: any) {
		super(props);

		this.onClick = this.onClick.bind(this);
	}

	public render() {
		return (
			<a href="" className="menu_button-handle" onClick={this.onClick}>
				<span className="svg_icon menu_hamburger">
					<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-hamburger.svg'} />
				</span>
				<span className="svg_icon menu_close">
					<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-close.svg'} />
				</span>
			</a>
		);
	}

	protected onClick(event) {
		document.body.classList.toggle(this.MENU_OPEN_BODY_CLASS);
		event.preventDefault();
	}
}
