import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { MenuButton } from './menu-button.component';

export class MenuButtonRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'menu-button';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<MenuButton />,
			element
		);
	}
}
