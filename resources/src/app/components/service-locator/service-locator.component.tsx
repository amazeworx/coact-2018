import * as algoliasearch from 'algoliasearch';
import * as React from 'react';

import ServiceLocatorDetails from '../service-locator-details/service-locator-details.component';
import ServiceLocatorListing from '../service-locator-listing/service-locator-listing.component';
import ServiceLocatorMap     from '../service-locator-map/service-locator-map.component';
import ServiceLocatorSearch  from '../service-locator-search/service-locator-search.component';

import './service-locator.component.scss';

const ALGOLIA_GEO_SEARCH_RADIUS = 10000; // 10km Radius

export class ServiceLocator extends React.Component<any, any> {

	protected algoliaClient: any;
	protected algoliaIndex: any;
	protected searchOptions: any = {
		hitsPerPage: 500,
	};

	constructor(props: any) {
		super(props);

		// algolia set up
		this.algoliaClient = algoliasearch(algolia.application_id, algolia.search_api_key);
		this.algoliaClient.setExtraHeader('X-Forwarded-For', websiteData.userIP);
		this.algoliaIndex = {
			servicePartners: this.initAlgoliaIndex('posts_service-partner'),
			serviceTypes: this.initAlgoliaIndex('terms_service_types'),
		};

		// state
		this.state = {
			mapInitialised: false,
			partners: [],
			selectedPartner: {},
			selectedPostcode: null,
			selectedService: null,
			serviceTypes: [],
			showPartner: false,
			userLatLng: {},
			withZoom: true,
		};

		// methods
		this.handleClosePartnerDetails = this.handleClosePartnerDetails.bind(this);
		this.handleSelectedPartner = this.handleSelectedPartner.bind(this);
		this.handleSelectServiceType = this.handleSelectServiceType.bind(this);
		this.handleSelectPostcode = this.handleSelectPostcode.bind(this);
		this.handlePostcodeQuery = this.handlePostcodeQuery.bind(this);
		this.handleMapCenterUpdate = this.handleMapCenterUpdate.bind(this);
	}

	public componentWillMount() {
		this.getServiceTypes();
		// this.getPartnersAroundUserIp();
	}

	public componentDidMount() {
		this.getPartnersAroundUserIp();
	}

	public render() {
		const {
			partners,
			serviceTypes,
			selectedPartner,
			selectedPostcode,
			selectedService,
			showPartner,
			userLatLng,
			withZoom,
		} = this.state;

		return (
			<div className="service_locator">
				<div className="service_locator-content_wrapper">
					<div className="service_locator-content">
						<div className="service_locator-sidepane">
							<ServiceLocatorSearch
								serviceTypes={serviceTypes}
								selectedPostcode={selectedPostcode}
								selectedService={selectedService}
								onPostcodeQuery={this.handlePostcodeQuery}
								onSelectServiceType={this.handleSelectServiceType}
								onSelectPostcode={this.handleSelectPostcode}
							/>
							<ServiceLocatorListing
								partners={partners}
								hasFilters={(selectedPostcode || selectedService)}
								onSelect={this.handleSelectedPartner}
								center={userLatLng}
							/>
						</div>
						<div className="service_locator-map">
							<ServiceLocatorMap
								userLatLng={userLatLng}
								partners={partners}
								onSelect={this.handleSelectedPartner}
								onMapCenterUpdate={this.handleMapCenterUpdate}
								withZoom={withZoom}
							/>
						</div>
					</div>
				</div>
				<ServiceLocatorDetails
					partner={selectedPartner}
					isOpen={showPartner}
					close={this.handleClosePartnerDetails}
				/>
			</div>
		);
	}

	private initAlgoliaIndex(indexLabel) {
		const index = algolia.indices[indexLabel];
		if (index) {
			return this.algoliaClient.initIndex(index.name);
		}
	}

	private getServiceTypes() {
		if (this.algoliaIndex.serviceTypes) {
			return this.algoliaIndex.serviceTypes.search()
			.then((res) => {
				this.setState({
					serviceTypes: res.hits.map((type) => {
						return {
							label: type.name,
							value: type.name,
						};
					}),
				});
			});
		}
	}

	private getPartnersAroundUserIp() {
		if (this.algoliaIndex.servicePartners) {
			return this.algoliaIndex.servicePartners.search({
				aroundLatLngViaIP: true,
				hitsPerPage: 500,
			})
			.then((res) => {
				const resLatLong = res.aroundLatLng.split(',');

				this.setState({
					mapInitialised: true,
					partners: res.hits,
					userLatLng: {
						lat: parseFloat(resLatLong[0]),
						lng: parseFloat(resLatLong[1]),
					},
					withZoom: false,
				});
			});
		}
	}

	private getPartnersFiltered(searchOptions: any = {}, withZoom = true) {
		if (
			this.algoliaIndex.servicePartners &&
			this.state.mapInitialised
		) {
			return this.algoliaIndex.servicePartners.search(searchOptions)
			.then((res) => {
				this.setState({
					partners: res.hits,
					withZoom,
				});
			});
		}
	}

	private handleSelectedPartner(partner) {
		return () => {
			document.body.classList.add('partner-open');
			this.setState({
				selectedPartner: partner,
				showPartner: true,
			});
		};
	}

	private handleClosePartnerDetails() {
		document.body.classList.remove('partner-open');
		this.setState({
			selectedPartner: {},
			showPartner: false,
		});
	}

	private handleSelectServiceType(selectedService) {
		// let searchOptions;
		if (selectedService) {
			// searchOptions = {
			// 	facetFilters: [`taxonomies.service_types:${selectedService.value}`],
			// };
			this.searchOptions.facetFilters = [`taxonomies.service_types:${selectedService.value}`];
			delete this.searchOptions.insideBoundingBox;
		} else {
			delete this.searchOptions.facetFilters;
		}

		this.setState({
			selectedPostcode: null,
			selectedService,
		});
		this.getPartnersFiltered(this.searchOptions);
	}

	private handlePostcodeQuery(inputValue, callback) {
		const service = new google.maps.places.AutocompleteService();
		const options = {
			componentRestrictions: {
				country: 'AU',
			},
			input: inputValue,
			types: [
				'(regions)',
				// 'address',
			],
		};
		const handlePlaceQuery = (predictions, status) => {
			let results = [];
			if (status === google.maps.places.PlacesServiceStatus.OK) {
				results = predictions.map((res) => {
					return {
						label: res.description,
						value: res.place_id,
					};
				});
			}

			callback(results);
		};

		return service.getPlacePredictions(options, handlePlaceQuery);
	}

	private handleSelectPostcode(selectedPostcode) {
		// clear the selected service
		this.setState({
			selectedPostcode,
			selectedService: null,
		});
		if (selectedPostcode) {
			// this.searchOptions.facetFilters = [`taxonomies.serviced_areas:${selectedService.value}`];

			const service = new google.maps.Geocoder();
			return service.geocode({
				placeId: selectedPostcode.value,
			}, (results, status) => {
				if (status !== google.maps.GeocoderStatus.OK) {
					return;
				}
				const place = results[0];
				// Find the postcode
				let postcode = null;
				if (
					'address_components' in place &&
					place.address_components &&
					place.address_components.length
				) {
					for (const singleAddressComponent of place.address_components) {
						if (
							'types' in singleAddressComponent &&
							singleAddressComponent.types &&
							singleAddressComponent.types.indexOf('postal_code') >= 0
						) {
							postcode = singleAddressComponent.short_name;
						}
					}
				}

				this.searchOptions.facetFilters = [`taxonomies.serviced_areas:${postcode}`];

				return this.getPartnersFiltered(this.searchOptions);
			});
		} else {
			delete this.searchOptions.facetFilters;
		}

		return this.getPartnersFiltered(this.searchOptions);
	}

	private handleMapCenterUpdate(center) {
		this.setState({
			userLatLng: center,
		});
	}
}
