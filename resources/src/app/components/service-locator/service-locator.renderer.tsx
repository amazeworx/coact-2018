import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { ServiceLocator } from './service-locator.component';

export class ServiceLocatorRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'service-locator';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<ServiceLocator />,
			element
		);

	}

}
