import * as React from 'react';

import Clipboard from 'react-clipboard.js';
import * as ReactSVG from 'react-svg';
import { PageProgress } from './../page-progress';

import { IArticleHeaderProps } from './lockedin-header.renderer';

interface IArticleHeaderData {
	email_share: string;
	facebook_share: string;
	header_title: string;
	twitter_share: string;
	linkedin_share: string;
	logoIconURL: string;
	partnerIconURL: string;
	homeUrl: string;
	contentHubUrl: string;
	themeUrl: string;
	title: string;
	isPost: boolean;
	partnerPhone: string;
}

interface IArticleHeaderState {
	data: IArticleHeaderData;
	isHeaderActive: boolean;
	isShareActive: boolean;
	isCopyActive: boolean;
}

export class LockedinHeader extends React.Component<IArticleHeaderProps, IArticleHeaderState> {
	protected wrapperRef = null;

	constructor(props: IArticleHeaderProps) {
		super(props);

		const data = websiteData[props.uniqueParameter];

		this.state = {
			data,
			isCopyActive: false,
			isHeaderActive: false,
			isShareActive: false,
		};

		this.toggleShare = this.toggleShare.bind(this);
		this.closeShare = this.closeShare.bind(this);
		this.onPageScroll = this.onPageScroll.bind(this);
		this.copiedState = this.copiedState.bind(this);

		this.setWrapperRef = this.setWrapperRef.bind(this);
		this.handleClickOutside = this.handleClickOutside.bind(this);
	}

	public componentDidMount() {
		window.addEventListener('scroll', this.onPageScroll);
		document.addEventListener('mousedown', this.handleClickOutside);
	}

	public componentWillUnmount() {
		document.removeEventListener('mousedown', this.handleClickOutside);
	}

	public render() {
		const isPost = this.state.data.isPost;
		const mailToBody = `Have a look at this website: ${location.href}`;
		const mailToLink = `mailto:?subject=Check this out&body=${encodeURIComponent(mailToBody)}`;

		let phoneNumberWithNoSpaces = this.state.data.partnerPhone;

		if (phoneNumberWithNoSpaces) {
			phoneNumberWithNoSpaces = this.state.data.partnerPhone.replace(' ', '');
		}

		return (
			<div ref={this.setWrapperRef}>
				{isPost ? (
					<header className={'lockedin_header' + (this.state.isHeaderActive ? ' active' : '')}>
					<div className="lockedin_header-container container">

						<div className="lockedin_header-left_aligned">
							<div className="lockedin_header-logo">
								<ReactSVG path={`${this.state.data.logoIconURL}`} />
							</div>

							<div className="lockedin_header-back_button">
								<a className="link_overlay-anchor" href={`${this.state.data.contentHubUrl}`}></a>
								<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/common-arrow.svg`} />
							</div>

							<div className="lockedin_header-back link_overlay">
								<a className="link_overlay-anchor" href={`${this.state.data.contentHubUrl}`}></a>
								<ReactSVG
									className="lockedin_header-arrow"
									path={`${this.state.data.themeUrl}/assets/icons/common-expand-purple.svg`} />
								<span>Content Hub</span>
							</div>

							<div className="lockedin_header-article_title">
								<h3>{this.state.data.title}</h3>
							</div>
						</div>

						<div className="lockedin_header-right_aligned">
							<div tabIndex={0} className={'lockedin_header-share_container' + (this.state.isShareActive ? ' active' : '')}>
								<button
									tabIndex={0}
									className={'lockedin_header-share_button' + (this.state.isShareActive ? ' active' : '')}
									onClick={this.toggleShare}
									>
										<span>
											Share this
										</span>

										<ReactSVG
										className="lockedin_header-share_icon"
										path={`${this.state.data.themeUrl}/assets/icons/common-share.svg`} />
								</button>

								<div className={'lockedin_header-share' + (this.state.isShareActive ? ' active' : '')}>
									<span className={'lockedin_header-share-item' + (this.state.isShareActive ? ' icon-active' : '')}>
										<a
											target="_blank"
											rel="noopener"
											href={this.state.data.facebook_share}
											>
											<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/article-header-facebook-icon.svg`} />
										</a>
									</span>

									<span className={'lockedin_header-share-item' + (this.state.isShareActive ? ' icon-active' : '')}>
										<a
											target="_blank"
											rel="noopener"
											href={this.state.data.twitter_share}
											>
											<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/article-header-twitter-icon.svg`} />
										</a>
									</span>

									<span className={'lockedin_header-share-item' + (this.state.isShareActive ? ' icon-active' : '')}>
										<a
											target="_blank"
											rel="noopener"
											href={this.state.data.linkedin_share}>
											<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/article-header-linkedin-icon.svg`} />
										</a>
									</span>

									<span className={'lockedin_header-share-item' + (this.state.isShareActive ? ' icon-active' : '')}>
										<a
											target="_blank"
											rel="noopener"
											href={mailToLink}
											>
											<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/article-header-mail-icon.svg`} />
										</a>
									</span>

									<span
										className={'lockedin_header-share-item' + (this.state.isShareActive ? ' icon-active' : '')}
										onClick={this.copiedState}>
										<Clipboard
											className="lockedin_header-clipboard"
											data-clipboard-text={location.href}>
											<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/article-header-link-icon.svg`} />
										</Clipboard>

										{ this.state.isCopyActive &&
											<div className="lockedin_header-copied_state">
												<span>Copied!</span>
											</div>
										}
									</span>
								</div>
							</div>

							<div className={'lockedin_header-get_in_touch_button' + (this.state.isShareActive ? ' hide-when-active' : '')}>
								<a href="/contact" className="button button--primary">
									Get in touch
								</a>
							</div>
						</div>
					</div>
						<div className="lockedin_header-page_progress">
							<PageProgress />
						</div>
					</header>
				) : (
					<header className={'lockedin_header partner-detail' + (this.state.isHeaderActive ? ' active' : '')}>
						<div className="lockedin_header-container container">
							<div className="lockedin_header-left_aligned">
								<div className="lockedin_header-logo">
									<ReactSVG path={`${this.state.data.logoIconURL}`} />
								</div>

								<div className="lockedin_header-partner_logo">
									<img src={`${this.state.data.partnerIconURL}`} />
								</div>
							</div>

							<div className="lockedin_header-right_aligned">
								<div className="header-phone_button">
									<a href={`tel:${phoneNumberWithNoSpaces}`}>
										<ReactSVG path={`${this.state.data.themeUrl}/assets/icons/common-phone.svg`} />
										<span className="header-phone_number">{this.state.data.partnerPhone}</span>
									</a>
								</div>

								<div className="header-get_in_touch_button">
									<a href="/contact" className="button button--primary">
										Get in touch
									</a>
								</div>
							</div>
						</div>
					</header>
				)}
			</div>
		);
	}

	protected onPageScroll() {
		const scrollTop = (document.documentElement && document.documentElement.scrollTop) || document.body.scrollTop;
		if (scrollTop > 98) {
			if (!this.state.isHeaderActive) {
				this.setState({isHeaderActive: true});
			}
		} else {
			if (this.state.isHeaderActive) {
				this.setState({isHeaderActive: false});
			}
		}
	}

	protected toggleShare() {
		this.setState({
			isShareActive: !this.state.isShareActive,
		});
	}

	protected closeShare() {
		this.setState({
			isCopyActive: false,
			isShareActive: false,
		});
	}

	protected copiedState() {
		this.setState({
			isCopyActive: true,
		}, () => {
			setTimeout(() => {
				this.setState({
					isCopyActive: false,
					isShareActive: false,
				});
			}, 2000);
		});
	}

	protected setWrapperRef(node) {
		this.wrapperRef = node;
	}

	protected handleClickOutside(event) {
		if (this.wrapperRef && !this.wrapperRef.contains(event.target)) {
			this.closeShare();
		}
	}
}
