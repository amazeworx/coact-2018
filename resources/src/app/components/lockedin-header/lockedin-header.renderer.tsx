import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';
import { LockedinHeader } from './lockedin-header.component';

/**
 * Attributes expected for the component initialisation
 */
export interface IArticleHeaderProps {
	uniqueParameter: string;
}

export class LockedinHeaderRenderer extends ReactRenderer {
	protected _attributeSelector: string = 'lockedin-header';
	protected _attributeProperties: Array<[string, string]> = [
		['uniqueParameter', 'unique-parameter'],
	];

	public initReact(element: Element, attributes: IArticleHeaderProps) {
		ReactDOM.render(
			<LockedinHeader uniqueParameter={attributes.uniqueParameter}/>,
			element
		);
	}
}
