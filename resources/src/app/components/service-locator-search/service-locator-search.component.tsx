import * as React from 'react';
import Select, { Async as AsyncSelect } from 'react-select';
import SelectStyles from '../coact-select/coact-select-styles';

import './service-locator-search.component.scss';

const postcodeSelectStyles = Object.assign({}, SelectStyles, {
	dropdownIndicator: () => {
		return {
			display: 'none',
		};
	},
});

export default (props) => {
	return (
		<div className="service_locator-search">
			<form className="service_locator-panel">
				<h2 className="service_locator-title">Find your nearest service provider</h2>
				<Select
					classNamePrefix="form_select"
					placeholder="Select a service type"
					value={props.selectedService}
					onChange={props.onSelectServiceType}
					options={props.serviceTypes}
					isClearable={true}
					styles={SelectStyles}
				/>
				<div className="service_locator-postcode">
					<AsyncSelect
						classNamePrefix="form_select"
						className="coact_search-select"
						placeholder="Or enter your postcode"
						value={props.selectedPostcode}
						cacheOptions
						loadOptions={props.onPostcodeQuery}
						onChange={props.onSelectPostcode}
						isClearable={true}
						styles={postcodeSelectStyles}
					/>
				</div>
			</form>
		</div>
	);
};
