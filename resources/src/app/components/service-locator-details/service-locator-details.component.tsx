import * as React from 'react';
import * as ReactSVG from 'react-svg';

import './service-locator-details.component.scss';

const PHONE_NUMBER = (props) => {
	if (!props.number) {
		return (<div></div>);
	}

	const telNumber = props.number.replace(/\s/g, '');
	return (
		<div className="service_locator-proofPoint">
			<span className="svg_icon">
				<ReactSVG path={websiteData.urlTheme + `/assets/icons/common-phone.svg`} />
			</span>
			<a href={`tel:${telNumber}`} className="phantom-phone-number">{props.label}: {props.number}</a>
		</div>
	);
};

export default class ServiceLocatorDetails extends React.Component<any, any> {

	constructor(props: any) {
		super(props);
	}

	public render() {
		const partner = this.props.partner;
		const isOpenClass = this.props.isOpen ? 'open' : 'closed';
		const hasDetails = Boolean(Object.keys(partner).length);

		return (
			<div className={`service_locator-details ${isOpenClass}`}>
				<div className="service_locator-details_panel">
					{
						!hasDetails
							? null
							: (
								<div>
									<header className="service_locator-details_header">
										<button className="button button--link back-button" onClick={this.props.close}>
											<span className="svg_icon back-arrow">
												<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-arrow.svg'} />
											</span>
											<span>Back to results</span>
										</button>
										<button className="button button--link close-button" onClick={this.props.close}>
											<span className="svg_icon back-close">
												<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-close.svg'} />
											</span>
										</button>
									</header>

									<div className="service_locator-details_body">
										<div className="service_locator-details_title details_block">
											<h3>{partner.post_title}</h3>
											<div className="service_locator-details_tags">
												{partner.taxonomies && this.renderTags(partner.taxonomies.service_types)}
											</div>
										</div>
										<div className="details_block">
											<div className="service_locator-details_contact list_block">
												{this.renderListItem(partner.address, 'common-location')}

												{ partner.phone_numbers &&
													partner.phone_numbers.map((phoneNumber) => {
														return (<PHONE_NUMBER label={phoneNumber.phone_label} number={phoneNumber.phone_number} />);
													})
												}
											</div>
											<div className="service_locator-details_description">
												{partner.description}
											</div>
										</div>
										<div className="service_locator-details_proofpoints details_block list_block">
											{partner.proof_points && partner.proof_points.map((point) => this.renderListItem(point, 'common-tick'))}
										</div>
									</div>
									<footer className="service_locator-details_footer">
										<a href={partner.permalink + '#enquiry_form'} className="button register-button">Register your interest</a>
										<a href={partner.permalink} className="button button--secondary">Learn More</a>
									</footer>
								</div>
							)
					}
				</div>
				<div className="service_locator-details_overlay" onClick={this.props.close}></div>
			</div>
		);
	}

	private renderListItem(str: string, icon: string) {
		return (
			<div className="service_locator-proofPoint">
				<span className="svg_icon">
					<ReactSVG path={websiteData.urlTheme + `/assets/icons/${icon}.svg`} />
				</span>
				<span>{str}</span>
			</div>
		);
	}

	private renderTags(tags: any[]) {
		return (
			<div className="tags">
				{tags.map((tag) => {
					return <span className="service_locator-listing_tag tag">{tag}</span>;
				})}
			</div>
		);
	}
}
