import * as React from 'react';
import * as ReactSVG from 'react-svg';

import './service-locator-listing.component.scss';

export default class ServiceLocatorListing extends React.Component<any, any> {

	constructor(props: any) {
		super(props);
	}

	public render() {
		const {
			hasFilters,
			partners,
		} = this.props;

		let noOfPartnersFound = 'Showing all Service Partners';
		if (hasFilters) {
			if (partners.length) {
				noOfPartnersFound = `Showing ${partners.length} Service Partner${(partners.length > 1) ? 's' : ''}`;
			} else if (partners.length === 0) {
				noOfPartnersFound = 'Sorry, no service providers found.';
			}
		}

		const servicePartners = this.orderServicePartners(partners);

		return (
			<div className="service_locator-listing">
				<h3 className="service_locator-listing_title h4">
					{noOfPartnersFound}
				</h3>
				<ul className="service_locator-listing_tabs">
					{servicePartners && servicePartners.map((partner) => this.renderPartnerTab(partner))}
				</ul>
			</div>
		);
	}

	protected orderServicePartners(servicePartnersRaw: any[]) {
		const servicePartners = [];
		if (
			servicePartnersRaw instanceof Array &&
			this.props.center
		) {
			servicePartnersRaw.forEach((singlePartner) => {
				if (singlePartner.hasOwnProperty('_geoloc')) {
					// Get the LatLng distance from the center
					const latLng = {
						lat: parseFloat(singlePartner._geoloc.lat),
						lng: parseFloat(singlePartner._geoloc.lng),
					};

					singlePartner.distance = this.getDistanceFromLatLonInKm(
						latLng.lat,
						latLng.lng,
						this.props.center.lat,
						this.props.center.lng
					);

					servicePartners.push(singlePartner);
				}
			});
		}

		// Reorder the service partners
		servicePartners.sort((a, b) => {
			if (a.distance < b.distance) {
				return -1;
			}
			if (a.distance > b.distance) {
				return 1;
			}
			// a must be equal to b
			return 0;
		});

		return servicePartners;
	}

	protected getDistanceFromLatLonInKm(lat1, lon1, lat2, lon2) {
		const R = 6371; // Radius of the earth in km
		const dLat = this.deg2rad(lat2 - lat1);  // deg2rad below
		const dLon = this.deg2rad(lon2 - lon1);
		const a =
		Math.sin(dLat / 2) * Math.sin(dLat / 2) +
		Math.cos(this.deg2rad(lat1)) * Math.cos(this.deg2rad(lat2)) *
		Math.sin(dLon / 2) * Math.sin(dLon / 2)
		;
		const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
		const d = R * c; // Distance in km
		return d;
	}

	protected deg2rad(deg) {
		return deg * (Math.PI / 180);
	}

	private renderPartnerTab(partner) {
		const tags = partner.taxonomies.service_types.map((tag) => {
			return <span className="service_locator-listing_tag tag_element">{tag}</span>;
		});

		return (
			<li className="service_locator-listing_tab link_overlay">
				<button
					className="service_locator-listing_tab_link link_overlay-anchor"
					onClick={this.props.onSelect(partner)}
					title={'View ' + partner.post_title}
				></button>
				<h4 className="service_locator-listing_partner_title">{partner.post_title}</h4>
				<div className="service_locator-listing_tags tags">
					{tags}
				</div>
				<span className="svg_icon right-arrow">
					<ReactSVG path={websiteData.urlTheme + '/assets/icons/common-next.svg'} />
				</span>
			</li>
		);
	}
}
