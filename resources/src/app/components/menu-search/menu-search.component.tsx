import * as React from 'react';
import * as ReactSVG from 'react-svg';

export class MenuSearch extends React.Component<any, any> {
	protected MENU_OPEN_BODY_CLASS = 'toggle-overlay';

	constructor(props: any) {
		super(props);

		this.state = {
			isSearchOpen: false,
			searchQuery: '',
		};

		this.handleClick = this.handleClick.bind(this);
	}

	public render() {
		return (
			<div>
				<div className="search" onClick={this.handleClick}>
					{this.state.isSearchOpen &&
						<span><ReactSVG path={websiteData.urlTheme + '/assets/icons/common-close.svg'} /></span>
					}

					{!this.state.isSearchOpen &&
						<span><ReactSVG path={websiteData.urlTheme + '/assets/icons/common-search.svg'} /></span>
					}

				</div>

				{this.state.isSearchOpen &&
					<div className="search_box">
						<div className="search_box-container container">
							<div className="search_box-inner_container">
								<form onSubmit={this.submit}>
									<div className="search_box-search_query">
										<span><ReactSVG path={websiteData.urlTheme + '/assets/icons/common-search.svg'} /></span>
										<input
											onChange={this.addSearchValueToState}
											value={this.state.searchQuery}
											placeholder="What are you searching for?" />
									</div>

									<div className="header-get_in_touch_button">
										<button onClick={this.submit} type="" className="button button--primary">
											Search
										</button>
									</div>
								</form>

								<div className="search header-search_trigger" onClick={this.handleClick}>
									<span><ReactSVG path={websiteData.urlTheme + '/assets/icons/common-close.svg'} /></span>
								</div>
							</div>
						</div>
					</div>
				}
			</div>
		);
	}

	protected handleClick = () => {
		this.setState({
			isSearchOpen: !this.state.isSearchOpen,
		});

		document.body.classList.toggle(this.MENU_OPEN_BODY_CLASS);
	}

	protected addSearchValueToState = (event) => {
		this.setState({
			searchQuery: event.target.value,
		});
	}

	protected submit = (event) => {
		event.preventDefault();
		const searchQuery = `${websiteData.urlWebsite}/?s=${this.state.searchQuery}`;

		location.href = searchQuery;
	}
}
