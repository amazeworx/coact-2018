import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer, ReactRendererInterface } from '../../frank-react-renderer';
import { MenuSearch } from './menu-search.component';

export class MenuSearchRenderer extends ReactRenderer implements ReactRendererInterface {
	protected _attributeSelector = 'menu-search';
	protected _attributeProperties: Array<[string, string]> = [];

	public initReact(element: Element, attributes: any) {
		ReactDOM.render(
			<MenuSearch />,
			element
		);
	}
}
