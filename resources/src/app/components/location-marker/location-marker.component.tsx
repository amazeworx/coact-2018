import * as React from 'react';
import {
	GoogleMap,
	Marker,
	withGoogleMap,
	withScriptjs
} from 'react-google-maps';
import * as ReactSVG from 'react-svg';
import { compose, lifecycle, withHandlers, withProps, withState } from 'recompose';
import mapStyles from './../service-locator-map/service-locator-map-styles.js';

export interface ILocationMarkerProps {
	uniqueParameter: string;
}

interface IState {
	mapData: any[];
}

const googleMapURL = [
	`https://maps.googleapis.com/maps/api/js?key=${websiteData.googleMapsApi}`,
	'&v=3.exp&libraries=geolocation,geometry,drawing,places',
].join('');

const data = null;

export default compose(
	// Disabling tslint here to allow properties to be unordered
	// tslint:disable
	withProps({
		googleMapURL,
		containerElement: <div className="location_section-map_container" style={{ height: `100%` }} />,
		loadingElement: <div style={{ height: `100%` }} />,
		mapElement: <div style={{ height: `100%` }} />,
	}),
	withState({
		test: 'test',
	}),
	// tslint:enable
	lifecycle({
		componentDidMount() {
			// this.setState({
			// 	zoomToMarkers: (map) => {
			// 		const bounds = new google.maps.LatLngBounds();
			// 		const markers = map.props.children.props.children;
			// 		markers.forEach((child) => {
			// 			if (child && child.type === Marker) {
			// 				const marker = child.props;
			// 				bounds.extend(new google.maps.LatLng(marker.position.lat, marker.position.lng));
			// 			}
			// 		});
			// 		map.fitBounds(bounds);
			// 	},
			// });
		},
	}),
	withScriptjs,
	withGoogleMap
)((props) => {

	const defaultCenter = {
		lat: parseFloat(props.mapLat),
		lng: parseFloat(props.mapLng),
	};

	return (
		<GoogleMap
			defaultZoom={14}
			defaultCenter={defaultCenter}
			defaultOptions={{
				fullscreenControl: false,
				mapTypeControl: false,
				panControl: false,
				rotateControl: false,
				scaleControl: false,
				streetViewControl: false,
				styles: mapStyles,
				zoomControl: false,
			  }}
		>
			<Marker
				position={defaultCenter}
				icon={{ url: websiteData.urlTheme + '/assets/icons/common-location-purple.svg' }}
			/>
		</GoogleMap>
	);
});
