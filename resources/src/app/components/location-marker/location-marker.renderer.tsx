import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { ReactRenderer } from '../../frank-react-renderer';
import LocationMarker from './location-marker.component';
import {
	ILocationMarkerProps
} from './location-marker.component';

/**
 * Attributes expected for the component initialisation
 */

interface IAttributes {
	mapAddress: string;
	mapLat: string;
	mapLng: string;
 }

export class LocationMarkerRenderer extends ReactRenderer {
	protected _attributeSelector = 'location-marker';
	protected _attributeProperties: Array<[string, string]> = [
		['mapAddress', 'map-address'],
		['mapLat', 'map-lat'],
		['mapLng', 'map-lng'],
	];

	public initReact(element: Element, attributes: IAttributes) {
		ReactDOM.render(
			<LocationMarker
				mapAddress={attributes.mapAddress}
				mapLat={attributes.mapLat}
				mapLng={attributes.mapLng}/>,
			element
		);
	}
}
