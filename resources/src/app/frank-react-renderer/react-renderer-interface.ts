export interface ReactRendererInterface {
	/**
	* Init a react component on a DOM Element
	*
	* @param Element The DOM element to initialise the react component from
	* @param any The attributes to attach to the element
	*/
	initReact(element: Element, attributes: any): void;
	/**
	* Function allowing the bootstrapping of the React components across the page
	*/
	bootstrap(): void;
}
