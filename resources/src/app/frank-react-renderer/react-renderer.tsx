export interface IReactRenderer {
	/**
	 * Init a react component on a DOM Element
	 *
	 * @param {Element} element The DOM element to initialise the react component from
	 * @param {any} attributes The attributes to attach to the element
	 */
	initReact(element: Element, attributes: any): void;
	/**
	 * Function allowing the bootstrapping of the React components across the page
	 */
	bootstrap(): void;
}

export abstract class ReactRenderer implements IReactRenderer {
	protected _attributeSelector: string;
	protected _attributeProperties: Array<[string, string]>;

	/**
	 * Init a react component on a DOM Element
	 *
	 * @param {Element} element The DOM element to initialise the react component from
	 * @param {any} attributes The attributes to attach to the element
	 */
	public abstract initReact(element: Element, attributes: any): void;

	public bootstrap() {
		const components: NodeListOf<Element> = document.querySelectorAll(`[${this._attributeSelector}]`);

		[].forEach.call(components, (element: Element) => {
			const attributes = this.initialiseAttributeProperties(element);
			this.initReact(element, attributes);
		});
	}

	/**
	 * Get the attribute properties to initialise the React component
	 */
	protected initialiseAttributeProperties(element: Element): null|any {
		let result = null;
		if (this._attributeProperties.length) {
			result = {};
			for (const attribute of this._attributeProperties) {
				result[attribute[0]] = element.getAttribute(attribute[1]);
			}
		}
		return result;
	}
}

if (!NodeList.prototype.forEach) {
	NodeList.prototype.forEach = (callback, thisArg) => {
		thisArg = thisArg || window;
		for (let i = 0; i < this.length; i++) {
			callback.call(thisArg, this[i], i, this);
		}
	};
}
