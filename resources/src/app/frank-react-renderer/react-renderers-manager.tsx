import { IReactRenderer } from './react-renderer';

export class ReactRenderersManager {
	protected _renderers: any[] = [];

	/**
	 * Add a renderer to the list of page renderers
	 *
	 * @param {IReactRenderer} renderer the renderer
	 */
	public addRenderer(renderer: IReactRenderer): void {
		this._renderers.push(renderer);
	}

	public bootstrap() {
		this._renderers.forEach((renderer) => {
			if (renderer.bootstrap) {
				renderer.bootstrap();
			}
		});
	}
}
