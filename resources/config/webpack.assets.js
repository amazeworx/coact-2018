var webpack = require('webpack')
var ExtractTextPlugin = require('extract-text-webpack-plugin')
var BrowserSyncPlugin = require('browser-sync-webpack-plugin')
var Webpack2Polyfill = require('webpack2-polyfill-plugin')
var helpers = require('./helpers')

const ENV = process.env.NODE_ENV = process.env.ENV = 'production'

module.exports = {
  devtool: 'source-map',

  entry: {
    'admin-custom': './resources/src/admin-custom.ts',
    'fonts': './resources/src/fonts.ts',
    'app': './resources/src/main.tsx'
  },

  output: {
    path: helpers.root('assets'),
    publicPath: '/',
    filename: '[name].js',
    chunkFilename: '[id].chunk.js',
    sourceMapFilename: '[file].map'
  },

  resolve: {
    extensions: ['.ts', '.tsx', '.js', '.jsx', '.json']
  },

  module: {
    rules: [
      {
        test: /\.tsx?$/,
        use: [{
          loader: 'ts-loader',
          options: { configFile: helpers.root('resources/src', 'tsconfig.json') }
        },
        {
          loader: 'tslint-loader',
          options: {
            tsConfigFile: helpers.root('/', 'tslint.json')
          }
        }]
      },
      {
        test: /\.html$/,
        use: [{
          loader: 'html-loader',
          options: {
            minimize: true,
            collapseWhitespace: true
          }
        }]
      },
      {
        test: /icons\/.*\.(png|jpe?g|gif|ico|svg)$/,
        use: [
          'file-loader?name=icons/[name].[ext]',
          {
            loader: 'image-webpack-loader',
            query: {
              mozjpeg: {
                progressive: true
              },
              gifsicle: {
                interlaced: false
              },
              optipng: {
                optimizationLevel: 4
              },
              pngquant: {
                quality: '75-90',
                speed: 3
              }
            }
          }
        ]
      },
      {
        test: /images\/.*\.(png|jpe?g|gif|ico|svg)$/,
        use: [
          'file-loader?name=images/[name].[ext]',
          {
            loader: 'image-webpack-loader',
            query: {
              mozjpeg: {
                progressive: true
              },
              gifsicle: {
                interlaced: false
              },
              optipng: {
                optimizationLevel: 4
              },
              pngquant: {
                quality: '75-90',
                speed: 3
              }
            }
          }
        ]
      },
      {
        test: /fonts\/.*\.(eot|svg|ttf|woff|woff2)$/,
        use: [{
          loader: 'file-loader',
          query: {
            name: 'fonts/[name].[ext]',
            useRelativePath: true,
            publicPath: ''
          }
        }]
      },
      {
        test: /\.scss$/,
        exclude: helpers.root('resources/src', 'app'),
        loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader?sourceMap!postcss-loader!sass-loader'})
      },
      {
        test: /\.scss$/,
        include: helpers.root('resources/src', 'app'),
        use: [
          'to-string-loader',
          'style-loader',
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },
      {
        test: /\.css$/,
        exclude: helpers.root('resources/src', 'app'),
        loader: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader?sourceMap!postcss-loader' })
      },
      {
        test: /\.css$/,
        include: helpers.root('resources/src', 'app'),
        use: [
          'raw-loader',
          'postcss-loader'
        ]
      },
      // Added this because React-Slick could not process the gif inside the node_modules folder
      // Solution Reference here: https://github.com/akiran/react-slick/issues/520
      {
        test: /\.(gif)$/,
        loader: 'url-loader?limit=30000&name=[name]-[hash].[ext]'
      }
    ]
  },

  plugins: [
    new webpack.DllReferencePlugin({
      context: '.',
      manifest: require(helpers.root('dll', 'fonts-manifest.json'))
    }),

    new webpack.DllReferencePlugin({
      context: '.',
      manifest: require(helpers.root('dll', 'vendors-manifest.json'))
    }),
    new Webpack2Polyfill(),
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin({ // https://github.com/angular/angular/issues/10618
      mangle: {
        screw_ie8: true,
        keep_fnames: true
      },
      compress: {
        screw_ie8: true,
        warnings: false
      },
      sourcemap: true
    }),
    new ExtractTextPlugin('[name].css'),
    new webpack.DefinePlugin({
      'process.env': {
        'ENV': JSON.stringify(ENV)
      }
    }),
    new webpack.LoaderOptionsPlugin({
      htmlLoader: {
        minimize: true
      }
	}),
    // new BrowserSyncPlugin({
    //   host: 'localhost',
    //   port: 80,
    //   proxy: 'http://coact.org.local/'
    // })
  ]
}
