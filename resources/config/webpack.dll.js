var webpack = require('webpack');
var helpers = require('./helpers');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = {
	devtool: 'source-map',

	entry: {
		'vendors': [
			// Polyfills
			'core-js/es6',
			'core-js/es7/reflect',
			'whatwg-fetch',
			// React libs
			'react',
			'react-dom',
			'react-svg',
		],
		'fonts': [
			'webfontloader'
		]
	},

	output: {
		path: helpers.root('dll'),
		publicPath: '/',
		filename: '[name].dll.js',
		// The name of the global variable which the library's
		// require() function will be assigned to
		library: '[name]_lib',
		sourceMapFilename: '[file].map',
	},

	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			mangle: {
				screw_ie8: true,
				keep_fnames: true
			},
			compress: {
				screw_ie8: true,
				warnings: false
			},
			sourcemap: true
		}),
		new webpack.DefinePlugin({
			'process.env': {
				'NODE_ENV': JSON.stringify(ENV)
			}
		}),
		new webpack.DllPlugin({
			// The path to the manifest file which maps between
			// modules included in a bundle and the internal IDs
			// within that bundle
			path: helpers.root('dll', '[name]-manifest.json'),
			// The name of the global variable which the library's
			// require function has been assigned to. This must match the
			// output.library option above
			name: '[name]_lib'
		}),
	],
}
