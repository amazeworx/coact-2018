var webpack = require('webpack');
var webpackMerge = require('webpack-merge');
var BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var assetsConfig = require('./webpack.assets.js');

const ENV = process.env.NODE_ENV = process.env.ENV = 'production';

module.exports = webpackMerge(assetsConfig, {
	plugins: [
		new BundleAnalyzerPlugin(),
	]
});
