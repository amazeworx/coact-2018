<?php
use Frank\PageRenderer\PageRenderer;

// Get the default wordpress header
get_header();

$renderer = new PageRenderer();

$renderer->setSingular((is_single() || is_page()));

if (have_posts()) {
	while (have_posts()) {
		the_post();

		$renderer->addPost($post);
	}
}

$renderer->render();

get_footer();

?>
