<?php
namespace App\Utilities;

class SubMenuWrapper extends \Walker_Nav_Menu {
    function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<div class=\"sub-menu_full\"><div class=\"sub-menu_full_container container\"><div class=\"sub-menu_list_wrapper\"><ul class=\"sub-menu\">\n";
    }
    function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul></div></div></div>\n";
    }
}

?>
