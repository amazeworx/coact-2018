<?php
namespace App\Utilities;

/**
 * Round time to the closest time unit
 */
class RoundTime {

	public static function getRoundedTime($timestamp) {
		$result = array();
		$minutes = floor(($timestamp - time()) / 60);

		$roundedMinutes = $minutes % 60;
		$roundedHours = floor($minutes / 60);

		if ($roundedHours > 0) {
			if ($roundedHours > 1) {
				$result[] = $roundedHours . ' hours';
			} else if ($roundedHours > 1) {
				$result[] = $roundedHours . ' hour';
			}
		}

		if ($roundedMinutes > 0) {
			if ($roundedMinutes > 1) {
				$result[] = $roundedMinutes . ' minutes';
			} else if ($roundedMinutes > 1) {
				$result[] = $roundedMinutes . ' minute';
			}
		}

		return implode(' ', $result);
	}

}
?>
