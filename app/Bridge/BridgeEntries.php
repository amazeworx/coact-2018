<?php
namespace App\Bridge;

use GuzzleHttp\Client;

/**
 * Handle the synchronisation of the entries within Bridge
 */
class BridgeEntries {

	protected $client;
	protected $header;

	public function __construct(){
		$this->client = new Client([
			'base_uri' => get_field('bridge_api_url', 'options'),
		]);
	}

	public function synchroniseEntries() {
		// Grab all the entries currently set as draft
		$posts = get_posts([
			'posts_per_page' => -1,
			'post_type' => 'form-submission',
			'post_status' => 'draft',
		]);

		$errors = [];

		if (is_array($posts)) {
			foreach ($posts as $singleEntry) {
				try {
					$this->synchroniseSingleEntry($singleEntry);
				} catch (\Exception $e) {
					$errors[] = "Error while synchronising {$posts->post_title}: " . $e->getMessage();
				}
			}
		}

		if (sizeof($errors)) {
			foreach ($errors as $singleError) {
				echo $singleError . "<br/>" . PHP_EOL;
			}
		}
	}

	/**
	 * Synchronise a single entry
	 *
	 * @param WP_Post $post
	 */
	protected function synchroniseSingleEntry(\WP_Post $singleEntry) {
		// Set the post as published
		wp_update_post([
			'ID' => $singleEntry->ID,
			'post_status' => 'publish',
		]);

		try {
			// Convert the post content to a JSON string
			$jsonData = json_decode(utf8_encode($singleEntry->post_content), true);

			$this->createBridgeEntry($jsonData, $singleEntry->ID);
			// Delete the post
			wp_update_post([
				'ID' => $singleEntry->ID,
				'post_status' => 'trash',
			]);
		} catch (\Exception $e) {
			$timezone = null;
			if (get_option('timezone_string')) {
				$timezone = new \DateTimeZone(get_option('timezone_string'));
			}
			$now = new \DateTime('now', $timezone);
			// Log the error
			update_field(
				'submission_data_error',
				get_field('submission_data_error',  $singleEntry->ID) .
				$now->format('Y-m-d H:i:s') . " " .
				$e->getFile() . " (" . $e->getLine() . ") :" .
				$e->getMessage() . PHP_EOL,
				$singleEntry->ID
			);
			wp_update_post([
				'ID' => $singleEntry->ID,
				'post_status' => 'pending',
			]);
			// Re-throw the exception to be catched higher
			throw $e;
		}
	}

	protected function createBridgeEntry($data, $entryId) {
		$formattedData = null;
		if (isset($data['contact-type'])) {
			switch ($data['contact-type']) {
				case "I'm looking for a job":
					$contractId = 5; // Based on Bridge data
					$formattedData = $this->formatData_lookingForJob($data, $contractId);
					break;
				default:
					throw new \Exception("No formatter for \"{$data['contact-type']}\" submissions.");
					break;
			}
		} else {
			throw new \Exception("No contact type defined");
		}

		if ($formattedData) {
			$timezone = null;
			if (get_option('timezone_string')) {
				$timezone = new \DateTimeZone(get_option('timezone_string'));
			}
			$now = new \DateTime('now', $timezone);
			update_field(
				'submission_data_error',
				get_field('submission_data_error',  $entryId) .
				$now->format('Y-m-d H:i:s') . " Data sent:" . PHP_EOL .
				json_encode($formattedData, JSON_PRETTY_PRINT) . PHP_EOL,
				$entryId
			);

			// Submit the data to Bridge
			$this->client->request('POST', 'clients', [
				'json' => $formattedData,
				'headers' => [
					'Authorization' => 'Bearer ' . get_field('bridge_api_token', 'options'),
					'Content-Type'  => 'application/vnd.api+json',
					'Accept'        => 'application/vnd.api+json',
				]
			]);
		} else {
			throw new \Exception("No formatted data available");
		}
	}

	// I'm a job seeker
	protected function formatData_lookingForJob($data, $contractId) {
		$customValuesMapping = [
			"api_postcode_client"   => "group-job-search-postcode",
			"api_citizen"           => "group-job-search-working-visa",
			"api_disabilityFlag"    => "group-job-search-health-condition",
			"api_HumanServicesFlag" => "group-job-search-human-service-benefits",
			"api_comments_client"   => "group-job-search-comments",
			"api_source"            => "utm_source",
			"api_campaign"          => "utm_campaign",
			"api_medium"            => "utm_medium",
			"api_content"           => "utm_content",
			"api_term"              => "utm_term",
			"api_webpage_id"        => "page_referrer",
		];

		$result = [
			"data" => [
				"type" => "clients",
				"attributes" => [
					"first_name"   => $this->getFieldIfSet('group-job-search-firstname', $data),
					"last_name"    => $this->getFieldIfSet('group-job-search-lastname', $data),
					"email"        => $this->getFieldIfSet('group-job-search-email', $data),
					"phone_mobile" => $this->getFieldIfSet('group-job-search-phone', $data),
					"dob" =>
						$this->getFieldIfSet('group-job-search-date-day', $data)  . "/" .
						$this->getFieldIfSet('group-job-search-date-month', $data) . "/" .
						$this->getFieldIfSet('group-job-search-date-year', $data),
					"custom_values_attributes" => $this->formatCustomValues($customValuesMapping, $data),
					"contract_referrals_attributes" => [
						[
							"contract_id" => $contractId,
							"site_id" => $this->getSiteId($data),
						],
					],
				],
			],
		];

		return $result;
	}

	// // I'm a carer
	// protected function formatData_carer($data, $contractId) {
	// 	$customValuesMapping = [
	// 		"api_postcode_client"   => "group-carer-postcode",
	// 		"api_citizen"           => "group-carer-working-visa",
	// 		// "api_disabilityFlag"    => "group-job-search-health-condition",
	// 		"api_HumanServicesFlag" => "group-carer-human-service-benefits",
	// 		"api_comments_client"   => "group-carer-comments",
	// 		"api_source"            => "utm_source",
	// 		"api_campaign"          => "utm_campaign",
	// 		"api_medium"            => "utm_medium",
	// 		"api_content"           => "utm_content",
	// 		"api_term"              => "utm_term",
	// 		"api_webpage_id"        => "page_referrer",
	// 	];

	// 	$result = [
	// 		"data" => [
	// 			"type" => "clients",
	// 			"attributes" => [
	// 				"first_name"   => $this->getFieldIfSet('group-carer-firstname', $data),
	// 				"last_name"    => $this->getFieldIfSet('group-carer-lastname', $data),
	// 				"email"        => $this->getFieldIfSet('group-carer-email', $data),
	// 				"phone_mobile" => $this->getFieldIfSet('group-carer-phone', $data),
	// 				"dob" =>
	// 					$this->getFieldIfSet('group-carer-date-day', $data)  . "/" .
	// 					$this->getFieldIfSet('group-carer-date-month', $data) . "/" .
	// 					$this->getFieldIfSet('group-carer-date-year', $data),
	// 				"custom_values_attributes" => $this->formatCustomValues($customValuesMapping, $data),
	// 				"contract_referrals_attributes" => [
	// 					[
	// 						"contract_id" => $contractId,
	// 						"site_id" => $this->getSiteId($data),
	// 					],
	// 				],
	// 			],
	// 		],
	// 	];

	// 	return $result;
	// }

	// // I'm a referral partner
	// protected function formatData_referralPartner($data, $contractId) {
	// 	$customValuesMapping = [
	// 		"api_postcode_client"   => "group-service-partner-postcode",
	// 		"api_citizen"           => "group-service-partner-working-visa",
	// 		"api_disabilityFlag"    => "group-service-partner-health-condition",
	// 		"api_HumanServicesFlag" => "group-service-partner-human-service-benefits",
	// 		"api_comments_client"   => "group-service-partner-comments",
	// 		"api_source"            => "utm_source",
	// 		"api_campaign"          => "utm_campaign",
	// 		"api_medium"            => "utm_medium",
	// 		"api_content"           => "utm_content",
	// 		"api_term"              => "utm_term",
	// 		"api_webpage_id"        => "page_referrer",
	// 	];

	// 	$result = [
	// 		"data" => [
	// 			"type" => "clients",
	// 			"attributes" => [
	// 				"first_name"   => $this->getFieldIfSet('group-service-partner-firstname', $data),
	// 				"last_name"    => $this->getFieldIfSet('group-service-partner-lastname', $data),
	// 				"email"        => $this->getFieldIfSet('group-service-partner-email', $data),
	// 				"phone_mobile" => $this->getFieldIfSet('group-service-partner-phone', $data),
	// 				"dob" =>
	// 					$this->getFieldIfSet('group-service-partner-date-day', $data)  . "/" .
	// 					$this->getFieldIfSet('group-service-partner-date-month', $data) . "/" .
	// 					$this->getFieldIfSet('group-service-partner-date-year', $data),
	// 				"custom_values_attributes" => $this->formatCustomValues($customValuesMapping, $data),
	// 				"contract_referrals_attributes" => [
	// 					[
	// 						"contract_id" => $contractId,
	// 						"site_id" => $this->getSiteId($data),
	// 					],
	// 				],
	// 			],
	// 		],
	// 	];

	// 	return $result;
	// }

	// // I'm a service partner
	// protected function formatData_servicePartner($data, $contractId) {
	// 	$customValuesMapping = [
	// 		"api_postcode_client"   => "group-service-partner-postcode",
	// 		"api_citizen"           => "group-service-partner-working-visa",
	// 		"api_disabilityFlag"    => "group-service-partner-health-condition",
	// 		"api_HumanServicesFlag" => "group-service-partner-human-service-benefits",
	// 		"api_comments_client"   => "group-service-partner-comments",
	// 		"api_source"            => "utm_source",
	// 		"api_campaign"          => "utm_campaign",
	// 		"api_medium"            => "utm_medium",
	// 		"api_content"           => "utm_content",
	// 		"api_term"              => "utm_term",
	// 		"api_webpage_id"        => "page_referrer",
	// 	];

	// 	$result = [
	// 		"data" => [
	// 			"type" => "clients",
	// 			"attributes" => [
	// 				"first_name"   => $this->getFieldIfSet('group-service-partner-firstname', $data),
	// 				"last_name"    => $this->getFieldIfSet('group-service-partner-lastname', $data),
	// 				"email"        => $this->getFieldIfSet('group-service-partner-email', $data),
	// 				"phone_mobile" => $this->getFieldIfSet('group-service-partner-phone', $data),
	// 				"dob" =>
	// 					$this->getFieldIfSet('group-service-partner-date-day', $data)  . "/" .
	// 					$this->getFieldIfSet('group-service-partner-date-month', $data) . "/" .
	// 					$this->getFieldIfSet('group-service-partner-date-year', $data),
	// 				"custom_values_attributes" => $this->formatCustomValues($customValuesMapping, $data),
	// 				"contract_referrals_attributes" => [
	// 					[
	// 						"contract_id" => $contractId,
	// 						"site_id" => $this->getSiteId($data),
	// 					],
	// 				],
	// 			],
	// 		],
	// 	];

	// 	return $result;
	// }

	protected function getFieldIfSet($fieldName, $data) {
		if (isset($data[$fieldName])) {
			if (is_array($data[$fieldName])) {
				if (sizeof($data[$fieldName])) {
					return $data[$fieldName][0];
				}
			} else {
				return $data[$fieldName];
			}
		}
		return null;
	}

	protected function formatCustomValues($customValuesMapping, $data) {
		$result = [];

		if (sizeof($customValuesMapping)) {
			foreach ($customValuesMapping as $code => $fieldName) {
				$value = $this->getFieldIfSet($fieldName, $data);
				if ($value) {
					$result[] = [
						"code" => $code,
						"value" => $value,
					];
				}
			}
		}

		return $result;
	}

	protected function getSiteId($data) {
		$siteId = $this->getFieldIfSet('api_id', $data);
		// Use the default site ID
		if (!$siteId) {
			$siteId = get_field('bridge_api_default_site_id', 'options');
		}
		return $siteId;
	}

}
?>
