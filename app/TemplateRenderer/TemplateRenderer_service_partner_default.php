<?php
namespace App\TemplateRenderer;

use Frank\TemplateRenderer\TemplateRenderer;

/**
 * Template Renderer for a default page
 */
class TemplateRenderer_service_partner_default extends TemplateRenderer {

	/**
	 * The template sections renderer and their formatter
	 *
	 * The template sections array must be formatted as follow:
	 * [
	 *   ["section_renderer_name", "section_formatter_name"],
	 *   ["section_renderer_name", "section_formatter_name"]
	 * ]
	 *
	 * Please note the section formatter and renderer name "flexible_sections" is reserved for flexible sections
	 *
	 * @var array<array<string>>
	 */
	protected $templateSections = [
		['header',                  'header'],
		['lockedin_header',         'lockedin_header'],
		['partner_hero',            'partner_hero'],
		['checkmark_list',          'checkmark_list'],
		['specialise_links',        'specialise_links'],
		['testimonials',            'testimonials'],
		['employers_logos',         'employers_logos'],
		['staff_list',              'staff_list'],
		['numbers',                 'numbers'],
		['tiles_one',               'tiles_one'],
		['tiles_two',               'tiles_two'],
		['location',                'location'],
		['enquiry_form',            'enquiry_form'],
		['related_tiles',           'related_tiles'],
		['subscribe_default',       'subscribe_default'],
		['footer',                  'footer'],
	];
}

?>
