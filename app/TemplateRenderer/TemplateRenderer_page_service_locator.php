<?php
namespace App\TemplateRenderer;

use Frank\TemplateRenderer\TemplateRenderer;

/**
 * Template Renderer for a default page
 */
class TemplateRenderer_page_service_locator extends TemplateRenderer {

	/**
	 * The template sections renderer and their formatter
	 *
	 * The template sections array must be formatted as follow:
	 * [
	 *   ["section_renderer_name", "section_formatter_name"],
	 *   ["section_renderer_name", "section_formatter_name"]
	 * ]
	 *
	 * Please note the section formatter and renderer name "flexible_sections" is reserved for flexible sections
	 *
	 * @var array<array<string>>
	 */
	protected $templateSections = [
		['header',          'header'],
		['service_locator', 'service_locator'],
	];

}

?>
