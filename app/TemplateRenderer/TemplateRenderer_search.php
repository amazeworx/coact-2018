<?php
namespace App\TemplateRenderer;

use Frank\TemplateRenderer\TemplateRenderer;

/**
 * Template Renderer for a default post
 */
class TemplateRenderer_search extends TemplateRenderer {

	/**
	 * The template sections renderer and their formatter
	 *
	 * The template sections array must be formatted as follow:
	 * [
	 *   ["section_renderer_name", "section_formatter_name"],
	 *   ["section_renderer_name", "section_formatter_name"]
	 * ]
	 *
	 * Please note the section formatter and renderer name "flexible_sections" is reserved for flexible sections
	 *
	 * @var array<array<string>>
	 */
	protected $templateSections = [
		['header',           'header'],
		['search',           'search'],
		['subscribe_default','subscribe_default'],
		['cta',              'cta'],
		['footer',           'footer'],
	];

}

?>
