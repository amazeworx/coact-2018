<?php
namespace App\TemplateRenderer;

use Frank\TemplateRenderer\TemplateRenderer;

/**
 * Template Renderer for the content hub landing page
 */
class TemplateRenderer_page_content_hub_landing extends TemplateRenderer {

	/**
	 * The template sections renderer and their formatter
	 *
	 * The template sections array must be formatted as follow:
	 * [
	 *   ["section_renderer_name", "section_formatter_name"],
	 *   ["section_renderer_name", "section_formatter_name"]
	 * ]
	 *
	 * Please note the section formatter and renderer name "flexible_sections" is reserved for flexible sections
	 *
	 * @var array<array<string>>
	 */
	protected $templateSections = [
		['header',                   'header'],
		['top_featured_article',     'top_featured_article'],
		['featured_articles',        'featured_articles'],
		['breakout_article',         'breakout_article'],
		['lower_featured_articles',  'lower_featured_articles'],
		['subscribe_default',        'subscribe_default'],
		['cta',                      'cta'],
		['footer',                   'footer'],
	];

}

?>
