<?php
namespace App\EntriesSynchronisation;

use GuzzleHttp\Client;

/**
 * Handle the synchronisation of the entries within
 */
class Entries {
	protected $client;
	protected $apiUrl;
	protected $header;

	public function __construct(){
		$this->apiUrl = get_field('entries_api_url', 'options');
		$this->client = new Client();
	}

	public function synchroniseEntries() {
		// Grab all the entries currently set as draft
		$posts = get_posts([
			'posts_per_page' => -1,
			'post_type' => 'form-submission',
			'post_status' => 'draft',
		]);

		$errors = [];

		if (is_array($posts)) {
			foreach ($posts as $singleEntry) {
				try {
					$this->synchroniseSingleEntry($singleEntry);
				} catch (\Exception $e) {
					$errors[] = "Error while synchronising {$posts->post_title}: " . $e->getMessage();
				}
			}
		}

		if (sizeof($errors)) {
			foreach ($errors as $singleError) {
				echo $singleError . "<br/>" . PHP_EOL;
			}
		}
	}

	/**
	 * Synchronise a single entry
	 *
	 * @param WP_Post $post
	 */
	protected function synchroniseSingleEntry(\WP_Post $singleEntry) {
		// Set the post as published
		wp_update_post([
			'ID' => $singleEntry->ID,
			'post_status' => 'publish',
		]);

		try {
			// Convert the post content to a JSON string
			$jsonData = json_decode(utf8_encode($singleEntry->post_content), true);

			$this->createEntry($jsonData, $singleEntry->ID);
			// Delete the post
			wp_update_post([
				'ID' => $singleEntry->ID,
				'post_status' => 'trash',
			]);
		} catch (\Exception $e) {
			$timezone = null;
			if (get_option('timezone_string')) {
				$timezone = new \DateTimeZone(get_option('timezone_string'));
			}
			$now = new \DateTime('now', $timezone);
			// Log the error
			update_field(
				'submission_data_error',
				get_field('submission_data_error',  $singleEntry->ID) .
				$now->format('Y-m-d H:i:s') . " " .
				$e->getFile() . " (" . $e->getLine() . ") :" .
				$e->getMessage() . PHP_EOL,
				$singleEntry->ID
			);
			wp_update_post([
				'ID' => $singleEntry->ID,
				'post_status' => 'pending',
			]);
			// Re-throw the exception to be catched higher
			throw $e;
		}
	}

	protected function createEntry($data, $entryId) {
		$formattedData = null;
		if (isset($data['contact-type'])) {
			switch ($data['contact-type']) {
				case "I'm making a general enquiry":
					$formattedData = $this->formatData_general($data);
					break;
				case "I'm a service partner":
					$formattedData = $this->formatData_servicePartner($data);
					break;
				case "I'm an employer":
					$formattedData = $this->formatData_employer($data);
					break;
				case "I'm looking for a job":
					$formattedData = $this->formatData_lookingForJob($data);
					break;
				case "I'm a referral partner":
					$formattedData = $this->formatData_referralPartner($data);
					break;
				default:
					throw new \Exception("No formatter for \"{$data['contact-type']}\" submissions.");
					break;
			}
		} else {
			throw new \Exception("No contact type defined");
		}

		if ($formattedData) {
			$timezone = null;
			if (get_option('timezone_string')) {
				$timezone = new \DateTimeZone(get_option('timezone_string'));
			}
			$now = new \DateTime('now', $timezone);
			update_field(
				'submission_data_error',
				get_field('submission_data_error',  $entryId) .
				$now->format('Y-m-d H:i:s') . " Data sent:" . PHP_EOL .
				json_encode($formattedData, JSON_PRETTY_PRINT) . PHP_EOL,
				$entryId
			);

			// Submit the data
			$this->client->request('POST', $this->apiUrl, [
				'form_params' => $formattedData,
			]);
		} else {
			throw new \Exception("No formatted data available");
		}
	}

	// I'm making a general enquiry
	protected function formatData_general($data) {
		if ($this->getFieldIfSet('group-general-opt-in', $data) === 'Yes') {
			$optValue = '1';
		} else {
			$optValue = '0';
		}
		$result = [
			"orgid" => $this->getFieldIfSet('orgid', $data),
			"retURL" => $this->getFieldIfSet('retURL', $data),
			"name" => $this->getFieldIfSet('group-general-firstname', $data) . " " . $this->getFieldIfSet('group-general-lastname', $data),
			"email" => $this->getFieldIfSet('group-general-email', $data),
			"phone" => $this->getFieldIfSet('group-general-phone', $data),
			"company" => $this->getFieldIfSet('group-general-company', $data),
			"00N0o00000I3a3f" => $this->getFieldIfSet('group-general-position', $data),
			"00N0o00000I2con" => $this->getFieldIfSet('group-general-comments', $data),
			"00N0o00000I2cly" => $optValue,
			"00N0o00000I2bpB" => $this->getFieldIfSet('utm_source', $data),
			"00N0o00000I2bpa" => $this->getFieldIfSet('utm_medium', $data),
			"00N0o00000I2bpu" => $this->getFieldIfSet('utm_campaign', $data),
			"00N0o00000I2bpz" => $this->getFieldIfSet('utm_content', $data),
			"00N0o00000I2bqT" => $this->getFieldIfSet('page_referrer', $data),
			"00N0o00000I3Lvd" => $this->getFieldIfSet('contact-type', $data),
		];
		return $result;
	}

	// I'm a service partner
	protected function formatData_servicePartner($data) {
		if ($this->getFieldIfSet('group-service-partner-opt-in', $data) === 'Yes') {
			$optValue = '1';
		} else {
			$optValue = '0';
		}
		$result = [
			"orgid" => $this->getFieldIfSet('orgid', $data),
			"retURL" => $this->getFieldIfSet('retURL', $data),
			"name" => $this->getFieldIfSet('group-service-partner-firstname', $data) . " " . $this->getFieldIfSet('group-service-partner-lastname', $data),
			"email" => $this->getFieldIfSet('group-service-partner-email', $data),
			"phone" => $this->getFieldIfSet('group-service-partner-phone', $data),
			"company" => $this->getFieldIfSet('group-service-partner-company', $data),
			"00N0o00000I3a3f" => $this->getFieldIfSet('group-service-partner-position', $data),
			"00N0o00000I2con" => $this->getFieldIfSet('group-service-partner-comments', $data),
			"00N0o00000I2cly" => $optValue,
			"00N0o00000I2bpB" => $this->getFieldIfSet('utm_source', $data),
			"00N0o00000I2bpa" => $this->getFieldIfSet('utm_medium', $data),
			"00N0o00000I2bpu" => $this->getFieldIfSet('utm_campaign', $data),
			"00N0o00000I2bpz" => $this->getFieldIfSet('utm_content', $data),
			"00N0o00000I2bqT" => $this->getFieldIfSet('page_referrer', $data),
			"00N0o00000I3Lvd" => $this->getFieldIfSet('contact-type', $data),
		];
		return $result;
	}

	// I'm an employer
	protected function formatData_employer($data) {
		if ($this->getFieldIfSet('group-employer-opt-in', $data) === 'Yes') {
			$optValue = '1';
		} else {
			$optValue = '0';
		}
		$result = [
			"orgid" => $this->getFieldIfSet('orgid', $data),
			"retURL" => $this->getFieldIfSet('retURL', $data),
			"name" => $this->getFieldIfSet('group-employer-firstname', $data) . " " . $this->getFieldIfSet('group-employer-lastname', $data),
			"email" => $this->getFieldIfSet('group-employer-email', $data),
			"phone" => $this->getFieldIfSet('group-employer-phone', $data),
			"company" => $this->getFieldIfSet('group-employer-company', $data),
			"00N0o00000I3a3f" => $this->getFieldIfSet('group-employer-position', $data),
			"00N0o00000I2con" => $this->getFieldIfSet('group-employer-comments', $data),
			"00N0o00000I2cly" => $optValue,
			"00N0o00000I2bpB" => $this->getFieldIfSet('utm_source', $data),
			"00N0o00000I2bpa" => $this->getFieldIfSet('utm_medium', $data),
			"00N0o00000I2bpu" => $this->getFieldIfSet('utm_campaign', $data),
			"00N0o00000I2bpz" => $this->getFieldIfSet('utm_content', $data),
			"00N0o00000I2bqT" => $this->getFieldIfSet('page_referrer', $data),
			"00N0o00000I3Lvd" => $this->getFieldIfSet('contact-type', $data),
			"00N0o00000I3a9U" => $this->getFieldIfSet('group-employer-contact-nature', $data)
		];
		return $result;
	}

	// I'm a job seeker
	protected function formatData_lookingForJob($data) {
		if ($this->getFieldIfSet('group-job-search-opt-in', $data) === 'Yes') {
			$optValue = '1';
		} else {
			$optValue = '0';
		}
		$result = [
			"orgid" => $this->getFieldIfSet('orgid', $data),
			"retURL" => $this->getFieldIfSet('retURL', $data),
			"name" => $this->getFieldIfSet('group-job-search-firstname', $data) . " " . $this->getFieldIfSet('group-job-search-lastname', $data),
			"email" => $this->getFieldIfSet('group-job-search-email', $data),
			"phone" => $this->getFieldIfSet('group-job-search-phone', $data),
			"00N0o00000I2bo8" => $this->getFieldIfSet('group-job-search-postcode', $data),
			"00N0o00000I2boh" => $this->getFieldIfSet('group-job-search-date-day', $data) . "/" . $this->getFieldIfSet('group-job-search-date-month', $data) . "/" . $this->getFieldIfSet('group-job-search-date-year', $data),
			"00N0o00000I2aMX" => $this->getFieldIfSet('group-job-search-working-visa', $data),
			"00N0o00000I2aMw" => $this->getFieldIfSet('group-job-search-health-condition', $data),
			"00N0o00000I2aMr" => $this->getFieldIfSet('group-job-search-employer-situation', $data),
			"00N0o00000I2aNp" => $this->getFieldIfSet('group-job-search-human-service-benefits', $data),
			"00N0o00000I2aNf" => $this->getFieldIfSet('group-job-search-preferred-communication', $data),
			"00N0o00000I2con" => $this->getFieldIfSet('group-job-search-comments', $data),
			"00N0o00000I2cly" => $optValue,
			"00N0o00000I2bpB" => $this->getFieldIfSet('utm_source', $data),
			"00N0o00000I2bpa" => $this->getFieldIfSet('utm_medium', $data),
			"00N0o00000I2bpu" => $this->getFieldIfSet('utm_campaign', $data),
			"00N0o00000I2bpz" => $this->getFieldIfSet('utm_content', $data),
			"00N0o00000I3Lvd" => $this->getFieldIfSet('contact-type', $data),
		];
		return $result;
	}

	// I'm a referral partner
	protected function formatData_referralPartner($data) {
		if ($this->getFieldIfSet('group-referral-partner-opt-in', $data) === 'Yes') {
			$optValue = '1';
		} else {
			$optValue = '0';
		}
		$result = [
			"orgid" => $this->getFieldIfSet('orgid', $data),
			"retURL" => $this->getFieldIfSet('retURL', $data),
			"00N0o00000I3b7J" => $this->getFieldIfSet('group-referral-partner-firstname', $data) . " " . $this->getFieldIfSet('group-referral-partner-lastname', $data),
			"00N0o00000I3b90" => $this->getFieldIfSet('group-referral-partner-email', $data),
			"00N0o00000I3b8H" => $this->getFieldIfSet('group-referral-partner-phone', $data),
			"00N0o00000I3bEa" => $this->getFieldIfSet('group-referral-partner-relationship-to-seeker', $data),
			"00N0o00000I3bO8" => $this->getFieldIfSet('group-referral-partner-company-school', $data),
			"00N0o00000I3bNy" => $this->getFieldIfSet('group-referral-partner-position', $data),
			"name" => $this->getFieldIfSet('group-referral-partner-patient-firstname', $data) . " " . $this->getFieldIfSet('group-referral-partner-patient-lastname', $data),
			"email" => $this->getFieldIfSet('group-referral-partner-patient-email', $data),
			"phone" => $this->getFieldIfSet('group-referral-partner-patient-phone', $data),
			"00N0o00000I2bo8" => $this->getFieldIfSet('group-referral-partner-postcode', $data),
			"00N0o00000I2boh" => $this->getFieldIfSet('group-referral-partner-date-day', $data) . "/" . $this->getFieldIfSet('group-referral-partner-date-month', $data) . "/" . $this->getFieldIfSet('group-referral-partner-date-year', $data),
			"00N0o00000I2aMX" => $this->getFieldIfSet('group-referral-partner-visa', $data),
			"00N0o00000I2aMw" => $this->getFieldIfSet('group-referral-partner-disability', $data),
			"00N0o00000I2aMr" => $this->getFieldIfSet('group-referral-partner-employer-situation', $data),
			"00N0o00000I2aNp" => $this->getFieldIfSet('group-referral-partner-receiving-dohs-benefits', $data),
			"00N0o00000I3bTi" => $this->getFieldIfSet('group-referral-partner-medical-situation', $data),
			"00N0o00000I3bU7" => $this->getFieldIfSet('group-referral-partner-client-ito', $data),
			"00N0o00000I3bXG" => $this->getFieldIfSet('group-referral-partner-client-forensic-order', $data),
			"00N0o00000I3bXL" => $this->getFieldIfSet('group-referral-partner-client-formal-guardian', $data),
			"00N0o00000I3bXk" => $this->getFieldIfSet('group-referral-partner-client-refferal', $data),
			"00N0o00000I3bsJ" => $this->getFieldIfSet('group-referral-partner-client-relevant-reasons', $data),
			"00N0o00000I2cly" => $optValue,
			"00N0o00000I2bpB" => $this->getFieldIfSet('utm_source', $data),
			"00N0o00000I2bpa" => $this->getFieldIfSet('utm_medium', $data),
			"00N0o00000I2bpu" => $this->getFieldIfSet('utm_campaign', $data),
			"00N0o00000I2bpz" => $this->getFieldIfSet('utm_content', $data),
			"00N0o00000I3Lvd" => $this->getFieldIfSet('contact-type', $data),
		];
		return $result;
	}

	protected function getFieldIfSet($fieldName, $data) {
		if (isset($data[$fieldName])) {
			if (is_array($data[$fieldName])) {
				if (sizeof($data[$fieldName])) {
					return $data[$fieldName][0];
				}
			} else {
				return $data[$fieldName];
			}
		}
		return null;
	}

	protected function formatCustomValues($customValuesMapping, $data) {
		$result = [];

		if (sizeof($customValuesMapping)) {
			foreach ($customValuesMapping as $code => $fieldName) {
				$value = $this->getFieldIfSet($fieldName, $data);
				if ($value) {
					$result[] = [
						"code" => $code,
						"value" => $value,
					];
				}
			}
		}

		return $result;
	}

	protected function getSiteId($data) {
		$siteId = $this->getFieldIfSet('api_id', $data);
		// Use the default site ID
		if (!$siteId) {
			$siteId = get_field('entries_api_default_site_id', 'options');
		}
		return $siteId;
	}

}

?>
