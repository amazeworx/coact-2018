<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the related content area
 */
interface SectionContract_testimonials extends SectionFormatterInterface {

	/**
	 *  Are there testimonails
	 *
	 * @return boolean
	 */
	public function hasTestimonials();

	/**
	 * Get the testimonails
	 *
	 * @return array
	 */
	public function getTestimonials();

}

?>
