<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of an 404 content
*/
interface SectionContract_404_content extends SectionFormatterInterface {

	/**
	 * Nothing needs to be done
	 */

}

?>
