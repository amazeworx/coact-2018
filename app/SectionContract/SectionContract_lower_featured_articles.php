<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of content hub landing page featured articles
 */
interface SectionContract_lower_featured_articles extends SectionFormatterInterface {

	/**
	 * Checks if has featured articles
	 *
	 * @return boolean true if value is an array AND it has an object inside
	 */
	public function hasFeaturedArticles();

	/**
	 * Gets array featured articles
	 *
	 * @return array returns array of proofpoints
	 */
	public function getFeaturedArticles();
}

?>
