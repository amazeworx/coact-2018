<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a page content section
*/
interface SectionContract_related_tiles extends SectionFormatterInterface {

	/**
	 * Check if the section has a title
	 *
	 * @return boolean
	 */
	public function hasTitle();

	/**
	 * Get the section title
	 *
	 * @return string
	 */
	public function getTitle();

	/**
	 * check if section has article tile data
	 *
	 * @return boolean
	 */
	public function hasRelatedTilesData();

	/**
	 * check if section has articles
	 *
	 * @return boolean
	 */
	public function hasTiles();

	/**
	 * Get related articles
	 *
	 * @return array
	 */
	public function getTiles();
}

?>
