<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the employers logos section
 */
interface SectionContract_employers_logos extends SectionFormatterInterface {

	/**
	 * Is content set
	 *
	 * @return boolean
	 */
	public function hasLogos();

	/**
	 * Get the page content
	 *
	 * @return string the page content coming from the WYSIWYG
	 */
	public function getLogos();

}

?>
