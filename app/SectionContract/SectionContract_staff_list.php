<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of a staff list
 */
interface SectionContract_staff_list extends SectionFormatterInterface {

	/**
	 * Is staff data set and does it have any data in the array
	 *
	 * @return boolean
	 */
	public function hasStaffList();

	/**
	 * Get the staff data
	 *
	 * @return array
	 */
	public function getStaffList();

}

?>
