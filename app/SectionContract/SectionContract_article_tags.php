<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the article Tags area
 */
interface SectionContract_article_tags extends SectionFormatterInterface {

	/**
	 * Checks if has article has an image
	 *
	 * @return boolean
	 */
	public function hasArticleTags();

	/**
	 * Gets article tags
	 *
	 * @return array
	 */
	public function getArticleTags();
}

?>
