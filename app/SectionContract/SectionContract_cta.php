<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a page content section
*/
interface SectionContract_cta extends SectionFormatterInterface {
	/**
	 * Check if the cta section has a image
	 *
	 * @return boolean the section has a value
	 */
	public function hasCtaImage();

	/**
	 * Get the cta image
	 *
	 * @return string
	 */
	public function getCtaImage();

	/**
	 * Check if the Cta section has a illustration
	 *
	 * @return boolean
	 */
	public function hasCtaIllustration();
	/**
	 * Get the cta illustration
	 *
	 * @return string
	 */
	public function getCtaIllustration();

	/**
	 * Check if the Cta form has a title
	 *
	 * @return boolean
	 */
	public function hasCtaTitle();

	/**
	 * Get the cta title
	 *
	 * @return string
	 */
	public function getCtaTitle();

	/**
	 * Check if the Cta section has a description
	 *
	 * @return boolean
	 */
	public function hasCtaDescription();

	/**
	 * Get the cta description
	 *
	 * @return string
	 */
	public function getCtaDescription();

	/**
	 * Check if the cta has a buttons
	 *
	 * @return boolean
	 */

	public function hasCtaButtons();

	/**
	 * Get the cta buttons
	 *
	 * @return array
	 */
	public function getCtaButtons();
}

?>
