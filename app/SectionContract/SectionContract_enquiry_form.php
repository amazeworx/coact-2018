<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of a enquiry form section
 */
interface SectionContract_enquiry_form extends SectionFormatterInterface {

	/**
	 * Is form set
	 *
	 * @return boolean
	 */
	public function hasForm();

	/**
	 * Get the page form
	 *
	 * @return string
	 */
	public function getForm();

}

?>
