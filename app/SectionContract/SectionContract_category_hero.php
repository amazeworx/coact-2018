<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the category hero area
 */
interface SectionContract_category_hero extends SectionFormatterInterface {

	/**
	 * Checks if has category has an image
	 *
	 * @return boolean
	 */
	public function hasHeroImage();

	/**
	 * Gets image URL
	 *
	 * @return string
	 */
	public function getHeroImage();

	/**
	 * Checks if category has category
	 *
	 * @return boolean
	 */
	public function hasCategoryTitle();

	/**
	 * Get category title
	 *
	 * @return string
	 */
	public function getCategoryTitle();
}

?>
