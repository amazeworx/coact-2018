<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the single tile area
 */
interface SectionContract_small_cta extends SectionFormatterInterface {

	/**
	 * Is there small cta text
	 *
	 * @return boolean
	 */
	public function hasSmallCtaText();

	/**
	 * Get small cta text
	 * @return array
	 */
	public function getSmallCtaText();

	/**
	 * Check if the cta has a buttons
	 *
	 * @return boolean
	 */

	public function hasCtaButtons();

	/**
	 * Get the cta buttons
	 *
	 * @return array
	 */
	public function getCtaButtons();
}

?>
