<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of a page header section
 */
interface SectionContract_header extends SectionFormatterInterface {

	/**
	 * Does the primary menu have items inside
	 *
	 * @return boolean
	 */

	public function hasPrimaryMenuItems();

	/**
	 * Get primary navigation
	 *
	 * @return array
	 */

	public function getPrimaryMenuItems();

	/**
	 * Is the contact number set
	 *
	 * @return boolean
	 */
	public function hasContactNumber();

	/**
	 * Get the contact number
	 *
	 * @param $stripSpaces - whether to strip spaces out of the contact number
	 * @return boolean
	 */
	public function getContactNumber($stripSpaces);

}

?>
