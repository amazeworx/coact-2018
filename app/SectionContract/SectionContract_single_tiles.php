<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the single tile area
 */
interface SectionContract_single_tiles extends SectionFormatterInterface {

	/**
	 * Is there a single tile image
	 *
	 * @return boolean
	 */
	public function hasSingleTile();

	/**
	 * Gets the single tile image
	 * @return array
	 */
	public function getSingleTile();

}

?>
