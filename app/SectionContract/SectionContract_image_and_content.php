<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of an image and content section
*/
interface SectionContract_image_and_content extends SectionFormatterInterface {

	/**
	* Get the section alignment
	*
	* @return string
	*/
	public function getAlignment();

	/**
	* Is an image set
	*
	* @return boolean
	*/
	public function hasImage();

	/**
	* Get the image url
	*
	* @return string
	*/
	public function getImage();

	/**
	* If a title is set
	*
	* @return boolean
	*/
	public function hasTitle();

	/**
	* Get the title
	*
	* @return string the title
	*/
	public function getTitle();

	/**
	* Is content set
	*
	* @return boolean
	*/
	public function hasContent();

	/**
	* Get the content
	*
	* @return string
	*/
	public function getContent();

	/**
	* Are buttons set
	*
	* @return boolean
	*/
	public function hasButtons();

	/**
	* Show the buttons
	*
	* @return string
	*/
	public function getButtonURL();

	/**
	* Are buttons set
	*
	* @return boolean
	*/
	public function hasButtonLabel();

	/**
	* Show the buttons
	*
	* @return string
	*/
	public function getButtonLabel();

	/**
	* Get the buttons
	*
	* @return array
	*/

	public function getButtons();

}

?>
