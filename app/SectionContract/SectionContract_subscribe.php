<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a page content section
*/
interface SectionContract_subscribe extends SectionFormatterInterface {
	/**
	 * Check if the subscribe section has a value
	 *
	 * @return boolean the section has a value
	 */
	public function hasSubscribeTitle();
	/**
	 * Get the section subscribe
	 *
	 * @return array the subscribe
	 */
	public function getSubscribeTitle();

	/**
	 * Check if the subscribe section has a value
	 *
	 * @return boolean the section has a value
	 */
	public function hasSubscribeContent();
	/**
	 * Get the section subscribe
	 *
	 * @return string the subscribe
	 */
	public function getSubscribeContent();

	/**
	 * Check if the subscribe form has a value
	 *
	 * @return boolean the subscribe form has a value
	 */
	public function hasSubscribeForm();
	/**
	 * Get the section subscribe form ID
	 *
	 * @return string the subscribe form ID
	 */
	public function getSubscribeForm();
}

?>
