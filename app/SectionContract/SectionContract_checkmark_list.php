<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of partner service detail
 */
interface SectionContract_checkmark_list extends SectionFormatterInterface {

	/**
	 * Checks if has checkmark list proof points
	 *
	 * @return boolean true if value is an array AND it has an object inside
	 */
	public function hasProofPoints();

	/**
	 * Gets array of checkmarks
	 *
	 * @return array returns array of proofpoints
	 */
	public function getProofPoints();
}

?>
