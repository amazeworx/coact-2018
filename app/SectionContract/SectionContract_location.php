<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a location details section
*/
interface SectionContract_location extends SectionFormatterInterface {
	/**
	 * Check if the location sup tiytle section has a value
	 *
	 * @return boolean the section has a value
	 */
	public function hasLocationSupTitle();
	/**
	 * Get the location sup title
	 *
	 * @return string the subscribe
	 */
	public function getLocationSupTitle();

	/**
	 * Check if the location site name section has a value
	 *
	 * @return boolean the section has a value
	 */
	public function hasLocationSiteName();
	/**
	 * Get the location site name
	 *
	 * @return string the subscribe
	 */
	public function getLocationSiteName();

	/**
	 * Check if the location google maps section has a value
	 *
	 * @return boolean the section has a value
	 */
	public function hasLocationGoogleMaps();
	/**
	 * Get the google maps area
	 *
	 * @return string the subscribe
	 */
	public function getLocationGoogleMaps();

	/**
	 * Check if the location section has Opening Hours
	 *
	 * @return boolean the section has a value
	 */
	public function hasLocationOpeningHours();

	/**
	 * Get the opening hours
	 *
	 * @return string the opening hours
	 */
	public function getLocationOpeningHours();

	/**
	 * Check if the location section has Opening Hours
	 *
	 * @return boolean the section has a value
	 */
	public function hasLocationBackgroundColour();

	/**
	 * Get the opening hours
	 *
	 * @return string the opening hours
	 */
	public function getLocationBackgroundColour();

	/**
	 * Has contact Numbers
	 *
	 * @return boolean
	 */
	public function hasLocationContactNumbers();

	/**
	 * Get array of contact numbers
	 *
	 * @return array
	 */
	public function getLocationContactNumbers();

	/**
	 * Has contact Numbers
	 *
	 * @return boolean
	 */
	public function hasLocationContactEmails();

	/**
	 * Get array of contact numbers
	 *
	 * @return array
	 */
	public function getLocationContactEmails();

}

?>
