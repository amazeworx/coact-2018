<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the related content area
 */
interface SectionContract_general_tiles extends SectionFormatterInterface {

	/**
	 * Is there related content
	 *
	 * @return boolean
	 */
	public function hasGeneralTiles();

	/**
	 * Gets the Featured content
	 *
	 * @return array
	 */
	public function getGeneralTiles();

}

?>
