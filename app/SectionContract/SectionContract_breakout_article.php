<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of partner service detail
 */
interface SectionContract_breakout_article extends SectionFormatterInterface {

	/**
	 * Checks if has checkmark list proof points
	 *
	 * @return boolean true if value is an array AND it has an object inside
	 */
	public function hasBreakoutArticle();

	/**
	 * Gets array of checkmarks
	 *
	 * @return array returns array of proofpoints
	 */
	public function getBreakoutArticle();
}

?>
