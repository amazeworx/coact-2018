<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the article header area
 */
interface SectionContract_lockedin_header extends SectionFormatterInterface {

	/**
	 * Checks if has article has a title
	 *
	 * @return boolean
	 */
	public function hasArticleTitle();

	/**
	 * Gets image URL
	 *
	 * @return string
	 */
	public function getArticleTitle();

	/**
	 * Checks if has article has a title
	 *
	 * @return boolean
	 */
	public function hasPartnerLogo();

	/**
	 * Gets image URL
	 *
	 * @return string
	 */
	public function getPartnerLogo();
}

?>
