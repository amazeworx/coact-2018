<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the article Tags area
 */
interface SectionContract_iframe extends SectionFormatterInterface {
	/**
	* Is an image set
	*
	* @return boolean
	*/
	public function hasiframeUrl();

	/**
	* Get the image url
	*
	* @return string
	*/
	public function getiframeUrl();
}

?>
