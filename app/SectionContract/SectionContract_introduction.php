<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of a page header section
 */

interface SectionContract_introduction extends SectionFormatterInterface {
	/**
	 * Is content set
	 *
	 * @return boolean
	 */
	public function hasIntroductionText();

	/**
	 * Get the page content
	 *
	 * @return string the page content coming from the WYSIWYG
	 */
	public function getIntroductionText();

	/**
	 * Is content set
	 *
	 * @return boolean
	 */
	public function hasIntroductionHeading();

	/**
	 * Get the page content
	 *
	 * @return string the page content coming from the WYSIWYG
	 */
	public function getIntroductionHeading();
}

?>
