<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of an archive default content
*/
interface SectionContract_archive_content_default extends SectionFormatterInterface {

	/**
	 * Check if the archive has posts into it
	 *
	 * @return boolean true if the archive page has posts
	 */
	public function hasPosts();

	/**
	 * Get the archive page posts
	 *
	 * @return array<WP_Post> the posts in the archive page
	 */
	public function getPosts();

	/**
	 * Gets category name
	 * No need to check as if there was not a category this page would nto show
	 *
	 * @return string true if the pagination is available
	 */
	public function getCategoryName();

	/**
	 * Get the number of post in the category
	 * No need to check as if there was not a category this page would nto show
	 *
	 * @return number pagination
	 */
	public function getCountOfTotalPostsInCategory();

	/**
	 * Check if a pagination is available
	 *
	 * @return boolean true if the pagination is available
	 */
	public function hasPagination();

	/**
	 * Get the pagination content as html string
	 *
	 * @return string pagination
	 */
	public function getPagination();

}

?>
