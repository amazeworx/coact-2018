<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of a page footer section
 */
interface SectionContract_footer extends SectionFormatterInterface {

	/**
	 * Is facebook link set
	 *
	 * @return boolean
	 */
	public function hasFacebookLink();

	/**
	 * Get the facebook link
	 *
	 * @return string the facebook link
	 */
	public function getFacebookLink();

	/**
	 * Is twitter link set
	 *
	 * @return boolean
	 */
	public function hasLinkedInLink();

	/**
	 * Get the twitter link
	 *
	 * @return string the twitter link
	 */
	public function getLinkedInLink();

	/**
	 * Is youtube link set
	 *
	 * @return boolean
	 */
	public function hasYoutubeLink();

	/**
	 * Get the youtube link
	 *
	 * @return string the youtube link
	 */
	public function getYoutubeLink();

}

?>
