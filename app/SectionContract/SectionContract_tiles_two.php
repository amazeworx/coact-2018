<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the related content area
 */
interface SectionContract_tiles_two extends SectionFormatterInterface {

	/**
	 * Is there related content
	 *
	 * @return boolean
	 */
	public function hasFeaturedContent();

	/**
	 * Gets the Featured content
	 *
	 * @return array
	 */
	public function getFeaturedContent();

	/**
	 * Is there Featured content title
	 *
	 * @return boolean
	 */
	public function hasFeaturedContentTitle();

	/**
	 * Gets the Featured content title
	 *
	 * @return string
	 */
	public function getFeaturedContentTitle();

	/**
	 * Is there Featured link
	 *
	 * @return boolean
	 */
	public function hasFeaturedContentLink();

	/**
	 * Gets the Featured link
	 *
	 * @return array
	 */
	public function getFeaturedContentLink();

}

?>
