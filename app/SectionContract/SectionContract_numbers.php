<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a page content section
*/
interface SectionContract_numbers extends SectionFormatterInterface {
	/**
	 * Check if the section has a numbers array
	 *
	 * @return boolean the section has numbers
	 */
	public function hasNumbers();
	/**
	 * Get the section numbers
	 *
	 * @return array the numbers
	 */
	public function getNumbers();
}

?>
