<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of partner service detail
 */
interface SectionContract_partner_hero extends SectionFormatterInterface {

	/**
	 * Check if a hero image is available
	 *
	 * @return boolean true if the hero has a background image
	 */
	public function hasPartnerHeroImage();

	/**
	 * Get the hero background image url
	 *
	 * @return string background image url
	 */
	public function getPartnerHeroImage();

	/**
	 * Check if the page has a partner logo
	 *
	 * @return boolean true if the hero has a logo
	 */
	public function hasPartnerLogo();

	/**
	 * Get the partner logo
	 *
	 * @return string img src of partner logo
	 */
	public function getPartnerLogo();

	/**
	 * Check if partne has siteName
	 *
	 * @return boolean true is the partner name field has a value
	 */
	public function hasPartnerName();

	/**
	 * Get the partner name
	 *
	 * @return string return name of partner
	 */
	public function getPartnerName();


	/**
	 * Check if the partner has a description
	 *
	 * @return boolean true if the partner has a description
	 */
	public function hasPartnerDescription();

	/**
	 * Get the partner description
	 *
	 * @return string returns description content
	 */
	public function getPartnerDescription();


}

?>
