<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the article hero area
 */
interface SectionContract_article_hero extends SectionFormatterInterface {

	/**
	 * Checks if has article has an image
	 *
	 * @return boolean
	 */
	public function hasHeroImage();

	/**
	 * Gets image URL
	 *
	 * @return string
	 */
	public function getHeroImage();

	/**
	 * Checks if article has category
	 *
	 * @return boolean
	 */
	public function hasPostCategory();

	/**
	 * Gets article category
	 *
	 * @return string
	 */
	public function getPostCategory();

	/**
	 * Gets article category
	 *
	 * @return string
	 */
	public function getPostCategoryLink();

	/**
	 * Checks if has article has an title
	 *
	 * @return boolean
	 */
	public function hasPostTitle();

	/**
	 * Gets title
	 *
	 * @return string
	 */
	public function getPostTitle();

	/**
	 * Checks if has article has an introduction
	 *
	 * @return boolean
	 */
	public function hasPostIntroduction();

	/**
	 * Gets introduction
	 *
	 * @return string
	 */
	public function getPostIntroduction();
}

?>
