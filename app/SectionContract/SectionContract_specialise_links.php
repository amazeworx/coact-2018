<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of the single tile area
 */
interface SectionContract_specialise_links extends SectionFormatterInterface {
	/**
	 * Check if the section has a title
	 *
	 * @return boolean
	 */
	public function hasSpecialiseLinks();

	/**
	 * Check if the section has a title
	 *
	 * @return boolean
	 */
	public function getSpecialiseLinks();
}
?>
