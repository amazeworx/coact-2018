<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the article hero area
 */
interface SectionContract_introduction_text extends SectionFormatterInterface {

	/**
	 * Checks if has article has an title
	 *
	 * @return boolean
	 */
	public function hasIntroductionTitle();

	/**
	 * Gets title
	 *
	 * @return boolean
	 */
	public function getIntroductionTitle();

	/**
	 * Checks if has article has an image
	 *
	 * @return boolean
	 */
	public function hasIntroductionText();

	/**
	 * Gets image URL
	 *
	 * @return string
	 */
	public function getIntroductionText();
}

?>
