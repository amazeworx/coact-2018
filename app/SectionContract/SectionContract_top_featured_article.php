<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the formatter of a enquiry form section
 */
interface SectionContract_top_featured_article extends SectionFormatterInterface {

	/**
	 * Is there a featured article
	 *
	 * @return boolean
	 */
	public function hasTopFeaturedArticle();

	/**
	 * Get the featured article data
	 *
	 * @return array
	 */
	public function getTopFeaturedArticle();

	/**
	 * Is there a featured article image
	 *
	 * @return boolean
	 */
	public function hasArticleFeaturedImage();

	/**
	 * Get the featured article image
	 *
	 * @return string
	 */
	public function getArticleFeaturedImage();

	/**
	 * Is there a catorgy attached to the post
	 *
	 * @return boolean
	 */

	public function hasCategoriesAttachedToArticle();

	/**
	 *  Are the categories attached to the post
	 *
	 * @return array
	 */

	public function getCategoriesAttachedToArticle();

	/**
	 *  Does the featured article have a title
	 *
	 * @return boolean
	 */

	public function hasTitleAttachedToArticle();

	/**
	 *
	 *
	 * @return string
	 */

	public function getTitleAttachedToArticle();

	/**
	 *  Does the post have an excerpt
	 *
	 * @return boolean
	 */

	public function hasExcerptAttachedToArticle();

	/**
	 *  Get the posts excerpt
	 *
	 * @return array
	 */

	public function getExcerptAttachedToArticle();

	/**
	 *  Does the featured post have a permink?
	 *
	 * @return boolean
	 */

	public function hasPermalinkAttachedToArticle();

	/**
	 *  get permalink that is attached to post
	 *
	 * @return string
	 */

	public function getPermalinkAttachedToArticle();

}

?>
