<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of an 404 content
*/
interface SectionContract_promotional_cta extends SectionFormatterInterface {
	/**
	* Check if the cta section has a image
	*
	* @return boolean the section has a value
	*/
	public function hasPromotionalData();

	/**
	 * Get the cta image
	 *
	 * @return array
	*/
	public function getPromotionalData();
}

?>
