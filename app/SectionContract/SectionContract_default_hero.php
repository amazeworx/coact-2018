<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
 * Defines the methods available for the article hero area
 */
interface SectionContract_default_hero extends SectionFormatterInterface {

	/**
	 * Checks if has article has an image
	 *
	 * @return boolean
	 */
	public function hasHeroImage();

	/**
	 * Gets image URL
	 *
	 * @return string
	 */
	public function getHeroImage();
}

?>
