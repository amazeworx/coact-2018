<?php
namespace App\SectionContract;

use Frank\SectionFormatter\SectionFormatterInterface;

/**
* Defines the methods available for the formatter of a page content section
*/
interface SectionContract_post_content extends SectionFormatterInterface {
	/**
	 * Has blog post content
	 *
	 * @return boolean
	 */
	public function hasPostTitle();
	/**
	 * Get the blog content
	 *
	 * @return string
	 */
	public function getPostTitle();

	/**
	 * Has blog post title
	 *
	 * @return boolean
	 */
	public function hasPostContent();
	/**
	 * Get the blog title
	 *
	 * @return string
	 */
	public function getPostContent();
}

?>
