<?php
namespace App\TileRenderer;

use Frank\TileRenderer\TileRenderer;

/**
 * Render the tile for a specific post
 */
class TileRenderer_post_latest extends TileRenderer {

	use \Frank\Traits\GetPrimaryTaxonomyTerm;

	public function render() {
		if ($this->post) {
			?>

			<div class="tile_post link_overlay">
				<a class="link_overlay-anchor" href="<?php echo get_permalink($this->post->ID); ?>"></a>
				<div class="tile_post-image" style="background-image: url('<?php echo get_field('featured_image', $this->post->ID); ?>')">

				</div>

				<div class="tile_post-content">
					<div class="tile_post-post_title">
						<a href="<?php echo get_permalink($this->post->ID) ?>">
							<span><?php echo $this->post->post_title; ?></span>
						</a>
					</div>
				</div>
			</div>

			<?php
		} else {
			throw new \Exception("No post available for TileRenderer_post.");
		}
	}

	protected function getTrimmedTitle() {
		$title = $this->post->post_title;

		// Trim the number of words
		$title = wp_trim_words($title, 6);

		return $title;
	}

	protected function getExcerpt() {
		$excerpt = $this->post->post_excerpt;

		if (!$excerpt) {
			$excerpt = $this->post->post_content;
		}

		// Trim the number of words
		$excerpt = wp_trim_words($excerpt, 16);

		return wp_strip_all_tags(apply_filters('get_the_excerpt', $excerpt, $this->post), true);
	}

	protected function getPrimaryCategory() {
		$cat = $this->getPrimaryTaxonomyTerm($this->post->ID, 'category' ) ;

		return $cat->name;
	}

	protected function getCateogoryLink() {
		$cat = $this->getPrimaryTaxonomyTerm($this->post->ID, 'category' ) ;
		$categoryLink = get_term_link($cat);

		return $categoryLink;
	}
}

?>
