<?php
namespace App\TileRenderer;

use Frank\TileRenderer\TileRenderer;

/**
 * Render the tile for a specific post
 */
class TileRenderer_post extends TileRenderer {

	use \Frank\Traits\GetPrimaryTaxonomyTerm;

	public function render() {
		if ($this->post) {
			?>
			<div class="tile_post">
				<div class="tile_post-image_wrapper">
					<div class="tile_post-image" style="background-image: url('<?php echo get_field('featured_image', $this->post->ID); ?>')"></div>
				</div>

				<div class="tile_post-content">
					<div class="tile_post-cat_name">
						<a href="<?php echo $this->getCateogoryLink() ; ?>"><?php echo $this->getPrimaryCategory() ; ?></a>
					</div>

					<div class="tile_post-post_title">
						<a href="<?php echo get_permalink($this->post->ID) ?>">
							<span><?php echo $this->getTrimmedTitle(); ?></span>
						</a>
					</div>

					<div class="tile_post-post_excerpt">
						<span><?php echo $this->getExcerpt(); ?></span>
					</div>

					<div class="tile_post-post_link">
						<a href="<?php echo get_permalink($this->post->ID); ?>">
							<span>Learn More</spam>
						</a>
					</div>
				</div>
			</div>
			<?php
		} else {
			throw new \Exception("No post available for TileRenderer_post.");
		}
	}

	protected function getTrimmedTitle() {
		$title = $this->post->post_title;

		// Trim the number of words
		//$title = wp_trim_words($title, 5);

		$trimmedTitle = (strlen($title) > 25) ? substr($title,0,25).'...' : $title;

		return $trimmedTitle;
	}

	protected function getExcerpt() {
		$excerpt = get_field('post_introduction', $this->post->ID);

		// Trim the number of words
		$trimmedExcerpt = (strlen($excerpt) > 90) ? substr($excerpt,0,90).'...' : $excerpt;

		// return wp_strip_all_tags(apply_filters('get_the_excerpt', $trimmedExcerpt, $this->post), true);
		return $trimmedExcerpt;
	}

	protected function getPrimaryCategory() {
		$cat = $this->getPrimaryTaxonomyTerm($this->post->ID, 'category' ) ;

		return $cat->name;
	}

	protected function getCateogoryLink() {
		$cat = $this->getPrimaryTaxonomyTerm($this->post->ID, 'category' ) ;
		$categoryLink = get_term_link($cat);

		return $categoryLink;
	}
}

?>
