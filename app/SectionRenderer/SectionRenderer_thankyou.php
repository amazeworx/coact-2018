<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_thankyou;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_thankyou extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_thankyou
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_thankyou';

	public function generateSection() { ?>
		<section class="thank_you">
			<div class="container">
				<h2>Thank you for getting in touch</h2>
				<p>One of our team will contact you as soon as possible.</p>

				<a class="button button--secondary" href="/">CoAct Home</a>
			</div>
		</section>
	<?php }
}

?>
