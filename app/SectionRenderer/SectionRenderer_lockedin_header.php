<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_lockedin_header;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_lockedin_header extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_lockedin_header
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_lockedin_header';

	public function generateSection() { ?>
			<div class="lockedin_header-container">
				<?php if ( get_post_type( get_the_ID() ) == 'post' ) : ?>
					<script type="text/javascript">
						websiteData.articleHeader = {
							isPost: true,
							title: '<?php echo addslashes($this->formatter->getArticleTitle()) ; ?>',
							facebook_share: 'https://www.facebook.com/sharer/sharer.php?u=' + location.href,
							twitter_share: 'https://twitter.com/home?status=' + location.href,
							linkedin_share: 'https://www.linkedin.com/shareArticle?mini=true&url=' + location.href,
							homeUrl: '<?php echo home_url() ; ?>',
							themeUrl: '<?php echo get_template_directory_uri() ; ?>',
							contentHubUrl: '<?php echo home_url() ; ?>/content-hub/',
							logoIconURL: '<?php echo get_template_directory_uri(); ?>/assets/icons/misc-coact.svg '
						}
					</script>

					<div lockedin-header unique-parameter="articleHeader"></div>
				<?php endif ; ?>

				<?php if ( get_post_type( get_the_ID() ) == 'service-partner' ) : ?>
					<script type="text/javascript">
						websiteData.articleHeader = {
							isPost: false,
							title: '<?php echo addslashes($this->formatter->getArticleTitle()) ; ?>',
							facebook_share: 'https://www.facebook.com/sharer/sharer.php?u=' + location.href,
							twitter_share: 'https://twitter.com/home?status=' + location.href,
							linkedin_share: 'https://www.linkedin.com/shareArticle?mini=true&url=' + location.href,
							homeUrl: '<?php echo home_url() ; ?>',
							themeUrl: '<?php echo get_template_directory_uri() ; ?>',
							contentHubUrl: '<?php echo home_url() ; ?>/content-hub/',
							logoIconURL: '<?php echo get_template_directory_uri(); ?>/assets/icons/misc-coact.svg ',
							partnerIconURL: '<?php echo $this->formatter->getPartnerLogo(); ?>',
							partnerPhone: '<?php echo $this->formatter->getContactNumber(); ?>',
						}
					</script>

					<div lockedin-header unique-parameter="articleHeader"></div>
				<?php endif ; ?>
			</div>
		<?php
	}
}

?>
