<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_general_tiles;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_general_tiles extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_general_tiles
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_general_tiles';

	public function generateSection() {
		if ($this->formatter->hasGeneralTiles()):
			?>
				<section class="general_tiles">
					<div class="general_tiles-container container">
						<div class="general_tiles-general_tiles_tiles">
							<?php foreach($this->formatter->getGeneralTiles() as $tileData) : ?>
								<?php $this->renderTiles($tileData) ; ?>
							<?php endforeach ; ?>
						</div>
					</div>
				</section>
			<?php
		endif;
	}

	protected function renderTiles($tileData) {


		if (
			isset($tileData['tile_image']) &&
			isset($tileData['tile_title']) &&
			isset($tileData['tile_sup_title'])
		) { ?>
			<div class="tile_post">
				<div class="tile_post-image_wrapper">
					<div class="tile_post-image" style="background-image: url('<?php echo $tileData['tile_image']; ?>')"></div>
				</div>

				<div class="tile_post-content">
					<div class="tile_post-cat_name">
						<h4><?php echo $tileData['tile_sup_title'] ;?></h4>
					</div>

					<div class="tile_post-post_title">
						<?php $ctaData = $tileData['tile_cta'][0] ?>

						<?php if (!$ctaData) : ?>
							<span><?php echo $tileData['tile_title'] ;?></span>
						<?php elseif ($ctaData['button_type'] === 'external') : ?>
							<a href="<?php echo $ctaData['button_url'] ?>">
								<span><?php echo $tileData['tile_title'] ;?></span>
							</a>
						<?php elseif ($ctaData['button_type'] === 'internal') : ?>
							<a href="<?php echo $ctaData['button_url_page'] ?>">
								<span><?php echo $tileData['tile_title'] ;?></span>
							</a>
						<?php endif ; ?>
					</div>

					<div class="tile_post-post_excerpt">
						<span><?php echo $tileData['tile_description'] ;?></span>
					</div>

					<div class="tile_post-post_link">
						<?php
							if ($tileData['tile_cta']) {
								foreach ($tileData['tile_cta'] as $buttonData) {
									$this->generateButton($buttonData, 'text_link');
								}
							}
						?>
					</div>
				</div>
			</div>

		<?php }

	}
}

?>
