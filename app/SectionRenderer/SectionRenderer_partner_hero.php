<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_partner_hero;

/**
* Class defining a default hero
*/
class SectionRenderer_partner_hero extends SectionRenderer {

	/**
	* The section formatter
	*
	* @var \App\SectionContract\SectionContract_partner_hero
	*/
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_partner_hero';

	public function generateSection() {
		if ($this->formatter->hasPartnerHeroImage()) : ?>
			<section class="partner_detail_hero">
				<?php $image = $this->formatter->getPartnerHeroImage(); ?>
				<div class="partner_detail_hero-hero_image" style="background-image: url('<?php echo $image ; ?>')" ></div>

				<?php if ($this->formatter->hasPartnerLogo()) :
					$partnerLogo = $this->formatter->getPartnerLogo();
					$this->renderLogos($partnerLogo);
				endif; ?>

				<div class="partner_detail_hero-partner_details">
					<?php
						$partnerName = $this->formatter->getPartnerName();
						$partnerDesc = $this->formatter->getPartnerDescription();
						$this->renderPartnerInformation($partnerDesc, $partnerName);
					?>
				</div>
			</section>
		<?php endif ; ?>
	<?php }

	protected function renderLogos($logoSrc) {
		if (isset($logoSrc)) :
		?>
		<div class="partner_detail_hero-logo_container">
			<div class="partner_detail_hero-logo_inner">
				<div class="partner_detail_hero-logo">
					<div class="partner_detail_hero-coact_logo">
						<img src="<?php echo get_template_directory_uri() . '/assets/icons/misc-coact.svg' ; ?>" />
					</div>

					<div class="partner_detail_hero-partner_logo">
						<img src="<?php echo $logoSrc ; ?>" alt="Partner Logo" />
					</div>
				</div>
			</div>
		</div>
		<?php endif ;
	}

	protected function renderPartnerInformation($partnerDesc, $partnerName) {
		if (
			isset($partnerDesc) &&
			isset($partnerName)
		) : ?>
			<h1 class="partner_detail_hero-partner_name"><?php echo $partnerName ; ?></h1>

			<div class="partner_detail_hero-partner_description">
				<?php echo $partnerDesc ; ?>
			</div>

			<a href="#enquiry_form" class="partner_detail_hero-button button">Enquire now</a>
		<?php
		endif ;
	}
}
?>
