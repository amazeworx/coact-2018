<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_top_featured_article;

/**
 * Class defining a section renderer
 */
class SectionRenderer_top_featured_article extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \App\SectionContract\SectionContract_top_featured_article
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_top_featured_article';

	public function generateSection() {
		if ( $this->formatter->hasTopFeaturedArticle() ) :
			?>
			<section class="top_featured_article">
				<div class="top_featured_article-image_container">
					<div
						class="top_featured_article-image"
						style="background-image: url('<?php echo $this->formatter->getArticleFeaturedImage() ; ?>')">
					</div>
				</div>

				<div class="top_featured_article-content_container">
					<div class="top_featured_article-content">
						<a class="top_featured_article-category_name" href="<?php echo $this->formatter->getCategoriesLinkAttachedToArticle(); ?>">
							<?php echo $this->formatter->getCategoriesAttachedToArticle(); ?>
						</a>

						<h1 class="top_featured_article-title"><?php echo $this->formatter->getTitleAttachedToArticle(); ?></h1>
						<span class="top_featured_article-excerpt"><?php echo $this->formatter->getExcerptAttachedToArticle(); ?></span>

						<span class="top_featured_article-post_link dark">
							<a href="<?php echo $this->formatter->getPermalinkAttachedToArticle(); ?>">Read more</a>
						</span>
					</div>
				</div>
			</section>
			<?php
		endif;
	}

}

?>
