<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_staff_list;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_staff_list extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_staff_list
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_staff_list';

	public function generateSection() {
		if ($this->formatter->hasStaffList()):
			?>
				<section class="staff_list">
					<div class="staff_list-container container">
						<h4 class="staff_list-title">Meet our wonderful staff</h4>

						<div class="staff_list-profiles">
							<?php foreach($this->formatter->getStaffList() as $staffData) : ?>
								<?php $this->renderStaffProfile($staffData) ; ?>
							<?php endforeach ; ?>
						</div>
					</div>
				</section>
			<?php
		endif;
	}

	protected function renderStaffProfile($staffDetail) {
		if (
			isset($staffDetail['staff_photo']) &&
			isset($staffDetail['staff_name']) &&
			isset($staffDetail['staff_position'])
		) { ?>
			<div class="staff_list-staff_profile">
				<div class="staff_list-headshot" style="background-image: url('<?php echo $staffDetail['staff_photo'] ; ?> ')">

				</div>

				<div class="staff_list-content">
					<div class="staff_list-name">
						<h4><?php echo $staffDetail['staff_position'] ;?></h4>
					</div>

					<div class="staff_list-position">
						<span><?php echo $staffDetail['staff_name'] ;?></span>
					</div>
				</div>
			</div>

		<?php }

	}
}

?>
