<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_small_cta;

/**
 * Class defining a cta section renderer
 */
class SectionRenderer_small_cta extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_small_cta
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_small_cta';

	public function generateSection() { ?>
		<?php if (
			$this->formatter->hasSmallCtaText() &&
			$this->formatter->hasCtaButtons()
		) { ?>
			<div class="cta_small_section">
				<div class="cta_small_section-container container">

					<div class="cta_small_section-text_wrapper">
						<span><?php echo $this->formatter->getSmallCtaText() ; ?></span>
					</div>

					<div class="cta_small_section-button_wrapper">
						<?php
							if ($this->formatter->hasCtaButtons()) {
								foreach ($this->formatter->getCtaButtons() as $buttonData) {
									$this->generateButton($buttonData, 'button cta_section-button');
								}
							}
						?>
					</div>
				</div>
			</div>

		<?php
			}
		}
	}

	?>
