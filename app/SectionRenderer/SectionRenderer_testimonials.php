<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_testimonials;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_testimonials extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_testimonials
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_testimonials';

	public function generateSection() {
		if ($this->formatter->hasTestimonials()) :
			$testimonialData = $this->formatter->getTestimonials() ;
			$testimonialDataJson = json_encode($testimonialData);
			$testimonialDataKey = "testimonialsData_" . hash('crc32', $testimonialDataJson);
			?>

				<script>
					websiteData.<?php echo $testimonialDataKey; ?> = <?php echo $testimonialDataJson; ?>;
				</script>

				<section class="testimonials">
					<div
						class="testimonials_slider"
						testimonial-slider="<?php echo $testimonialDataKey; ?>">
					</div>
				</section>
			<?php
		endif;
	}
}

?>
