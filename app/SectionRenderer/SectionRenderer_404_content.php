<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;

/**
 * Class defining a 404 content
 */
class SectionRenderer_404_content extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_404_content
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_404_content';

	public function generateSection() {
		?>
		<section class="error_section">
			<span class="error_section-shape_right" svg-icon="<?php echo get_template_directory_uri() . '/assets/icons/misc-shape-one.svg' ; ?>"></span>
			<div class="error_section-container">
				<h1 class="error_section-title">
					<span class="purple">4</span><span class="green">0</span><span class="orange">4</span>
				</h1>

				<span class="error_section-content">Oops! Something went wrong</span>

				<a href="/" class="button">Back to home</a>
			</div>
			<span class="error_section-shape_left" svg-icon="<?php echo get_template_directory_uri() . '/assets/icons/misc-shape-three.svg' ; ?>"></span>
		</section>
		<?php
	}

}

?>
