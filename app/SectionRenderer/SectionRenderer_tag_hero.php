<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_tag_hero;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_tag_hero extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_tag_hero
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_tag_hero';

	public function generateSection() {
		if ($this->formatter->hasHeroImage()):
			?>
			<section class="category_hero">
				<div class="category_hero-image" style="background-image: url('<?php echo $this->formatter->getHeroImage() ; ?>')">

				</div>

				<?php if ($this->formatter->hasTagTitle()) : ?>

					<div class="category_hero-container container">
						<div class="category_hero-category_info">
							<h1><?php echo $this->formatter->getTagTitle() ; ?></h1>
						</div>
					</div>

				<?php endif ; ?>
			</section>
			<?php
		endif;
	}
}

?>
