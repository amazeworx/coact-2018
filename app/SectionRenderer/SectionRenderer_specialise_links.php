<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_specialise_links;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_specialise_links extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_specialise_links
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_specialise_links';

	public function generateSection() {
		?>
		<?php if ($this->formatter->hasSpecialiseLinks()) : ?>
			<section class="specialise_links">
				<div class="specialise_links-container container">
					<div class="specialise_links-header">
						<h3>We specialise in the following</h3>
					</div>

					<div class="specialise_links-wrapper">
						<?php $this->generateTiles($this->formatter->getSpecialiseLinks()) ; ?>
					</div>
				</div>
			</section>
		<?php endif ;
	}

	protected function generateTiles($serviceTypes) {
		foreach($serviceTypes as $serviceType) : ?>
			<div class="small_tile_cta">
				<a href="<?php echo $serviceType->guid ?>" class="small_tile_cta-link">
					<span class="small_tile_cta-label">
						<?php echo $serviceType->post_title ?>
					</span>

					<span class="small_tile_cta-icon">
						<span svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/common-expand.svg"></span>
					</span>
				</a>
			</div>
		<?php endforeach ;
	}
}

?>
