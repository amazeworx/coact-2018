<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_subscribe;

/**
 * Class defining a subscribe section renderer
 */
class SectionRenderer_subscribe extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_subscribe
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_subscribe';

	public function generateSection() { ?>
		<section id="subscribe" class="subscribe_section 1">
			<div class="subscribe_section-container">
				<?php if($this->formatter->hasSubscribeTitle()) : ?>
					<header>
						<h3 class="subscribe_section-title">
							<?php echo $this->formatter->getSubscribeTitle() ; ?>
						</h3>
					</header>
				<?php endif; ?>

				<?php if($this->formatter->hasSubscribeContent()) : ?>
					<span class="subscribe_section-detail">
						<?php echo $this->formatter->getSubscribeContent() ; ?>
					</span>
				<?php endif; ?>

				<?php if($this->formatter->hasSubscribeForm()) : ?>
					<div class="subscribe_section-form">
						<?php
							echo do_shortcode("[contact-form-7 id=\"{$this->formatter->getSubscribeForm()}\"]");
						?>
					</span>
				<?php endif; ?>
			</div>
		</section>

		<?php }
	}

	?>
