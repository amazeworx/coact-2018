<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_single_tiles;

/**
 * Class defining a singlew tiles section
 */
class SectionRenderer_single_tiles extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_single_tiles
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_single_tiles';

	public function generateSection() {
		if ($this->formatter->hasSingleTile()):
			?>
			<section class="single_tiles">
				<div class="single_tiles-container container">
					<?php foreach($this->formatter->getSingleTile() as $singleTile) : ?>
						<?php $this->renderSingleTile($singleTile); ?>
					<?php endforeach ; ?>
				</div>
			</section>
			<?php
		endif;
	}

	protected function renderSingleTile($singleTile) {

		if (
			isset($singleTile['tile_image']) &&
			isset($singleTile['tile_sup_title']) &&
			isset($singleTile['tile_title']) &&
			isset($singleTile['tile_description'])
		) { ?>
			<div class="single_tiles-tile">
				<div class="single_tiles-image_wrapper">
					<div class="single_tiles-image" style="background-image: url('<?php echo $singleTile['tile_image'] ; ?>')">
					</div>
				</div>

				<div class="single_tiles-content">
					<div class="single_tiles-sup_title">
						<?php echo $singleTile['tile_sup_title'] ; ?>
					</div>

					<div class="single_tiles-lower_content">
						<div class="single_tiles-title">
							<?php echo $singleTile['tile_title'] ; ?>
						</div>

						<div class="single_tiles-description">
							<span><?php echo $singleTile['tile_description'] ; ?></span>

							<div class="single_tiles-buttons">
								<?php
									if (
										isset($singleTile['tile_cta']) &&
										is_array($singleTile['tile_cta']) &&
										sizeof($singleTile['tile_cta'])
									) {
										foreach ($singleTile['tile_cta'] as $buttonData) {
											$this->generateButton($buttonData, 'text_link');
										}
									}
								?>
							</div>
						</div>
					</div>
				</div>

			</div>

		<?php }
	}


}

?>
