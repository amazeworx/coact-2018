<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_footer;

/**
 * Class defining a section renderer
 */
class SectionRenderer_footer extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \App\SectionContract\SectionContract_footer
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_footer';

	public function generateSection() {
	?>
	</main>
	<footer class="main_footer">
		<div class="main_footer-container">
			<div class="main_footer-social_container">
				<?php if ($this->formatter->hasFacebookLink()) : ?>
				<a href="<?php echo $this->formatter->getFacebookLink(); ?>" class="main_footer-social_links" target="_blank">
					<span class="main_footer-social_icon icon_facebook" svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/social-facebook.svg"></span>
				</a>
				<?php endif; ?>

				<?php if ($this->formatter->hasYoutubeLink()) : ?>
				<a href="<?php echo $this->formatter->getYoutubeLink(); ?>" class="main_footer-social_links" target="_blank">
					<span class="main_footer-social_icon icon_twitter" svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/social-youtube.svg"></span>
				</a>
				<?php endif; ?>

				<?php if ($this->formatter->haslinkedInLink()) : ?>
				<a href="<?php echo $this->formatter->getlinkedInLink(); ?>" class="main_footer-social_links" target="_blank">
					<span class="main_footer-social_icon icon_linkedin" svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/social-linkedin.svg"></span>
				</a>
				<?php endif; ?>
			</div>
			<nav class="main_footer-footer_menu">
				<?php wp_nav_menu( array('theme_location' => 'footer_navigation', 'container' => false)); ?>
			</nav>
		</div>

		<div class="main_footer-post_footer">
			<span>Copyright &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></span>
		</div>
	</footer>
	<?php
	}

}

?>
