<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_subscribe_cta;

/**
 * Class defining a subscribe_cta section renderer
 */
class SectionRenderer_subscribe_cta extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_subscribe_cta
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_subscribe_cta';

	public function generateSection() { ?>
		<span subscribe-cta></span>
		<?php }
	}

	?>
