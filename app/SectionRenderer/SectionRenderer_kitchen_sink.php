<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;

/**
 * Class defining a section renderer
 */
class SectionRenderer_kitchen_sink extends SectionRenderer {

	/**
	 * The section formatter
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = null;

	public function generateSection() {
		?>
		<section class="kitchen_sink">
			<div class="kitchen_sink-inner">

				<h1 class="kitchen_sink-title">Kitchen Sink</h1>

				<div class="kitchen_sink-components">


					<div class="kitchen_sink_components-headers">
						<h2 class="kitchen_sink-section_title">Headers</h2>

						<div>
							<hgroup>
								<h1 class="display1 heading">Display 1</h1>
								<h1 class="display2 heading">Display 2</h1>
								<h1 class="heading"><span>H1</span> HTML5 Kitchen Sink</h1>
								<h2 class="heading"><span>H2</span> Back in my quaint <a href="#">garden</a></h2>
								<h3 class="heading"><span>H3</span> Jaunty <a href="#">zinnias</a> vie with flaunting phlox</h3>
								<h4 class="heading"><span>H4</span> Five or six big jet planes zoomed quickly by the new tower.</h4>
								<h5 class="heading"><span>H5</span> Expect skilled signwriters to use many jazzy, quaint old alphabets effectively.</h5>
								<h6 class="heading"><span>H6</span> Pack my box with five dozen liquor jugs.</h6>
							</hgroup>
						</div>

					</div>


					<div class="kitchen_sink_components-body">
						<hr>

						<h2 class="kitchen_sink-section_title">Body</h2>

						<div>
							<h3>Body1</h3>
							<p class="body1">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex laboriosam harum quam autem veritatis at, tenetur fugit, deleniti cum ratione fuga vitae sapiente. Accusantium nam consequatur, quaerat culpa, reiciendis voluptatem!
							</p>

							<h3>Body2</h3>
							<p class="body2">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex laboriosam harum quam autem veritatis at, tenetur fugit, deleniti cum ratione fuga vitae sapiente. Accusantium nam consequatur, quaerat culpa, reiciendis voluptatem!
							</p>

							<h3>Subheader</h3>
							<p class="subheader">
								Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex laboriosam harum quam autem veritatis at, tenetur fugit, deleniti cum ratione fuga vitae sapiente. Accusantium nam consequatur, quaerat culpa, reiciendis voluptatem!
							</p>
						</div>
					</div>


					<div class="kitchen_sink_components-nav">
						<hr>

						<h2 class="kitchen_sink-section_title">Nav</h2>

						<div>
							<nav>
								<ul>
									<li><a href="#">Home</a></li>
									<li><a href="#">About</a></li>
									<li><a href="#">Contact</a></li>
								</ul>
							</nav>
						</div>
					</div>


					<div class="kitchen_sink_components-table">
						<hr>

						<h2 class="kitchen_sink-section_title">Table</h2>
						<div>
							<table>
								<caption>Tables can have captions now.</caption>
								<tbody>
									<tr>
										<th>Person</th>
										<th>Number</th>
										<th>Third Column</th>
									</tr>
									<tr>
										<td>Someone Lastname</td>
										<td>900</td>
										<td>Nullam quis risus eget urna mollis ornare vel eu leo.</td>
									</tr>
									<tr>
										<td><a href="#">Person Name</a></td>
										<td>1200</td>
										<td>Vestibulum id ligula porta felis euismod semper. Donec ullamcorper nulla non metus auctor fringilla.</td>
									</tr>
									<tr>
										<td>Another Person</td>
										<td>1500</td>
										<td>Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Nullam id dolor id nibh ultricies vehicula ut id elit.</td>
									</tr>
									<tr>
										<td>Last One</td>
										<td>2800</td>
										<td>Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Cras mattis consectetur purus sit amet fermentum.</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>


					<div class="kitchen_sink_components-definition_list">
						<hr>

						<h2 class="kitchen_sink-section_title">Definition list</h2>
						<div>
							<dl>
								<dt>Definition List Title</dt>
								<dd>Definition list division.</dd>
								<dt>Kitchen Sink</dt>
								<dd>Used in expressions to describe work in which all conceivable (and some inconceivable) sources have been mined. In this case, a bunch of markup.</dd>
								<dt>aside</dt>
								<dd>Defines content aside from the page content</dd>
								<dt>blockquote</dt>
								<dd>Defines a section that is quoted from another source</dd>
							</dl>
						</div>
					</div>


					<div class="kitchen_sink_components-unordered_list">
						<hr>

						<h2 class="kitchen_sink-section_title">Unordered list</h2>

						<div>
							<ul>
								<li>Unordered List item one
									<ul>
										<li>Nested list item
											<ul>
												<li>Level 3, item one</li>
												<li>Level 3, item two</li>
												<li>Level 3, item three</li>
												<li>Level 3, item four</li>
											</ul>
										</li>
										<li>List item two</li>
										<li>List item three</li>
										<li>List item four</li>
									</ul>
								</li>
								<li>List item two</li>
								<li>List item three</li>
								<li>List item four</li>
							</ul>
						</div>
					</div>


					<div class="kitchen_sink_components-ordered_list">
						<hr>

						<h2 class="kitchen_sink-section_title">Ordered list</h2>

						<div>
							<ol>
								<li>List item one
									<ol>
									<li>List item one
										<ol>
											<li>List item one</li>
											<li>List item two</li>
											<li>List item three</li>
											<li>List item four</li>
										</ol>
									</li>
									<li>List item two</li>
									<li>List item three</li>
									<li>List item four</li>
									</ol>
								</li>
								<li>List item two</li>
								<li>List item three</li>
								<li>List item four</li>
							</ol>
						</div>
					</div>


					<div class="kitchen_sink_components-address">
						<hr>

						<h2 class="kitchen_sink-section_title">Address</h2>

						<div>
							<address>1 Infinite Loop<br> Cupertino, CA 95014<br> United States</address>
						</div>
					</div>


					<div class="kitchen_sink_components-pre">
						<hr>

						<h2 class="kitchen_sink-section_title">Pre</h2>

						<div>
							<pre>
								pre {
									display: block;
									padding: 7px;
									background-color: #F5F5F5;
									border: 1px solid #E1E1E8;
									border-radius: 3px;
									white-space: pre-wrap;
									word-break: break-all;
									font-family: Menlo, Monaco;
									line-height: 160%;
								}
							</pre>
						</div>
					</div>


					<div class="kitchen_sink_components-figure">
						<hr>

						<h2 class="kitchen_sink-section_title">Figure</h2>

						<div>
							<figure>
								<img src="https://www.fillmurray.com/505/314">
								<figcaption>Fig1. A picture of Bill Murray from <a href="https://www.fillmurray.com/">fillmurray.com</a></figcaption>
							</figure>
						</div>
					</div>


					<div class="kitchen_sink_components-form">
						<hr>

						<h2 class="kitchen_sink-section_title">Form</h2>

						<div>
							<form>
								<div class="form_control-container has-error">
									<label class="form_control-label" for="example-input-email">Email address</label>
									<input type="email" class="form_control-input" id="example-input-email" placeholder="Enter email">
									<div class="form_control-message">
										<span>Please enter a valid email address</span>
									</div>
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-password1">Number</label>
									<input type="number" class="form_control-input" id="example-input-number" placeholder="Number">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-password">Password</label>
									<input type="password" class="form_control-input" id="example-input-password" placeholder="Password">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-search">Search</label>
									<input type="search" class="form_control-input" id="example-input-serach" placeholder="Search ..">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-tel">Telephone number</label>
									<input type="tel" class="form_control-input" id="example-input-tel" placeholder="Telephone number">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-text">Text</label>
									<input type="text" class="form_control-input" id="example-input-text" placeholder="Enter some text here">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-url">Url</label>
									<input type="url" class="form_control-input" id="example-input-url" placeholder="Enter a url here">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-color">Color</label>
									<input type="color" class="form_control-input" id="example-inupt-color" placeholder="#fff">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-date">Date</label>
									<input type="date" class="form_control-input" id="example-input-date" placeholder="date">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-date-time">Date / Time</label>
									<input type="datetime" class="form_control-input" id="example-input-date-time" placeholder="date / time">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-date-time-local">Date / Time local</label>
									<input type="datetime-local" class="form_control-input" id="example-input-date-time-local" placeholder="date / time local">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-month">Month</label>
									<input type="month" class="form_control-input" id="example-input-month" placeholder="Month">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-week">Week</label>
									<input type="week" class="form_control-input" id="example-input-week" placeholder="Week">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-time">Time</label>
									<input type="time" class="form_control-input" id="example-input-time" placeholder="Time">
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-select1">Example select</label>
									<select class="form_control-input" id="example-select1">
										<option>Lorem</option>
										<option>Ipsum</option>
										<option>Dolor</option>
										<option>Sit</option>
										<option>Amet</option>
									</select>
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-select2">Example multiple select</label>
									<select class="form_control-input" multiple id="example-select2">
										<option>Lorem</option>
										<option>Ipsum</option>
										<option>Dolor</option>
										<option>Sit</option>
										<option>Amet</option>
									</select>
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-textarea">Example textarea</label>
									<textarea class="form_control-input" id="example-textarea" rows="3"></textarea>
								</div>

								<div class="form_control-container">
									<label class="form_control-label" for="example-input-file">File input</label>
									<input class="form_control-input" type="file" id="example-input-file">
								</div>

								<fieldset class="form_control-fieldset">
									<legend>I am legend</legend>
									<div class="form_control-container">
										<div class="radio">
											<input type="radio" name="options-radios" id="options-radios-1" class="form_control-input" value="option1" checked>
											<label for="options-radios-1"> Option one is this and that&mdash;be sure to include why it's great </label>
										</div>
										<div class="radio">
											<input type="radio" name="options-radios" id="options-radios-2" class="form_control-input" value="option2">
											<label for="options-radios-2"> Option two can be something else and selecting it will deselect option one </label>
										</div>
										<div class="radio">
											<input type="radio" name="options-radios" id="options-radios-3" class="form_control-input" value="option3" disabled>
											<label for="options-radios-3"> <strong>Option three is disabled</strong> </label>
										</div>
									</div>
								</fieldset>

								<fieldset class="form_control-fieldset">
									<legend>I am also legend</legend>
									<div class="checkbox">
										<input id="example-checkbox-1" type="checkbox">
										<label for="example-checkbox-1"> Check me out </label>
									</div>
									<div class="checkbox">
										<input id="example-checkbox-2" type="checkbox">
										<label for="example-checkbox-2"> Or check me out </label>
									</div>
									<div class="checkbox">
										<input id="example-checkbox-3" type="checkbox" disabled>
										<label for="example-checkbox-3"> <strong>But you can't check me out</strong> </label>
									</div>
								</fieldset>

								<div class="form_control-container">
									<h3 class="kitchen_sink-section_title">Primary Buttons</h3>
									<button class="button" type="button" name="button">Button</button>
									<input class="button" type="button" name="input" value="Input Button">
									<input class="button" type="submit" name="submit" value="Submit Button">
									<input class="button" type="reset" name="reset" value="Reset Button">
								</div>
								<div class="form_control-container">
									<h3 class="kitchen_sink-section_title">Secondary Buttons</h3>
									<button class="button button--secondary" type="button" name="button">Button</button>
									<input class="button button--secondary" type="button" name="input" value="Input Button">
									<input class="button button--secondary" type="submit" name="submit" value="Submit Button">
									<input class="button button--secondary" type="reset" name="reset" value="Reset Button">
								</div>
								<div class="form_control-container">
									<h3 class="kitchen_sink-section_title">Link Buttons</h3>
									<button class="button button--link" type="button" name="button">Button</button>
									<input class="button button--link" type="button" name="input" value="Input Button">
									<input class="button button--link" type="submit" name="submit" value="Submit Button">
									<input class="button button--link" type="reset" name="reset" value="Reset Button">
								</div>
								<div class="form_control-container">
									<h3 class="kitchen_sink-section_title">Default Buttons</h3>
									<button type="button" name="button">Button</button>
									<input type="button" name="input" value="Input Button">
									<input type="submit" name="submit" value="Submit Button">
									<input type="reset" name="reset" value="Reset Button">
								</div>
							</form>
						</div>
					</div>


				</div>
			</div>
		</section>
		<?php
	}

}

?>
