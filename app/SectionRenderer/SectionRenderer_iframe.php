<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_iframe;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_iframe extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_iframe
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_iframe';

	public function generateSection() {
		?>
		<style>iframe{width: 1px;min-width: 100%;}</style>
		<?php if ($this->formatter->hasiframeUrl()) { ?>
			<script src="https://cdnjs.cloudflare.com/ajax/libs/iframe-resizer/3.6.1/iframeResizer.min.js"></script>
			<iframe id="jobIframe" src="<?php echo $this->formatter->getiframeUrl(); ?>" scrolling="no"></iframe>
			<script>
				iFrameResize({log:true}, '#jobIframe');
			</script>
			<?php
		}
	}
}

?>
