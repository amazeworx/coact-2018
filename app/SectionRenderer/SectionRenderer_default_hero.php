<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_default_hero;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_default_hero extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_default_hero
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_default_hero';

	public function generateSection() {
		if ($this->formatter->hasHeroImage()):
			?>
			<section class="default_hero">
				<div class="default_hero-image" style="background-image: url('<?php echo $this->formatter->getHeroImage() ; ?>')">
				</div>
			</section>
			<?php
		endif;
	}
}

?>
