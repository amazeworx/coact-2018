<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_enquiry_form;

/**
 * Class defining a section renderer
 */
class SectionRenderer_contact_form extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \App\SectionContract\SectionContract_enquiry_form
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_enquiry_form';

	public function generateSection() {
		if ( $this->formatter->hasForm() ) :
		?>
		<section class="enquiry_form enquiry_form--contact-page">
			<div class="enquiry_form-container">
				<h2 class="enquiry_form-header">Get in touch</h2>
				<?php echo do_shortcode("[contact-form-7 id=\"{$this->formatter->getForm()}\"]"); ?>
			</div>
		</section>
		<?php
		endif;
	}

}

?>
