<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_numbers;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_numbers extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_numbers
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_numbers';

	public function generateSection() {
		if ($this->formatter->hasNumbers()):
			?>
			<section class="numbers_section">
				<div class="numbers_section-container">
					<?php foreach ($this->formatter->getNumbers() as $numberData): ?>
						<?php $this->renderNumber($numberData); ?>
					<?php endforeach ?>
				</div>
			</section>
			<?php
		endif;
	}

	protected function renderNumber($numberData) {
		if (isset($numberData['colour']) &&
			isset($numberData['number']) &&
			isset($numberData['icon']) &&
			isset($numberData['label'])
		) { ?>
			<div class="numbers_section-statistic color_inherit-<?php echo $numberData['colour']; ?>">
				<div class="numbers_section-icon">
					<img src="<?php echo $numberData['icon']; ?>">
				</div>

				<div class="numbers_section-content">
					<span
						number-ticker
						number-value="<?php echo $numberData['number']; ?>"
						<?php if (isset($numberData['formatter'])) : ?>
						number-formatter="<?php echo $numberData['formatter']; ?>"
						<?php endif ; ?>
						class="numbers_section-stats">
					</span>
					<span class="numbers_section-label"><?php echo $numberData['label']; ?></span>
				</div>
			</div>
			<?php
		}
	}
}

?>
