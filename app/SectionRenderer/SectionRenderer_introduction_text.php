<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_introduction_text;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_introduction_text extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_introduction_text
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_introduction_text';

	public function generateSection() {
		if (
			$this->formatter->hasIntroductionTitle() ||
			$this->formatter->hasIntroductionText()
		) {
		?>
			<section class="introduction_text">
				<div class="introduction_text-container container">
					<div class="introduction_text-text_container">
					<?php if($this->formatter->hasIntroductionTitle()) : ?>
						<h1><?php echo $this->formatter->getIntroductionTitle() ; ?></h1>
					<?php endif ; ?>

					<?php if ($this->formatter->hasIntroductionText()): ?>
						<p><?php echo $this->formatter->getIntroductionText() ; ?></p>
					<?php endif; ?>
				</div>
			</section>
		<?php
		}
	}
}

?>
