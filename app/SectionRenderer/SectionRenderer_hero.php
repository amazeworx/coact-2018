<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_hero;

/**
* Class defining a default hero
*/
class SectionRenderer_hero extends SectionRenderer {

	/**
	* The section formatter
	*
	* @var \App\SectionContract\SectionContract_hero
	*/
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_hero';

	public function generateSection() {
		if ($this->formatter->hasImage()):
			?>
			<div class="hero">
				<div class="hero-image" style="background-image: url('<?php echo $this->formatter->getImage(); ?>')"> </div>
				<div class="hero-body">
					<div class="hero-body_inner">
						<?php if ($this->formatter->hasTitle()): ?>
							<h1 class="hero-title"><?php echo $this->formatter->getTitle(); ?></h1>
						<?php endif; ?>
						<?php if ($this->formatter->hasSubtitle()): ?>
							<h2 class="hero-subtitle">
								<?php echo $this->formatter->getSubtitle(); ?>
							</h2>
						<?php endif; ?>
						<?php if ($this->formatter->getCtas()): ?>
							<div class="hero-ctas">
								<?php foreach ($this->formatter->getCtas() as $cta): ?>
									<a href="<?php echo $cta['cta_url'] ?>" class="button"><?php echo $cta['cta_label'] ?></a>
								<?php endforeach ?>
							</div>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php
		endif;
	}

}

?>
