<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_breakout_article;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_breakout_article extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_breakout_article
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_breakout_article';

	public function generateSection() {
		if ($this->formatter->hasBreakoutArticle()):
			?>
			<section class="breakout_article">
				<div class="breakout_article-container container">
					<?php $this->generatePostsTiles(); ?>
				</div>
			</section>
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasBreakoutArticle()) {
			foreach ($this->formatter->getBreakoutArticle() as $post) {
				// Dynamically call the tiles renderer
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}
		}
	}
}

?>
