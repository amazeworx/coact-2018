<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;

/**
 * Class defining a section renderer
 */
class SectionRenderer_service_locator extends SectionRenderer {

	/**
	 * The section formatter
	 */
	protected $formatter = null;

	public function generateSection() {
	?>
	<div service-locator class="service_locator-wrap"></div>
	<?php
	}

}

?>
