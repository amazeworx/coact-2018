<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_header;
use App\Utilities\SubMenuWrapper;
use App\Utilities\MobileMenu;

/**
 * Class defining a section renderer
 */
class SectionRenderer_header extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \App\SectionContract\SectionContract_header
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_header';

	function my_nav_wrap() {
		// default value of 'items_wrap' is <ul id="%1$s" class="%2$s">%3$s</ul>'

		// open the <ul>, set 'menu_class' and 'menu_id' values
		$wrap  = '<ul id="%1$s" class="%2$s">';

		// get nav items as configured in /wp-admin/
		$wrap .= '%3$s';

		// the static link
		$wrap .= '<li class="my-static-link"><a href="#">My Static Link</a></li>';

		// close the <ul>
		$wrap .= '</ul>';
		// return the result
		return $wrap;
	  }


	public function generateSection() {
	?>
	<header class="header">
		<?php if ( $this->formatter->hasPrimaryMenuItems()) : ?>
			<div class="mega_menu">
				<div class="mega_menu-container container">
					<div class="mega_menu_left-container">
						<?php if ($this->formatter->hasPrimaryMenuItems()) {
							echo $this->formatter->getPrimaryMenuItems() ;
						} ?>
					</div>

					<div class="mega_menu-right_container">
						<?php if ($this->formatter->hasPrimarySideMenuItems()) {
							echo $this->formatter->getPrimarySideMenuItems() ;
						} ?>

						<div class="header-accessibility_wrapper">
							<ul class="menu">
								<li class="accessibility-item" tabindex="0"><a href="#">Accessibility</a></li>
							</ul>

							<div class="header-accessibility-panel">
								<div accessibilty-module></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php endif ; ?>
		<div class="header-primary">
			<div class="header-main container">
				<div class="header-logo">
					<a href="<?php echo home_url() ?>">
						<span svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/misc-coact.svg"></span>
					</a>
				</div>

				<nav class="header-menu">
					<?php if( $this->formatter->isCurrentPageInvidualPage() ) :
							if ( has_nav_menu( 'individual_menu' ) ) :
								wp_nav_menu( array('theme_location' => 'individual_menu', 'walker' => new SubMenuWrapper()) );
							endif ;
						elseif ($this->formatter->isCurrentPageEmployersPage()) :
							if ( has_nav_menu( 'employer_menu' ) ) :
								wp_nav_menu( array('theme_location' => 'employer_menu', 'walker' => new SubMenuWrapper()) );
							endif;
						elseif ($this->formatter->isCurrentPageServicePartnersPage()) :
							if ( has_nav_menu( 'service_partner_menu' ) ) :
								wp_nav_menu( array('theme_location' => 'service_partner_menu', 'walker' => new SubMenuWrapper()) );
							endif;
						elseif ($this->formatter->isCurrentPageReferralPartnersPage()) :
							if ( has_nav_menu( 'referral_partner_menu' ) ) :
								wp_nav_menu( array('theme_location' => 'referral_partner_menu', 'walker' => new SubMenuWrapper()) );
							endif;
						elseif ($this->formatter->isCurrentPageAboutUsPage()) :
							if ( has_nav_menu( 'about_us_menu' ) ) :
								wp_nav_menu( array('theme_location' => 'about_us_menu', 'walker' => new SubMenuWrapper()) );
							endif;
						elseif ($this->formatter->isCurrentPageInnovationsPage()) :
							if ( has_nav_menu( 'innovations_menu' ) ) :
								wp_nav_menu( array('theme_location' => 'innovations_menu', 'walker' => new SubMenuWrapper()) );
							endif;
						endif;

					?>
				</nav>

				<div class="header-menu_button" menu-button></div>

				<div class="header-phone_button">
					<a href="tel:<?php echo $this->formatter->getContactNumber(true); ?>">
						<span svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/common-phone.svg"></span>
						<span class="header-phone_number"><?php echo $this->formatter->getContactNumber(); ?></span>
					</a>
				</div>

				<div class="header-get_in_touch_button">
					<?php
						if ($this->formatter->isCurrentPageInvidualPage() && get_field('get_in_touch_job_seekers','options')) :
							$button = get_field('get_in_touch_job_seekers','options');
						elseif ($this->formatter->isCurrentPageEmployersPage() && get_field('get_in_touch_employers','options')) :
							$button = get_field('get_in_touch_employers','options');
						elseif ($this->formatter->isCurrentPageReferralPartnersPage() && get_field('get_in_touch_referral_partners','options')) :
							$button = get_field('get_in_touch_referral_partners','options');
						elseif ($this->formatter->isCurrentPageServicePartnersPage() && get_field('get_in_touch_service_partners','options')) :
							$button = get_field('get_in_touch_service_partners','options');
						elseif ($this->formatter->isCurrentPageAboutUsPage() && get_field('get_in_touch_about_us','options')) :
							$button = get_field('get_in_touch_about_us','options');
						elseif ($this->formatter->isCurrentPageInnovationsPage() && get_field('get_in_touch_innovation','options')) :
							$button = get_field('get_in_touch_innovation','options');
						else:
							$button['url'] = get_permalink(get_page_by_path('contact-us'));
							$button['title'] = 'Get in touch';
						endif;
					?>
					<a
						href="<?php echo esc_url($button['url']); ?>"
						title="<?php echo esc_html($button['title']); ?>"
						class="button button--primary"
					><?php echo esc_html($button['title']); ?></a>
				</div>

				<div class="header-search_trigger">
					<span menu-search></span>
				</div>
			</div>
		</div>

		<?php
			$mobileMenuItems = $this->formatter->getNavMenuItems();

			if ($mobileMenuItems) :
				$mobileMenuItemsJson = json_encode(array_values($mobileMenuItems)); ?>
				<div class="header_primary-mobile_menu">
					<script>
						websiteData.mobileNavItems = <?php echo $mobileMenuItemsJson; ?>
					</script>

					<div mobile-menu unique-parameter="mobileNavItems"></div>
				</div>
			<?php endif ;
		?>

	</header>

	<div class="overlay">
	</div>

	<main>
	<?php
	}

}

?>
