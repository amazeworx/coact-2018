<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_image_and_content;

/**
 * Class defining a section renderer
 */
class SectionRenderer_image_and_content extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_image_and__content
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_image_and_content';

	public function generateSection() {
		$layout =  $this->formatter->getAlignment();

		$color = (strpos($layout, 'white')  !== false ? 'white_background' : 'purple_background');
		$position = (strpos($layout, 'left') !== false ? 'align_left' : 'align_right');
		$link = null;
		if ($this->formatter->hasButtons()) {
			foreach ($this->formatter->getButtons() as $buttonData) {
				$link = [
					'url' => '',
					'target' => '',
					'rel' => '',
					'label' => $buttonData['button_label'],
				];

				if ($buttonData['button_type'] === 'internal') {
					// Page link
					$link['url'] = (isset($buttonData['button_url_page']))? $buttonData['button_url_page'] : '';
				} else if ($buttonData['button_type'] === 'file') {
					// File
					$link['url'] = (isset($buttonData['button_url_file']))? $buttonData['button_url_file'] : '';
					$link['url'] = wp_get_attachment_url($attachmentId);
					$link['target'] = '_blank';
				} else if ($buttonData['button_type'] === 'custom') {
					// Custom link
					$link['url'] = (isset($buttonData['button_url']))? $buttonData['button_url'] : '';
				} else if ($buttonData['button_type'] === 'external') {
					// External link
					$link['url'] = (isset($buttonData['button_url']))? $buttonData['button_url'] : '';
					$link['rel'] = 'nofollow noopener noreferrer';
					$link['target'] = '_blank';
				}
			}
		}

		$sectionClasses = [];
		$sectionClasses[] = $layout;
		$sectionClasses[] = $position;
		$sectionClasses[] = $color;

		?>

		<div class="image_and_content <?php echo implode(' ', $sectionClasses); ?>">
			<?php if ($link): ?>
				<a class="image_and_content-hover_target" href="<?php echo $link['url']; ?>" target="<?php echo $link['target']; ?>" rel="<?php echo $link['rel']; ?>">
					<?php $this->renderImageLayout($position); ?>
					<?php $this->renderContentLayout($position, $link) ;?>
				</a>
			<?php else: ?>
				<div class="image_and_content-hover_target">
					<?php $this->renderImageLayout($position); ?>
					<?php $this->renderContentLayout($position, $link) ;?>
				</div>
			<?php endif; ?>
		</div>
		<?php
	}

	protected function renderImageLayout($position) {
		if ($this->formatter->hasImage()) : ?>
			<div class="image_and_content-image_container <?php echo $position ;?>">
				<div class="image_and_content-image">
					<div class="image_and_content-image_holder" style="background-image: url(<?php echo $this->formatter->getImage() ; ?>);">
					</div>
				</div>
			</div>
		<?php endif;
	}

	protected function renderContentLayout($position, $link) { ?>
		<?php if ($this->formatter->hasContent()) : ?>
			<div class="image_and_content-content_container <?php echo $position ;?>">
				<div class="image_and_content-content_inner_container">
					<section class="image_and_content-content">
						<?php if ($this->formatter->hasTitle()) : ?>
							<header>
								<h3 class="image_and_content-heading">
									<?php echo $this->formatter->getTitle(); ?>
								</h3>
							</header>
						<?php endif; ?>

						<div class="image_and_content-entry">
							<?php echo $this->formatter->getContent(); ?>
						</div>

						<?php if ($link): ?>
							<span class="text_link"><?php echo $link['label']; ?></span>
						<?php endif; ?>
					</section>
				</div>
			</div>
		<?php endif;
	}

}

?>
