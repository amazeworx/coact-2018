<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_introduction;

/**
 * Class defining a section renderer
 */
class SectionRenderer_introduction extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \App\SectionContract\SectionContract_introduction
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_introduction';

	public function generateSection() {
		if ( $this->formatter->hasIntroductionHeading() ) :
		?>
		<section class="introduction">
			<div class="introduction-container">
				<div class="introduction-heading">
					<span><?php echo $this->formatter->getIntroductionHeading(); ?></span>
				</div>

				<div class="introduction-text">
					<span><?php echo $this->formatter->getIntroductionText(); ?></span>
				</div>
			</div>
		</section>
		<?php
		endif;
	}

}

?>
