<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_cta;

/**
 * Class defining a cta section renderer
 */
class SectionRenderer_cta extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_cta
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_cta';

	public function generateSection() {
	 if ( $this->formatter->hasCtaImage() && $this->formatter->hasCtaTitle() && $this->formatter->hasCtaButtons()  ) : ?>
		<div class="cta_section">
			<div class="cta_section-image_container">
				<div class="cta_section-image" style="background-image: url('<?php echo  $this->formatter->getCtaImage(); ?>')">
				</div>
			</div>

			<div class="cta_section-content_container">
				<div class="cta_section-content_inner">
					<div class="cta_section-content">
						<?php if ($this->formatter->hasCtaIllustration()) : ?>
							<span class="cta_section-illustration" svg-icon="<?php echo $this->formatter->getCtaIllustration(); ?>"></span>
						<?php endif; ?>

						<h3 class="cta_section-heading"><?php echo $this->formatter->getCtaTitle(); ?></h3>

						<?php if ($this->formatter->hasCtaDescription()) : ?>
							<div class="cta_section-description">
								<?php echo  $this->formatter->getCtaDescription(); ?>
							</div>
						<?php endif; ?>

						<?php
						if ($this->formatter->hasCtaButtons()) {
							foreach ($this->formatter->getCtaButtons() as $buttonData) {
								$this->generateButton($buttonData, 'button cta_section-button');
							}
						}
						?>
					</div>
				</div>
			</div>
		</div>

		<?php endif ; }
	}

	?>
