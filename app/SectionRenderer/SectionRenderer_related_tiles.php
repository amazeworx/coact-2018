<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_related_tiles;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_related_tiles extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_related_tiles
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_related_tiles';

	public function generateSection() {
		if (
			$this->formatter->hasTiles()
		) : ?>
			<section class="related_tiles">
				<div class="related_tiles-container container">
					<?php if ($this->formatter->hasTitle()) {
						echo '<h4 class="related_tiles-title">' . $this->formatter->getTitle() . '</h4>';
					} ?>

					<div class="related_tiles-tiles_wrapper">
						<?php $this->generatePostsTiles() ; ?>
					</div>
				</div>
			</section>
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasTiles()) {
			foreach ($this->formatter->getTiles() as $post) {
				if (
					isset($post['related_tile_image']) &&
					isset($post['related_tile_sup_title']) &&
					isset($post['related_tile_title']) &&
					isset($post['related_tile_link'])
				) { ?>

					<div class="tile_post">
						<div class="tile_post-image_wrapper">
							<div class="tile_post-image" style="background-image: url('<?php echo $post['related_tile_image'] ?>')"></div>
						</div>

						<div class="tile_post-content">
							<div class="tile_post-cat_name">
								<?php // if there is no title then apply the link to the sup title ?>
								<?php if ($post['related_tile_title'] == '')  { ?>
									<a href="<?php echo $post['related_tile_link'] ?>">
										<span><?php echo $post['related_tile_sup_title'] ; ; ?></span>
									</a>
								<?php } else  { ?>
									<span><?php echo $post['related_tile_sup_title'] ; ; ?></span>
								<?php } ?>
							</div>

							<?php if (!$post['related_tile_title'] == '') { ?>
								<div class="tile_post-post_title">
									<a href="<?php echo $post['related_tile_link'] ?>">
										<span><?php echo $post['related_tile_title'] ?></span>
									</a>
								</div>
							<?php } ?>
						</div>
					</div>
					<?php
				}
			}
		}
	}
}

