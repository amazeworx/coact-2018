<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_featured_articles;

/**
 * Class defining a numbers featured articles
 */
class SectionRenderer_featured_articles extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_featured_articles
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_featured_articles';

	public function generateSection() {
		if ($this->formatter->hasFeaturedArticles()):
			?>

			<section class="featured_articles with_sidebar">
				<div class="featured_articles-content_container">
					<div class="featured_articles-inner_content_container">
						<div class="featured_articles-content">
							<div class="featured_articles-select_container">

								<script>
									websiteData.contentHubCategoryOptions = <?php echo json_encode($this->formatter->getListOfCats()); ?>
								</script>

								<div category-select unique-parameter="contentHubCategoryOptions"></div>
								</div>

							</div>

							<div class="featured_articles-content_articles">
								<?php $this->generatePostsTiles(); ?>
							</div>
						</div>
					</div>
				</div>

				<sidebar class="featured_articles-sidebar_container">
					<div class="featured_articles-sidebar">
						<h3 class="featured_articles-sidebar_title">Most Recent</h3>
						<?php $this->generateLatestPostsTiles(); ?>
					</div>
				</sidebar>
			</section>
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasFeaturedArticles()) {
			foreach ($this->formatter->getFeaturedArticles() as $post) {
				// Dynamically call the tiles renderer
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}
		}
	}

	protected function generateLatestPostsTiles() {

			foreach ($this->formatter->getLatestPosts() as $post) {
				// Update the post type to use a different renderer
				$post->post_type = 'post_latest';
				// Dynamically call the tiles renderer
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}

	}
}
?>
