<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_contact_tiles;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_contact_tiles extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_contact_tiles
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_contact_tiles';

	public function generateSection() {
		if ($this->formatter->hasContactTiles()):
			?>
			<section class="contact_tiles">
				<div class="contact_tiles-container container">
					<div class="contact_tiles-tiles_container">
						<?php foreach ($this->formatter->getContactTiles() as $tile): ?>
							<?php $this->renderContactTile($tile); ?>
						<?php endforeach ?>
					</div>
				</div>
			</section>
			<?php
		endif;
	}

	protected function renderContactTile($tile) {
		if (
			isset($tile['tile_title']) && isset($tile['link_type'])
		) { ?>
			<div class="contact_tiles-tile link_overlay <?php echo $tile['link_type']; ?>">
				<?php if ($tile['link_type'] == 'email') : ?>
					<a class="link_overlay-anchor" href="mailto:<?php echo $tile['tile_link'] ; ?>"></a>
				<?php elseif ($tile['link_type'] == 'phone') : ?>
					<a class="link_overlay-anchor" href="tel:<?php echo $tile['tile_link'] ; ?>"></a>
				<?php elseif ($tile['link_type'] == 'anchor') : ?>
					<a class="link_overlay-anchor" href="#<?php echo $tile['tile_link'] ; ?>"></a>
				<?php endif ; ?>

				<h3 class="contact_tiles-tile_title"><?php echo $tile['tile_title'] ; ?></h3>

				<div class="contact_tiles-tile_label">
					<?php if ($tile['link_type'] == 'email') : ?>
						<span class="contact_tiles-icon" svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/article-header-mail-icon.svg"></span>
						<span><?php echo $tile['tile_link_label'] ;?></span>
					<?php elseif ($tile['link_type'] == 'phone') : ?>
						<span class="contact_tiles-icon" svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/common-phone.svg"></span>
						<span><?php echo $tile['tile_link_label'] ;?></span>
					<?php elseif ($tile['link_type'] == 'anchor') : ?>
						<span class="contact_tiles-icon" svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/article-header-mail-icon.svg"></span>
						<span class="text_link"><?php echo $tile['tile_link_label'] ;?></span>
					<?php endif ; ?>
				</div>
			</div>
		<?php
		}
	}
}

?>
