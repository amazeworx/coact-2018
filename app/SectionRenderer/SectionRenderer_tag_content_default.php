<?php
namespace Frank\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;


/**
 * Class defining a default archive content
 */
class SectionRenderer_tag_content_default extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_tag_content_default
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_tag_content_default';

	public function generateSection() {
		if ($this->formatter->hasPosts()):
			?>
			<section class="archive_content">
				<div class="archive_content-container container">
					<div class="archive_content-post_count">
						<span><?php echo $this->formatter->getCountOfTotalPostsInTag() ; ?> <?php echo ($this->formatter->isPostPlural() ? 'posts' : 'post'); ?> in the <?php echo $this->formatter->getTagName() ; ?> tag</span>
					</div>
					<div class="archive_content-tiles_wrapper">
						<?php $this->generatePostsTiles(); ?>
					</div>
					<?php if ($this->formatter->hasPagination()): ?>
						<div class="archive_content-pagination_wrapper">
							<?php echo $this->formatter->getPagination(); ?>
						</div>
					<?php endif; ?>
				</div>
		</section>
			<?php
		else:
			?>
			No posts are available.
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasPosts()) {
			foreach ($this->formatter->getPosts() as $post) {
				// Dynamically call the tiles renderer
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}
		}
	}

}

?>
