<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_employers_logos;

/**
 * Class defining a section renderer
 */
class SectionRenderer_employers_logos extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \App\SectionContract\SectionContract_employers_logos
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_employers_logos';

	public function generateSection() {
		if ( $this->formatter->hasLogos() ) :
		?>
		<section class="employers_logos">
			<div class="employers_logos-container">
				<div class="employers_logos-header">
					<h3>Employers we work with</h3>
				</div>

				<div class="employers_logos-logo_container">
					<?php foreach ($this->formatter->getLogos() as $logoData): ?>
						<?php $this->renderLogo($logoData); ?>
					<?php endforeach ?>
				</div>
			</div>
		</section>
		<?php
		endif;
	}

	public function renderLogo($logo) {
		if (isset($logo['employers_logo']) ) : ?>
			<div class="employers_logos-logo_single">
				<img src="<?php echo $logo['employers_logo'] ; ?>" />
			</div>
		<?php endif ;
	}

}

?>
