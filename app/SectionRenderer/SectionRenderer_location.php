<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_location;

/**
 * Class defining a location section renderer
 */
class SectionRenderer_location extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_location
	 */
	protected $formatter = null;
	protected $googleMapDataKey = null;
	protected $googleMapDataJson = null;

	protected $mapAddress = null;
	protected $mapLat = null;
	protected $mapLng = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_location';

	public function generateSection() {
		$this->googleMapData = $this->formatter->getLocationGoogleMaps() ;
		$this->googleMapDataJson = json_encode($this->googleMapData);
		$this->googleMapDataKey = "googleMapData_" . hash('crc32', $this->googleMapDataJson);

		$this->mapAddress = $this->googleMapData['address'];
		$this->mapLat = $this->googleMapData['lat'];
		$this->mapLng = $this->googleMapData['lng'];

		if($this->formatter->getLocationBackgroundColour()) :
			if ($this->formatter->getLocationBackgroundColour() === 'purple') {
				$this->renderPurpleBackground() ;
			} elseif ($this->formatter->getLocationBackgroundColour() === 'white') {
				$this->renderWhiteBackground() ;
			}
		endif ;
	}

	protected function renderWhiteBackground() { ?>
		<section class="location_section <?php echo $this->formatter->getLocationBackgroundColour() ; ?>">
			<div class="location_section-divider">
				<?php $this->renderGoogleMap() ; ?>
			</div>

			<div class="location_section-divider">
				<?php $this->renderLocationInfomation() ; ?>
			</div>
		</section>
	<?php }

	protected function renderPurpleBackground() { ?>
		<section class="location_section <?php echo $this->formatter->getLocationBackgroundColour() ; ?>">
			<div class="location_section-divider location_section-divider--content">
				<?php $this->renderLocationInfomation() ; ?>
			</div>

			<div class="location_section-divider">
				<?php $this->renderGoogleMap() ; ?>
			</div>
		</section>
	<?php }

	protected function renderGoogleMap() { ?>
		<script>
			websiteData.<?php echo $this->googleMapDataKey; ?> = <?php echo $this->googleMapDataJson; ?>;
		</script>
		<div
			class="location_section-container_half google_maps"
			location-marker
			map-address="<?php echo $this->mapAddress ?>"
			map-lat="<?php echo $this->mapLat ?>"
			map-lng="<?php echo $this->mapLng ?>"
		>
		</div>
	<?php }


	protected function renderLocationInfomation() { ?>
		<div class="location_section-inner_content">

			<?php if ($this->formatter->hasLocationSiteName()) : ?>
				<div class="location_section-location_heading">
					<?php if ($this->formatter->hasLocationSupTitle()) {
						echo '<h5 class="subheader">' . $this->formatter->getLocationSupTitle() . '</h5>';
					} ?>

					<h2><?php echo $this->formatter->getLocationSiteName() ; ?></h2>
				</div>
			<?php endif ; ?>

			<div class="location_section-information">
				<?php
					if (
						$this->formatter->hasLocationGoogleMaps()
					) {

						$locationDetails = $this->formatter->getLocationGoogleMaps() ?>
						<span
							class="location_section-location_details_icon"
							svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/common-location.svg">
						</span>

						<span class="location_section-location_address">
							<?php echo $locationDetails['address'] ; ?>
						</span>

						<?php $getDirectLink =  urlencode($this->mapAddress) ; ?>
						<a target="_blank"
							href="<?php echo 'https://www.google.com/maps/search/?api=1&query=' . $getDirectLink ; ?>"
							class="text-link dark">
							Get Directions
						</a>

						<?php
					}
				?>
			</div>
			<?php
				if ($this->formatter->hasLocationContactNumbers()) {
					$numbers = $this->formatter->getLocationContactNumbers();
					$this->renderTelephoneNumbers($numbers) ;
				}
			?>

			<?php
				if ($this->formatter->hasLocationContactEmails()) {
					$emails = $this->formatter->getLocationContactEmails();
					$this->renderEmailAddresses($emails) ;
				}
			?>

			<?php
				if ($this->formatter->hasLocationOpeningHours()) {
					$hours = $this->formatter->getLocationOpeningHours();
					$this->renderOpeningHours($hours) ;
				}
			?>
		</div>
	<?php }

	protected function renderTelephoneNumbers($numbers) { ?>
		<div class="location_section-information">
			<span
				class="location_section-location_details_icon location_section-location_details_icon--phone"
				svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/common-phone.svg">
			</span>

			<ul>
				<?php foreach($numbers as $number) {
					$telLink = str_replace(' ', '', $number['phone_number']);
					echo '<li>' . $number['phone_label'] . ': ' . '<a href="tel:' . $telLink . '">' . $number['phone_number'] . '</a></li>';
				} ?>
			</ul>
		</div>
	<?php
	}

	protected function renderEmailAddresses($emails) { ?>
		<div class="location_section-information">
			<span
				class="location_section-location_details_icon location_section-location_details_icon--email"
				svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/article-header-mail-icon.svg">
			</span>

			<ul>
				<?php foreach($emails as $email) {
					$emailLink = str_replace(' ', '', $email['email_address']);
					echo '<li>' . $email['email_label'] . ': ' . '<a href="mailto:' . $emailLink . '">' . $email['email_address'] . '</a></li>';
				} ?>
			</ul>
		</div>
	<?php
	}

	protected function renderOpeningHours($timeFrames) { ?>
		<div class="location_section-information">
			<span
				class="location_section-location_details_icon location_section-location_details_icon--clock"
				svg-icon="<?php echo get_template_directory_uri(); ?>/assets/icons/common-clock-white.svg">
			</span>
			<p>
				<?php echo $timeFrames; ?>
			</p>
		</div>
	<?php
	}
}
?>
