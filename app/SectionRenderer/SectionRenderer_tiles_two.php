<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_tiles_two;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_tiles_two extends SectionRenderer {

	use \Frank\Traits\GenerateButton;

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_tiles_two
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_tiles_two';

	public function generateSection() {
		if ($this->formatter->hasFeaturedContent()):
			?>
				<section class="featured_content tiles_two">
					<div class="featured_content-container container">
						<div class="featured_content-pre_container">
							<div class="featured_content-title">
								<?php if($this->formatter->hasFeaturedContentTitle()) : ?>
									<h4><?php echo $this->formatter->getFeaturedContentTitle() ; ?></h4>
								<?php endif ; ?>
							</div>

							<div class="featured_content-link">
								<?php
									if ($this->formatter->hasFeaturedContentLink()) {
										foreach ($this->formatter->getFeaturedContentLink() as $buttonData) {
											$this->generateButton($buttonData, 'text_link');
										}
									}
								?>
							</div>
						</div>

						<div class="featured_content-featured_content_tiles">
							<?php $this->generatePostsTiles(); ?>
						</div>

						<div class="featured_content-link show_mobile">
							<?php
								if ($this->formatter->hasFeaturedContentLink()) {
									foreach ($this->formatter->getFeaturedContentLink() as $buttonData) {
										$this->generateButton($buttonData, 'text_link');
									}
								}
							?>
						</div>
					</div>
				</section>
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasFeaturedContent()) {
			foreach ($this->formatter->getFeaturedContent() as $post) {
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}
		}
	}
}

?>
