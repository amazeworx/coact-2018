<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;

/**
 * Class defining a section renderer
 */
class SectionRenderer_service_finder extends SectionRenderer {

	/**
	 * The section formatter
	 */
	protected $formatter = null;

    function finderContent() {
        ?>
        <?php echo $this->finderTaxonomyName(); ?>
        <div class="service_locator-wrap">
            <div class="service_locator">
                <div class="service_locator-content_wrapper">
                    <div class="service_locator-progress">
                        <div class="linear-progress-bar">
                            <div class="bar bar1"></div>
                            <div class="bar bar2"></div>
                        </div>
                    </div>
                    <div class="service_locator-content">
                        <div class="service_locator-sidepane">
                            <div class="service_locator-search">
                                <div class="service_locator-panel">
                                    <h2 class="service_locator-title">Find your nearest service provider</h2>
                                    <?php echo $this->finderSelectService(); ?>
                                    <?php echo $this->finderPostCodeInput(); ?>
                                </div>
                                <div class="service_locator-view-mode">
                                    <button id="view-as-list" class="active" type="button">View as List</button><button id="view-on-map" class="" type="button">View on Map</button>
                                </div>
                            </div>
                            <div id="service_locator-listing" class="service_locator-listing">
                                <h3 class="service_locator-listing_title h4">Showing all Service Partners</h3>
                                <ul class="service_locator-listing_tabs"></ul>
                            </div>
                        </div>
                        <div id="service_locator-map" class="service_locator-map">
                        </div>
                    </div>
                </div>
                <div class="service_locator-details closed">
                    <div class="service_locator-details_panel">
                        <div>
                            <header class="service_locator-details_header">
                                <button class="button button--link back-button">
                                    <span class="svg_icon back-arrow"><div><div>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="injected-svg" data-src="<?php echo get_stylesheet_directory_uri() ?>/assets/icons/common-arrow.svg"><path d="M0 0h24v24H0z" fill="none"></path><path d="M20 11H7.83l5.59-5.59L12 4l-8 8 8 8 1.41-1.41L7.83 13H20v-2z"></path></svg>
                                    </div></div></span>
                                    <span>Back to results</span>
                                </button>
                                <button class="button button--link close-button">
                                    <span class="svg_icon back-close"><div><div>
                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" class="injected-svg" data-src="<?php echo get_stylesheet_directory_uri() ?>/assets/icons/common-close.svg"><title>icon / close</title><g fill="none" fill-rule="evenodd"><path d="M24 0H0v24h24z"></path><path d="M18.455 5.57L5.579 18.445M5.57 5.57l12.875 12.875" stroke="#B2519E" stroke-linecap="square" stroke-width="2"></path></g></svg></div></div>
                                    </span>
                                </button>
                            </header>
                            <div class="service_locator-details_body"></div>
                            <footer class="service_locator-details_footer"></footer>
                        </div>
                    </div>
                    <div class="service_locator-details_overlay"></div>
                </div>
            </div>
        </div>

        <?php
    }

    function finderTaxonomyName() {

        $output = '';

        $taxonomies = get_terms( array(
            'taxonomy' => 'service_types',
            'ignore_term_order' => TRUE,
            'orderby' => 'count',
            'order' => 'DESC',
            'hide_empty' => true,
        ) );

        if ( !empty($taxonomies) ) :

            $output .= '<script type="text/javascript">';

            $output .= 'var service_types_tax = [';

            foreach( $taxonomies as $category ) {

                $term_id = esc_attr( $category->term_id );
                $term_name = esc_attr( $category->name );

                $output .= '{"term_id":"' . $term_id . '","term_name":"' . $term_name . '"},';

            }

            $output .= '];';

            //$output .= 'console.log(service_types_tax);';

            $output .= '</script>';

        endif;

        return $output;
    }

    function finderSelectService() {
        $output = '';
        $output .= '<select class="select_service_type">';

        $taxonomies = get_terms( array(
            'taxonomy' => 'service_types',
            'ignore_term_order' => TRUE,
            'orderby' => 'count',
            'order' => 'DESC',
            'hide_empty' => true,
        ) );

        if ( !empty($taxonomies) ) :

            $output .= '<option value="">Select a service type</option>';

            foreach( $taxonomies as $category ) {

                $output .= '<option value="' . esc_attr( $category->term_id ) . '">'. esc_attr( $category->name ) .'</option>';

            }
        endif;

        $output .= '</select>';

        return $output;
    }

    function finderPostCodeInput() {
        $output = '<div class="service_locator-postcode">';
        $output .= '<input id="input_postcode" type="text" placeholder="Or enter your postcode" />';
        $output .= '<div role="button" class="btn-clear"><svg height="20" width="20" viewBox="0 0 20 20" class="clear-icon" focusable="false" role="presentation"><path d="M14.348 14.849c-0.469 0.469-1.229 0.469-1.697 0l-2.651-3.030-2.651 3.029c-0.469 0.469-1.229 0.469-1.697 0-0.469-0.469-0.469-1.229 0-1.697l2.758-3.15-2.759-3.152c-0.469-0.469-0.469-1.228 0-1.697s1.228-0.469 1.697 0l2.652 3.031 2.651-3.031c0.469-0.469 1.228-0.469 1.697 0s0.469 1.229 0 1.697l-2.758 3.152 2.758 3.15c0.469 0.469 0.469 1.229 0 1.698z"></path></svg></div>';
        $output .= '</div>';
        return $output;
    }

    function finderSelectServiceType() {
        $output = '';
        $output .= '<div class="mdc-select mdc-select--filled mb-4 w-full">';
        $output .= '<div class="mdc-select__anchor" role="button" aria-haspopup="listbox" aria-expanded="false" aria-labelledby="service-label service-selected-text">';
            $output .= '<span class="mdc-select__ripple"></span>';
            $output .= '<span id="service-label" class="mdc-floating-label">Select a service</span>';
            $output .= '<span class="mdc-select__selected-text-container">';
                $output .= '<span id="select-service-selected-text" class="mdc-select__selected-text"></span>';
            $output .= '</span>';

            $output .= '<span class="mdc-select__dropdown-icon">';
                $output .= '<svg class="mdc-select__dropdown-icon-graphic" viewBox="7 10 10 5" focusable="false">';
                    $output .= '<polygon class="mdc-select__dropdown-icon-inactive" stroke="none" fill-rule="evenodd" points="7 10 12 15 17 10"></polygon>';
                    $output .= '<polygon class="mdc-select__dropdown-icon-active" stroke="none" fill-rule="evenodd" points="7 15 12 10 17 15"></polygon>';
                $output .= '</svg>';
            $output .= '</span>';
            $output .= '<span class="mdc-line-ripple"></span>';
        $output .= '</div>';

        $output .= '<div class="mdc-select__menu mdc-menu mdc-menu-surface mdc-menu-surface--fullwidth">';
            $output .= '<ul class="mdc-list" role="listbox" aria-label="Services listbox">';

                $taxonomies = get_terms( array(
                    'taxonomy' => 'service_types',
                    'hide_empty' => true,
                ) );

                if ( !empty($taxonomies) ) :
                    $output .= '<li class="mdc-list-item mdc-list-item--selected" aria-selected="true" data-value="" role="option">';
                        $output .= '<span class="mdc-list-item__ripple"></span>';
                    $output .= '</li>';

                    foreach( $taxonomies as $category ) {

                        $output .= '<li class="mdc-list-item" aria-selected="false" data-value="'. esc_attr( $category->term_id ) .'" role="option">';
                            $output .= '<span class="mdc-list-item__ripple"></span>';
                            $output .= '<span class="mdc-list-item__text">'. esc_attr( $category->name ) .'</span>';
                        $output .= '</li>';

                    }
                endif;

            $output .= '</ul>';
            $output .= '</div>';
        $output .= '</div>';

        return $output;
    }

	public function generateSection() {
        $this->finderContent();
	}

}

?>
