<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_search;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_search extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_search
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_search';

	public function generateSection() {
		?>
		<section class="search_section">
			<div search-results>

			<?php $this->algoliaInstantsearchTemplate(); ?>
		</section>
		<?php
	}

	protected function algoliaInstantsearchTemplate() {
		// Content for this section directly adapted from the algolia plugin's template/instantsearch.php file
		?>
		<div class="search_hero">
			<div class="search_hero-search_box">
				<h1 class="search_hero-title">Search Results</h1>
				<div id="search_form" class="search_form"></div>
			</div>
		</div>

		<div class="search_results">
			<div class="search_results-container container">
				<div id="stats-container" class="stats-container"></div>
				<div id="algolia-hits" class="algolia-hits"></div>
				<div id="algolia-pagination" class="algolia-pagination"></div>
			</div>
		</div>

		<script type="text/html" id="tmpl-instantsearch-hit">
			<div class="search_result">
				<# if ( data.post_image ) { #>
				<div class="search_result-image" style="background-image: url('{{{data.post_image}}}')">

				</div>
				<# } #>

				<div class="search_result-content">
					<# if ( data._highlightResult['taxonomies'] ) { #>
						<# if ( data._highlightResult.taxonomies.category ) { #>
							<div class="search_result-taxonomy">
								{{{ data._highlightResult.taxonomies.category[0].value }}}
							</div>
						<# } #>

						<# if ( data._highlightResult.taxonomies.service_types ) { #>
							<div class="search_result-taxonomy">
								{{{ data._highlightResult.taxonomies.service_types[0].value }}}
							</div>
						<# } #>
					<# } #>

					<div class="search_result-post_tile">
						<span>{{{ data._highlightResult.post_title.value }}}</span>
					</div>

					<# if ( data._highlightResult['taxonomies'] ) { #>
						<div class="search_result-post_except">
							{{{ data.content }}}
						</div>
					<# } #>

					<div class="search_result-link">
						<a class="text_link" href="{{ data.permalink }}">Learn More</a>
					</div>
				</div>
			</div>
		</script>
		<?php
	}
}

?>
