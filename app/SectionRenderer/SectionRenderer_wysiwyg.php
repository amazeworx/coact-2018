<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\SectionContract\SectionContract_wysiwyg;

/**
 * Class defining a WYSIWYG section renderer
 */
class SectionRenderer_wysiwyg extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_wysiwyg
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'Frank\SectionContract\SectionContract_wysiwyg';

	public function generateSection() {

	if ($this->formatter->hasContent()):
		?>

		<section class="post_content js-article">
				<div class="post_content-container container">
					<div class="post_content-content">
						<div class="post_content-body body_content">
							<?php echo $this->formatter->getContent(); ?>
						</div>
					</div>
				</div>
			</section>
		<?php
	endif;

	}

}

?>
