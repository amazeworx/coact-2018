<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_promotional_cta;

/**
 * Class defining a cta section renderer
 */
class SectionRenderer_promotional_cta extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_promotional_cta
	 */
	protected $formatter = null;
	protected $promotionalData = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_promotional_cta';

	public function generateSection() {
		$this->promotionalData = $this->formatter->getPromotionalData()
	?>
		<script type="text/javascript">
			websiteData.promotionalCtaData = <?php echo json_encode($this->promotionalData) ?>
		</script>

		<div promotional-cta unique-identifier="promotionalCtaData"></div>
	<?php
	}
} ?>
