<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_article_hero;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_article_hero extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_article_hero
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_article_hero';

	public function generateSection() {
		if ($this->formatter->hasHeroImage()):
			?>
			<section class="article_hero">
				<div class="article_hero-image" style="background-image: url('<?php echo $this->formatter->getHeroImage() ; ?>')">

				</div>

				<div class="article_hero-container container">
					<div class="article_hero-article_info">
						<?php if ($this->formatter->hasPostCategory()): ?>
							<span class="article_hero-category">
								<a href="<?php echo $this->formatter->getPostCategoryLink() ; ?>"><?php echo $this->formatter->getPostCategory() ; ?></a>
							</span>
						<?php endif ; ?>

						<?php if ($this->formatter->hasHeroImage()): ?>
							<h1><?php echo $this->formatter->getPostTitle() ; ?></h1>
						<?php endif ; ?>

						<?php if ($this->formatter->hasPostIntroduction()): ?>
							<h3><?php echo $this->formatter->getPostIntroduction() ; ?></h3>
						<?php endif ; ?>
					</div>
				</div>
			</section>
			<?php
		endif;
	}
}

?>
