<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_lower_featured_articles;

/**
 * Class defining a numbers featured articles
 */
class SectionRenderer_lower_featured_articles extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_lower_featured_articles
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_lower_featured_articles';

	public function generateSection() {
		if ($this->formatter->hasFeaturedArticles()):
			?>

			<section class="lower_featured_articles">
				<div class="lower_featured_articles-container container">
					<?php $this->generatePostsTiles(); ?>
				</div>
			</section>
			<?php
		endif;
	}

	/**
	 * Generate the tiles for the section content
	 */
	protected function generatePostsTiles() {
		if ($this->formatter->hasFeaturedArticles()) {
			foreach ($this->formatter->getFeaturedArticles() as $post) {
				// Dynamically call the tiles renderer
				$tileRenderer = new TileRendererFactory();
				$tileRenderer->setPost($post);
				$tileRenderer->render();
			}
		}
	}
}
?>
