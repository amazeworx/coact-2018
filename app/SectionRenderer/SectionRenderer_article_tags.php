<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_article_tags;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_article_tags extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_article_tags
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_article_tags';

	public function generateSection() {
		if($this->formatter->hasArticleTags()) :
			?>
			<section class="article_tags">
				<div class="article_tags-container container">
					<ul class="article_tags-tag_list">
					<?php foreach($this->formatter->getArticleTags() as $tag) : ?>

						<li class="article_tags-tag">
							<a href="<?php echo $tag['tag_permalink'] ; ?>"><?php echo $tag['tag_name'] ; ?></a>
						</li>

					<?php endforeach ; ?>
					</ul>
				</div>
			</section>
			<?php
		endif ;

	}
}

?>
