<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_checkmark_list;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_checkmark_list extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_checkmark_list
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_checkmark_list';

	public function generateSection() {
		if ($this->formatter->hasProofPoints()):
			?>
			<section class="checkmark_section">
				<div class="checkmark_section-container container">
					<?php foreach ($this->formatter->getProofPoints() as $checkmarkData): ?>
						<?php $this->renderProofPoint($checkmarkData); ?>
					<?php endforeach ?>
				</div>
			</section>
			<?php
		endif;
	}

	protected function renderProofPoint($checkmarkData) {
		if (isset($checkmarkData['point'])
		) { ?>
			<div class="checkmark_section-single_checkmark">
				<span
					svg-icon="<?php echo get_template_directory_uri() . '/assets/icons/common-checkmark_tick.svg' ; ?>"
					class="checkmark_section-icon"
					alt="Checkmark Icon">
				</span>

				<p><?php echo $checkmarkData['point'] ; ?></p>
			</div>
		<?php
		}
	}
}

?>
