<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use Frank\TileRenderer\TileRendererFactory;
use App\SectionContract\SectionContract_category_hero;

/**
 * Class defining a numbers section renderer
 */
class SectionRenderer_category_hero extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_category_hero
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_category_hero';

	public function generateSection() {
		if ($this->formatter->hasHeroImage()):
			?>
			<section class="category_hero">
				<div class="category_hero-image" style="background-image: url('<?php echo $this->formatter->getHeroImage() ; ?>')">

				</div>

				<?php if ($this->formatter->hasCategoryTitle()) : ?>

					<div class="category_hero-container container">
						<div class="category_hero-category_info">
							<h1><?php echo $this->formatter->getCategoryTitle() ; ?></h1>
						</div>
					</div>

				<?php endif ; ?>
			</section>
			<?php
		endif;
	}
}

?>
