<?php
namespace App\SectionRenderer;

use Frank\SectionRenderer\SectionRenderer;
use App\SectionContract\SectionContract_post_content;

/**
 * Class defining a post_content section renderer
 */
class SectionRenderer_post_content extends SectionRenderer {

	/**
	 * The section formatter
	 *
	 * @var \Frank\SectionContract\SectionContract_post_content
	 */
	protected $formatter = null;

	protected $sectionFormatterContract = 'App\SectionContract\SectionContract_post_content';

	public function generateSection() {
		if ($this->formatter->hasPostContent()):
			?>
			<section class="post_content js-article">
				<div class="post_content-container container">
					<div class="post_content-content">
						<div class="post_content-body body_content">
							<?php echo apply_filters('the_content', $this->formatter->getPostContent()) ; ?>
						</div>
					</div>
				</div>
			</section>
			<?php
		endif;
	}
}

?>
