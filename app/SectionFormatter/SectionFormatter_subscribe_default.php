<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_subscribe_default;

/**
 * Class defining a section renderer
 */
class SectionFormatter_subscribe_default extends SectionFormatter implements SectionContract_subscribe_default {

	protected $subscribeTitle = null;
	protected $subscribeContent = null;
	protected $subscribeForm = null;

	public function initialiseFlexibleData() {
		$this->subscribeTitle = get_field('subscribe_title', 'options');
		$this->subscribeContent = get_field('subscribe_content', 'options');
		$this->subscribeForm = get_field('subscribe_form', 'options');
	}

	public function initialiseData() {
		$this->subscribeTitle = get_field('subscribe_title', 'options');
		$this->subscribeContent = get_field('subscribe_content', 'options');
		$this->subscribeForm = get_field('subscribe_form', 'options');
	}

	public function hasSubscribeTitle() {
		return !!$this->subscribeTitle;
	}

	public function getSubscribeTitle() {
		return $this->subscribeTitle;
	}

	public function hasSubscribeContent() {
		return !!$this->subscribeContent;
	}

	public function getSubscribeContent() {
		return $this->subscribeContent;
	}

	public function hasSubscribeForm() {
		return !!$this->subscribeForm;
	}

	public function getSubscribeForm() {
		return $this->subscribeForm;
	}
}

?>
