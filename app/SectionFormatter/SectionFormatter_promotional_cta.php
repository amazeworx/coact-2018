<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_promotional_cta;

/**
 * Class defining a section renderer
 */
class SectionFormatter_promotional_cta extends SectionFormatter implements SectionContract_promotional_cta {

	protected $defaultPromotionalData = null;
	protected $customPromotionalData = null;

	protected $data = null;

	public function initialiseData() {
		$this->defaultPromotionalData = get_field('article_default_promotion_cta', 'options');
		$this->customPromotionalData = get_field('article_custom_promotion_cta', $this->post->ID);

		$this->data = $this->defaultPromotionalData;

		if (
			is_array($this->customPromotionalData) &&
			sizeof($this->customPromotionalData)
		) {
			$this->data = $this->customPromotionalData;
		}
	}

	public function hasPromotionalData() {
		return !!$this->data;
	}

	public function getPromotionalData() {
		return $this->data;
	}
}
