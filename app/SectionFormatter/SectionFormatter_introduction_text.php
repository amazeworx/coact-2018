<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_introduction_text;

/**
 * Class defining a section renderer
 */
class SectionFormatter_introduction_text extends SectionFormatter implements SectionContract_introduction_text {

	protected $introductionTitle = null;
	protected $introductionText = null;

	public function initialiseData() {
		$this->introductionTitle = get_field('introduction_title', $this->post->ID);
		$this->introductionText = get_field('introduction_text', $this->post->ID);
	}

	public function hasIntroductionTitle() {
		return !!$this->introductionTitle;
	}

	public function getIntroductionTitle() {
		return $this->introductionTitle;
	}

	public function hasIntroductionText() {
		return !!$this->introductionText;
	}

	public function getIntroductionText() {
		return $this->introductionText;
	}
}
