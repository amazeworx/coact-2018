<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_breakout_article;

/**
 * Class defining a section renderer
 */
class SectionFormatter_breakout_article extends SectionFormatter implements SectionContract_breakout_article {

	protected $breakoutArticle = null;

	public function initialiseData() {
		$this->breakoutArticle = get_field('breakout_article', $this->post->ID);
	}

	public function hasBreakoutArticle() {
		return !!$this->breakoutArticle;
	}

	public function getBreakoutArticle() {
		return $this->breakoutArticle;
	}
}
