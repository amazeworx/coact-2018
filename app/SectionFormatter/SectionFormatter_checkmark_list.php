<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_checkmark_list;

/**
 * Class defining a section renderer
 */
class SectionFormatter_checkmark_list extends SectionFormatter implements SectionContract_checkmark_list {

	protected $numbers = null;

	public function initialiseData() {
		$this->proofPoints = get_field('checkmark_list', $this->post->ID);
	}

	public function hasProofPoints() {
		return (
			is_array($this->proofPoints) &&
			sizeof($this->proofPoints)
		);
	}

	public function getProofPoints() {
		return $this->proofPoints;
	}
}
