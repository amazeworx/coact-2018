<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_featured_articles;

/**
 * Class defining a section renderer
 */
class SectionFormatter_featured_articles extends SectionFormatter implements SectionContract_featured_articles {

	protected $featuredArticles = null;
	protected $featuredArticlesImage = null;
	protected $latestArticles = null;
	protected $listCategories = null;
	protected $listCategoriesWithURL = null;

	public function initialiseData() {
		$allFeaturedArticles = get_field('featured_articles', $this->post->ID);
		if (is_array($allFeaturedArticles)) {
			$this->featuredArticles = array_slice($allFeaturedArticles, 0, 4);
		}

		$args = array(
			'posts_per_page'   => 5,
			'orderby'          => 'date',
			'order'            => 'DESC',
		);

		$latestArticles = get_posts($args);

		if (is_array($latestArticles)) {
			$this->latestArticles = $latestArticles;
		}

		$this->listCategories = $this->initialiseListCategories();
	}

	public function hasFeaturedArticles() {
		return (
			is_array($this->featuredArticles) &&
			sizeof($this->featuredArticles)
		);
	}

	public function getFeaturedArticles() {
		return $this->featuredArticles;
	}

	public function hasLatestPosts() {
		return !!$this->latestArticles;
	}

	public function getLatestPosts() {
		return $this->latestArticles;
	}

	public function getListOfCats() {
		return $this->listCategories;
	}

	protected function initialiseListCategories() {
		$terms = get_terms([
			'taxonomy' => 'category',
			'orderby' => 'name',
			'order'   => 'ASC'
		]);

		if (
			is_array($terms) &&
			sizeof($terms)
		) {
			$result = [];

			foreach($terms as $singleTerm) {
				$result[] = [
					'value' =>  get_category_link($singleTerm->term_id),
					'label' => $singleTerm->name,
				];
			}

			return $result;
		}

		return null;
	}
}
