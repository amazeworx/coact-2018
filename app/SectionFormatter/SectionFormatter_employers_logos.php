<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_employers_logos;

/**
 * Class defining a section renderer
 */
class SectionFormatter_employers_logos extends SectionFormatter implements SectionContract_employers_logos {

	protected $content = null;

	public function initialiseData() {
		$employers_logos = get_field('employers_logos', $this->post->ID);

		$this->content = $employers_logos;
	}

	public function hasLogos() {
		return !!$this->content;
	}

	public function getLogos() {
		return $this->content;
	}
}

?>
