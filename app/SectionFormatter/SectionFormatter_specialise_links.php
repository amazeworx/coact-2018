<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_specialise_links;

/**
 * Class defining a section renderer
 */
class SectionFormatter_specialise_links extends SectionFormatter implements SectionContract_specialise_links {
	protected $specialise_links = null;

	public function initialiseData() {
		$this->specialise_links = get_field('specialise_links', $this->post->ID);
	}

	public function hasSpecialiseLinks() {
		return (
			is_array($this->specialise_links) &&
			sizeof($this->specialise_links)
		);
	}

	public function getSpecialiseLinks() {
		return $this->specialise_links;
	}
}
?>
