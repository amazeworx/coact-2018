<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_iframe;

/**
 * Class defining a section renderer
 */
class SectionFormatter_iframe extends SectionFormatter implements SectionContract_iframe {

	protected $iframeUrl = null;

	public function initialiseFlexibleData() {
		$this->iframeUrl = $this->getFlexibleSectionDataByKey('iframe_url');
	}

	public function hasiframeUrl() {
		return !!$this->iframeUrl;
	}

	public function getiframeUrl() {
		return $this->iframeUrl;
	}
}
