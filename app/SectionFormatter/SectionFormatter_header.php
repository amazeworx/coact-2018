<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_header;


/**
 * Class defining a section renderer
 */
class SectionFormatter_header extends SectionFormatter implements SectionContract_header {

	protected $individualPageId = null;
	protected $employersPageId = null;
	protected $servicePartnersPageId = null;
	protected $referralPartnersPageId = null;
	protected $aboutUsPageId = null;
	protected $innovationsPageId = null;

	protected $primaryNavMenuItems = null;
	protected $primaryNavSideMenuItems = null;
	protected $pageAncestors = null;

	protected $showIndivualMenu = null;
	protected $showEmployersMenu = null;
	protected $showServicePartnersMenu = null;
	protected $showreferralPartnersMenu = null;
	protected $showAboutUsMenu = null;
	protected $showInnovationsMenu = null;
	protected $mobileNavMenuItems = null;

	public function initialiseData() {
		$this->individualPageId = get_field('individual_page', 'options');
		$this->employersPageId = get_field('employers_page', 'options');
		$this->servicePartnersPageId = get_field('service_partners', 'options');
		$this->referralPartnersPageId = get_field('referral_partners', 'options');
		$this->aboutUsPageId = get_field('about_us', 'options');
		$this->innovationsPageId = get_field('innovations', 'options');

		$this->contactNumber = get_field('contact_number', 'options');
		$this->primaryNavMenuItems = wp_get_nav_menu_items( 'Primary Navigation' );
		$this->primaryNavSideMenuItems = wp_get_nav_menu_items( 'Primary Navigation Side' );

		$this->pageAncestors = $this->getAncestorsOfCurrentPage();

		// Gets top level parent if not null
		if (
			empty($this->pageAncestors) &&
			isset($this->individualPageId)
		) {
			$currentPageObject = $this->getCurrentPageData();

			if (
				isset($currentPageObject) &&
				$currentPageObject->ID === $this->individualPageId
			)  {
				$this->showIndivualMenu = true;
			} elseif (
				isset($currentPageObject) &&
				$currentPageObject->ID === $this->employersPageId
			) {
				$this->showEmployersMenu = true;
			} elseif (
				isset($currentPageObject) &&
				$currentPageObject->ID === $this->servicePartnersPageId
			) {
				$this->showServicePartnersMenu = true;
			} elseif (
				isset($currentPageObject) &&
				$currentPageObject->ID === $this->referralPartnersPageId
			) {
				$this->showreferralPartnersMenu = true;
			} elseif (
				isset($currentPageObject) &&
				$currentPageObject->ID === $this->aboutUsPageId
			) {
				$this->showAboutUsMenu = true;
			} elseif (
				isset($currentPageObject) &&
				$currentPageObject->ID === $this->innovationsPageId
			) {
				$this->showInnovationsMenu = true;
			}
		} elseif (
			is_array($this->pageAncestors) &&
			sizeof($this->pageAncestors)
		) {
			//Is not top level
			$lastAncestor = array_values(array_slice($this->pageAncestors, -1))[0];

			//Get all children of top level parent
			$children = $this->getChildren($lastAncestor);

			foreach($children as $child) {
				if (
					$child->post_parent === $lastAncestor
				) {
					if ($lastAncestor == $this->individualPageId) {
						$this->showIndivualMenu = true;
					} elseif ($lastAncestor == $this->employersPageId) {
						$this->showEmployersMenu = true;
					} elseif ($lastAncestor == $this->servicePartnersPageId) {
						$this->showServicePartnersMenu = true;
					} elseif ($lastAncestor == $this->referralPartnersPageId) {
						$this->showreferralPartnersMenu = true;
					} elseif ($lastAncestor == $this->aboutUsPageId) {
						$this->showAboutUsMenu = true;
					} elseif ($lastAncestor == $this->innovationsPageId) {
						$this->showInnovationsMenu = true;
					}
				}
			}
		}

		// build Mobile menu
		$fullDataTree = $this->buildNavMenuItem( 'Mobile Navigation' );
		$this->mobileNavMenuItems = $this->formatDataTree($fullDataTree);
	}

	public function hasPrimaryMenuItems() {
		return (
			is_array($this->primaryNavMenuItems) &&
			sizeof($this->primaryNavMenuItems)
		);
	}

	public function getPrimaryMenuItems() {
		$args = array(
			'theme_location' => 'primary_navigation',
			'echo'           => false,
		);

		return wp_nav_menu( $args );
	}

	public function hasPrimarySideMenuItems() {
		return (
			is_array($this->primaryNavSideMenuItems) &&
			sizeof($this->primaryNavSideMenuItems)
		);
	}

	public function getPrimarySideMenuItems() {
		$args = array(
			'theme_location' => 'primary_navigation_side',
			'echo'           => false,
		);

		return wp_nav_menu( $args );
	}

	public function isCurrentPageInvidualPage() {
		return $this->showIndivualMenu;
	}

	public function isCurrentPageEmployersPage() {
		return $this->showEmployersMenu;
	}

	public function isCurrentPageServicePartnersPage() {
		return $this->showServicePartnersMenu;
	}

	public function isCurrentPageReferralPartnersPage() {
		return $this->showreferralPartnersMenu;
	}

	public function isCurrentPageAboutUsPage() {
		return $this->showAboutUsMenu;
	}

	public function isCurrentPageInnovationsPage() {
		return $this->showInnovationsMenu;
	}

	public function hasContactNumber() {
		return !!$this->contactNumber;
	}

	public function getContactNumber($stripSpaces = false) {
		if ($stripSpaces === true) {
			return preg_replace('/\s/', '', $this->contactNumber);
		}
		return $this->contactNumber;
	}

	public function getNavMenuItems() {
		return $this->mobileNavMenuItems;
	}

	protected function getCurrentPageData() {
		$pageObject = get_queried_object();
		return $pageObject;
	}

	protected function getAncestorsOfCurrentPage() {
		$pageObject = $this->getCurrentPageData();

		$ancestorsOfCurrentPage = get_post_ancestors($pageObject);
		return $ancestorsOfCurrentPage;
	}

	protected function getChildren($ancestor) {
		$args = array(
			'post_type' => 'page',
			'posts_per_page'   => -1,
		);

		$pages = get_posts($args);

		// Filter through all pages and find Portfolio's children
		$children = get_page_children( $ancestor, $pages );

		return $children;
	}

	protected function formatDataTree($tree) {
		if (isset($tree)) {
			$treeArray = array_values($tree);
			$result = [];
			if (
				is_array($treeArray) &&
				sizeof($treeArray)
			) {
				foreach($treeArray as $branch) {
					$formattedBranch = [
						'id' =>  $branch->ID,
						'title' => $branch->title,
						'url' => $branch->url,
						'children' => null
					];

					if (
						isset($branch->wpse_children) &&
						is_array($branch->wpse_children) &&
						sizeof($branch->wpse_children)
					) {
						$formattedBranch['children'] = $this->formatDataTree($branch->wpse_children);
					}

					$result[] = $formattedBranch;
				}
			}

			return $result;
		}

		return null;;
	}

	protected function buildNavMenuItem( $menuName ){
		$items = wp_get_nav_menu_items( $menuName );
		return  $items ? $this->buildTree( $items, 0 ) : null;
	}

	protected function buildtree(array &$elements, $parentId = 0 ) {
		$branch = array();
		foreach ( $elements as &$element )
		{
			if ( $element->menu_item_parent == $parentId )
			{
				$children = $this->buildTree( $elements, $element->ID );
				if ( $children )
					$element->wpse_children = array_values($children);

				$branch[$element->ID] = $element;
				unset( $element );
			}
		}
		return $branch;
	}

}

?>
