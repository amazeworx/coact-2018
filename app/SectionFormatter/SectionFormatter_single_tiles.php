<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_single_tiles;

/**
 * Class defining a section renderer
 */
class SectionFormatter_single_tiles extends SectionFormatter implements SectionContract_single_tiles {

	protected $singleTiles = null;

	public function initialiseFlexibleData() {
		$this->singleTiles = $this->getFlexibleSectionDataByKey('single_tiles');
	}

	public function hasSingleTile() {
		return !!$this->singleTiles;
	}

	public function getSingleTile() {
		return $this->singleTiles;
	}
}
