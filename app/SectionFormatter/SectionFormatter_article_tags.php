<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_article_tags;

/**
 * Class defining a section renderer
 */
class SectionFormatter_article_tags extends SectionFormatter implements SectionContract_article_tags {

	protected $articleTags = null;

	public function initialiseData() {
		$this->articleTags = $this->initArticleTags();
	}

	public function hasArticleTags() {
		return (
			is_array($this->articleTags) &&
			sizeof($this->articleTags)
		);
	}

	public function getArticleTags() {
		return $this->articleTags;
	}

	public function getArticleTagLink() {
		return $this->articleTags;
	}

	public function initArticleTags() {
		$tags = get_the_tags($this->post->ID);

		if (
			is_array($tags) &&
			sizeof($tags)
		) {
			$result = [];

			foreach ($tags as $tag) {
				$result[] = [
					'tag_name' => $tag->name,
					'tag_permalink' => get_tag_link($tag->term_id),
				];
			}

			return $result;
		}

		return null;
	}
}
