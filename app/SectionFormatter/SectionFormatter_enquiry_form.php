<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_enquiry_form;

/**
 * Class defining a section renderer
 */
class SectionFormatter_enquiry_form extends SectionFormatter implements SectionContract_enquiry_form {

	protected $enquiryForm = null;

	public function initialiseData() {
		$this->initialiseDataGeneral();
	}

	public function initialiseFlexibleData() {
		$enquiryFormRaw = $this->getFlexibleSectionDataByKey('enquiry_form');
		if (
			is_array($enquiryFormRaw) &&
			sizeof($enquiryFormRaw)
		) {
			$this->enquiryForm = $enquiryFormRaw[0];
		}
		if (!$this->enquiryForm) {
			$this->initialiseDataGeneral();
		}
	}

	protected function initialiseDataGeneral() {
		$this->enquiryForm = get_field('enquiry_form', 'options');
	}

	public function hasForm() {
		return !!$this->enquiryForm;
	}

	public function getForm() {
		return $this->enquiryForm;
	}
}

?>
