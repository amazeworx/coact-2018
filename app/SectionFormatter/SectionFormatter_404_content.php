<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use Frank\SectionContract\SectionContract_404_content;


/**
 * Class defining a 404 content
 */
class SectionFormatter_404_content extends SectionFormatter implements SectionContract_404_content {

	public function initialiseData() {
		// Nothing to do
	}

}

?>
