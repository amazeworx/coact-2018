<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_location;

/**
 * Class defining a section renderer
 */
class SectionFormatter_location extends SectionFormatter implements SectionContract_location {

	protected $locationSupTitle = null;
	protected $locationSiteName = null;
	protected $locationGoogleMaps = null;
	protected $locationOpeningHours = null;
	protected $locationBackgroundColour = null;
	protected $locationContactNumbers = null;
	protected $locationContactEmails = null;

	public function initialiseData() {
		$this->locationSupTitle = get_field('sup_header', $this->post->ID);
		$this->locationSiteName = get_field('location_site_name', $this->post->ID);
		$this->locationGoogleMaps = get_field('location', $this->post->ID);
		$this->locationOpeningHours = get_field('opening_hours', $this->post->ID);
		$this->locationBackgroundColour = get_field('location_background_colour', $this->post->ID);
		$this->locationContactNumbers = get_field('contact_numbers', $this->post->ID);
		$this->locationContactEmails = get_field('contact_email', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->locationSupTitle = $this->getFlexibleSectionDataByKey('sup_header');
		$this->locationSiteName = $this->getFlexibleSectionDataByKey('location_site_name');
		$this->locationGoogleMaps = $this->getFlexibleSectionDataByKey('location');
		$this->locationOpeningHours = $this->getFlexibleSectionDataByKey('opening_hours');
		$this->locationBackgroundColour = $this->getFlexibleSectionDataByKey('location_background_colour');
		$this->locationContactNumbers = $this->getFlexibleSectionDataByKey('contact_numbers');
		$this->locationContactEmails = $this->getFlexibleSectionDataByKey('contact_email');
	}

	public function hasLocationSupTitle() {
		return !!$this->locationSupTitle;
	}

	public function getLocationSupTitle() {
		return $this->locationSupTitle;
	}

	public function hasLocationSiteName() {
		return !!$this->locationSiteName;
	}

	public function getLocationSiteName() {
		return $this->locationSiteName;
	}

	public function hasLocationGoogleMaps() {
		return !!$this->locationGoogleMaps;
	}

	public function getLocationGoogleMaps() {
		return $this->locationGoogleMaps;
	}

	public function hasLocationBackgroundColour() {
		return !!$this->locationBackgroundColour;
	}

	public function getLocationBackgroundColour() {
		return $this->locationBackgroundColour;
	}

	public function hasLocationOpeningHours() {
		return (
			!empty($this->locationOpeningHours)
		);
	}

	public function getLocationOpeningHours() {
		return $this->locationOpeningHours;
	}

	public function hasLocationContactNumbers() {
		return (
			is_array($this->locationContactNumbers) &&
			sizeof($this->locationContactNumbers)
		);
	}

	public function getLocationContactNumbers() {
		return $this->locationContactNumbers;
	}

	public function hasLocationContactEmails() {
		return (
			is_array($this->locationContactEmails) &&
			sizeof($this->locationContactEmails)
		);
	}

	public function getLocationContactEmails() {
		return $this->locationContactEmails;
	}
}

?>
