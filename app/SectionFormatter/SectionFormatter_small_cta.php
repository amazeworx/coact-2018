<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_small_cta;

/**
 * Class defining a section renderer
 */
class SectionFormatter_small_cta extends SectionFormatter implements SectionContract_small_cta {

	protected $ctaText = null;
	protected $ctaButton = null;

	public function initialiseData() {
		$this->initialiseDataGeneral();
	}

	public function initialiseFlexibleData() {
		$this->initialiseDataGeneral();
	}

	protected function initialiseDataGeneral() {
		$this->ctaText = $this->getFlexibleSectionDataByKey('small_cta_description');
		$this->ctaButton = $this->getFlexibleSectionDataByKey('small_cta_button');
	}

	public function hasSmallCtaText() {
		return !!$this->ctaText;
	}

	public function getSmallCtaText() {
		return $this->ctaText;
	}

	// Change to this is array
	public function hasCtaButtons() {
		return !!$this->ctaButton;
	}

	public function getCtaButtons() {
		return $this->ctaButton;
	}
}

?>
