<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_lower_featured_articles;

/**
 * Class defining a section renderer
 */
class SectionFormatter_lower_featured_articles extends SectionFormatter implements SectionContract_lower_featured_articles {

	protected $featuredArticles = null;
	protected $featuredArticlesImage = null;
	protected $latestArticles = null;
	protected $listCategories = null;
	protected $listCategoriesWithURL = null;

	public function initialiseData() {
		$allFeaturedArticles = get_field('featured_articles', $this->post->ID);
		if (is_array($allFeaturedArticles)) {

			$lengthOfPosts = count($allFeaturedArticles);
			$postOffset = null;

			// Set array correct array offset if there are less than 13 total posts
			if ($lengthOfPosts >= 13) {
				$postOffset = 9;
			} else {
				$postOffset = $lengthOfPosts - 4;
			}

			$this->featuredArticles = array_slice($allFeaturedArticles, 4, $postOffset);
		}

		$categories = get_categories( array(
			'orderby' => 'name',
			'order'   => 'ASC'
		) );

		if (is_array($categories)) {
			$this->listCategories = $categories;
		}
	}

	public function hasFeaturedArticles() {
		return (
			is_array($this->featuredArticles) &&
			sizeof($this->featuredArticles)
		);
	}

	public function getFeaturedArticles() {
		return $this->featuredArticles;
	}

	public function getListOfCats() {
		$listCats = $this->listCategories;
		$newArrray = [];

		foreach($listCats as $key => $cat) {
			$key = (int) $key;

			$catTerm = get_category_link($cat->term_id);

			$listCats[$key]->cat_url = $catTerm;
		}

		$this->listCategories = $listCats;
		return $this->listCategories;
	}
}
