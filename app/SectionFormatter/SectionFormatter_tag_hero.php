<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_tag_hero;

/**
 * Class defining a section renderer
 */
class SectionFormatter_tag_hero extends SectionFormatter implements SectionContract_tag_hero {

	protected $heroImage = null;
	protected $tagData = null;

	public function initialiseData() {
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		$this->heroImage = get_field('category_featured_image', 'category_'.$term_id);

		if (is_null($this->heroImage)) {
			$this->heroImage = get_field('default_image', 'options');
		}

		$this->tagData = $this->initTagName();
	}

	public function hasHeroImage() {
		return !!$this->heroImage;
	}

	public function getHeroImage() {
		return $this->heroImage;
	}

	public function hasTagTitle() {
		return !!$this->tagData->name;
	}

	public function getTagTitle() {
		return $this->tagData->name;
	}

	protected function initTagName() {
		$tagData = get_queried_object();

		return $tagData;
	}
}
