<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_subscribe;

/**
 * Class defining a section renderer
 */
class SectionFormatter_subscribe extends SectionFormatter implements SectionContract_subscribe {

	protected $subscribeTitle = null;
	protected $subscribeContent = null;
	protected $subscribeForm = null;

	public function initialiseData() {
		$this->subscribeTitle = get_field('subscribe_form_title', $this->post->ID);
		$this->subscribeContent = get_field('subscribe_form_content', $this->post->ID);
		$this->subscribeForm = get_field('subscribe_form', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->subscribeTitle = $this->getFlexibleSectionDataByKey('subscribe_form_title');
		$this->subscribeContent = $this->getFlexibleSectionDataByKey('subscribe_form_content');
		$this->subscribeForm = $this->getFlexibleSectionDataByKey('subscribe_form');
	}
	public function hasSubscribeTitle() {
		return !!$this->subscribeTitle;
	}

	public function getSubscribeTitle() {
		return $this->subscribeTitle;
	}

	public function hasSubscribeContent() {
		return !!$this->subscribeContent;
	}

	public function getSubscribeContent() {
		return $this->subscribeContent;
	}

	public function hasSubscribeForm() {
		return !!$this->subscribeForm;
	}

	public function getSubscribeForm() {
		return $this->subscribeForm;
	}
}

?>
