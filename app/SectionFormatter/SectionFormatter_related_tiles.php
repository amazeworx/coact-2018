<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_related_tiles;

/**
 * Class defining a section renderer
 */
class SectionFormatter_related_tiles extends SectionFormatter implements SectionContract_related_tiles {

	protected $relatedTiles = null;
	protected $relatedTilesTitle = null;

	public function initialiseData() {
		$this->relatedTiles = get_field('related_tiles', $this->post->ID);
		$this->relatedTilesTitle = get_field('related_tiles_title', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->relatedTiles = $this->getFlexibleSectionDataByKey('related_tiles');
		$this->relatedTilesTitle = $this->getFlexibleSectionDataByKey('related_tiles_title');
	}

	public function hasTitle() {
		return !!$this->relatedTilesTitle;
	}

	public function getTitle() {
		return $this->relatedTilesTitle;
	}

	public function hasTiles() {
		return (
			is_array($this->relatedTiles) &&
			sizeof($this->relatedTiles)
		);
	}

	public function hasRelatedTilesData() {
		if ($this->relatedTiles) {
			return !is_null($this->relatedTiles['featured_content_posts']);
		}

		return null;
	}

	public function getTiles() {
		return $this->relatedTiles;
	}
}

?>
