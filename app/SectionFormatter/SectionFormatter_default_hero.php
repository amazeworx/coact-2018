<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_default_hero;

/**
 * Class defining a section renderer
 */
class SectionFormatter_default_hero extends SectionFormatter implements SectionContract_default_hero {

	use \Frank\Traits\GetPrimaryTaxonomyTerm;

	protected $heroImage = null;

	public function initialiseData() {
		$this->heroImage = get_field('hero_image', $this->post->ID);

		if (is_null($this->heroImage)) {
			$this->heroImage = get_field('default_image', 'options');
		}
	}

	public function hasHeroImage() {
		return !!$this->heroImage;
	}

	public function getHeroImage() {
		return $this->heroImage;
	}
}
