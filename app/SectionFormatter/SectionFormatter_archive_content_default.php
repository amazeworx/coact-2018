<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_archive_content_default;

/**
 * Class defining a default archive content
 */
class SectionFormatter_archive_content_default extends SectionFormatter implements SectionContract_archive_content_default {

	/**
	 * The posts present in the archive
	 *
	 * @var array<WP_Post>
	 */
	protected $posts = null;
	/**
	 * The pagination data
	 *
	 * @var string|null
	 */
	protected $pagination = null;
	protected $categoryName = null;
	protected $postPlural = null;


	public function initialiseData() {
		// get the posts from the Wordpress loop and fetch them
		global $post;
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				$this->posts[] = $post;
			}
		}

		// Use the default Wordpress posts pagination and remove default arrows
		$this->pagination = paginate_links(array(
			'prev_text'          => __('Prev'),
			'next_text'          => __('Next'),
		));

		$this->categoryName = $this->initCategoryName();
	}

	public function isPostPlural () {
		$countOfPosts = $this->getCountOfTotalPostsInCategory();

		if($countOfPosts > 1){
			return true;
		}

		return false;
	}

	public function getCategoryName() {
		return $this->categoryName;
	}

	public function getCountOfTotalPostsInCategory() {
		// Get category name
		$category = get_queried_object();
		$categoryName = get_cat_name($category->term_id);

		// Get total post in category
		$args = array(
			'category_name' => $categoryName,
			'post_type' => 'post',
			'posts_per_page' => -1,
		);

		$totalPosts = get_posts( $args );

		return count($totalPosts);
	}

	public function hasPosts() {
		return ($this->posts && count($this->posts));
	}

	public function getPosts() {
		return $this->posts;
	}

	public function hasPagination() {
		return !!($this->pagination);
	}

	public function getPagination() {
		return $this->pagination;
	}

	protected function initCategoryName() {
		// Get category name
		$category = get_queried_object();
		$categoryName = get_cat_name($category->term_id);

		return $categoryName;
	}

}

?>
