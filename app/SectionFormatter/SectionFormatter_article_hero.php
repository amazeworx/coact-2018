<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_article_hero;

/**
 * Class defining a section renderer
 */
class SectionFormatter_article_hero extends SectionFormatter implements SectionContract_article_hero {

	use \Frank\Traits\GetPrimaryTaxonomyTerm;

	protected $heroImage = null;
	protected $postCategory = null;
	protected $postCategoryPermalink = null;
	protected $postIntroduction = null;

	public function initialiseData() {
		$this->heroImage = get_field('featured_image', $this->post->ID);
		$this->postIntroduction = get_field('post_introduction', $this->post->ID);


		if (is_null($this->heroImage)) {
			$this->heroImage = get_field('default_image', 'options');
		}

		$this->postCategory = $this->getPrimaryTaxonomyTerm($this->post->ID, 'category' ) ;

		if ($this->postCategory) {
			$this->postCategoryPermalink = get_term_link($this->postCategory);
		}
	}

	public function hasHeroImage() {
		return !!$this->heroImage;
	}

	public function getHeroImage() {
		return $this->heroImage;
	}

	public function hasPostCategory() {
		return !!$this->postCategory;
	}

	public function getPostCategory() {
		return $this->postCategory->name;
	}

	public function getPostCategoryLink() {
		return $this->postCategoryPermalink;
	}

	public function hasPostTitle() {
		return !!$this->post->post_title;
	}

	public function getPostTitle() {
		return $this->post->post_title;
	}

	public function hasPostIntroduction() {
		return !!$this->postIntroduction;
	}

	public function getPostIntroduction() {
		return $this->postIntroduction;
	}
}
