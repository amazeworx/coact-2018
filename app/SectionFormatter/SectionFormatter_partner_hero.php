<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_partner_hero;

/**
* Class defining a section formatter
*/
class SectionFormatter_partner_hero extends SectionFormatter implements SectionContract_partner_hero {

	public function initialiseData() {
		$this->partnerHeroImage        = get_field('hero_image', $this->post->ID);
		$this->partnerName     = get_field('site_name', $this->post->ID);
		$this->partnerLogo         = get_field('logo', $this->post->ID);
		$this->partnerDescription  = get_field('description', $this->post->ID);
	}

	// Check
	public function hasPartnerHeroImage() {
		return !!($this->partnerHeroImage);
	}

	public function getPartnerHeroImage() {
		return $this->partnerHeroImage;
	}

	public function hasPartnerLogo() {
		return !!($this->partnerLogo);
	}

	public function getPartnerLogo() {
		return $this->partnerLogo;
	}

	public function hasPartnerName() {
		return !!($this->partnerName);
	}

	public function getPartnerName() {
		return $this->partnerName;
	}

	public function hasPartnerDescription() {
		return !!($this->partnerDescription);
	}

	public function getPartnerDescription() {
		return $this->partnerDescription;
	}
}

?>
