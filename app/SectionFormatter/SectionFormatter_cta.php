<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_cta;

/**
 * Class defining a section renderer
 */
class SectionFormatter_cta extends SectionFormatter implements SectionContract_cta {

	protected $ctaImage = null;
	protected $ctaIllustration = null;
	protected $ctaTitle = null;
	protected $ctaDescription = null;
	protected $ctaCta = null;

	public function initialiseData() {
		$this->initialiseDataGeneral();
	}

	public function initialiseFlexibleData() {
		$this->initialiseDataGeneral();
	}

	protected function initialiseDataGeneral() {
		$this->ctaImage = get_field('settings_cta_image', 'options');
		$this->ctaIllustration = get_field('settings_cta_illustration', 'options');
		$this->ctaTitle = get_field('settings_cta_title', 'options');
		$this->ctaDescription = get_field('settings_cta_description', 'options');
		$this->ctaCta = get_field('settings_buttons', 'options');
	}

	public function hasCtaImage() {
		return !!$this->ctaImage;
	}

	public function getCtaImage() {
		return $this->ctaImage;
	}

	public function hasCtaIllustration() {
		return !!$this->ctaIllustration;
	}

	public function getCtaIllustration() {
		return $this->ctaIllustration;
	}

	public function hasCtaTitle() {
		return !!$this->ctaTitle;
	}

	public function getCtaTitle() {
		return $this->ctaTitle;
	}

	public function hasCtaDescription() {
		return !!$this->ctaDescription;
	}

	public function getCtaDescription() {
		return $this->ctaDescription;
	}

	// Change to this is array
	public function hasCtaButtons() {
		return !!$this->ctaCta;
	}

	public function getCtaButtons() {
		return $this->ctaCta;
	}
}

?>
