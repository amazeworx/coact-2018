<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_tag_content_default;

/**
 * Class defining a default archive content
 */
class SectionFormatter_tag_content_default extends SectionFormatter implements SectionContract_tag_content_default {

	/**
	 * The posts present in the archive
	 *
	 * @var array<WP_Post>
	 */
	protected $posts = null;
	/**
	 * The pagination data
	 *
	 * @var string|null
	 */
	protected $pagination = null;
	protected $tagName = null;
	protected $postPlural = null;


	public function initialiseData() {
		// get the posts from the Wordpress loop and fetch them
		global $post;
		if (have_posts()) {
			while (have_posts()) {
				the_post();
				$this->posts[] = $post;
			}
		}

		// Use the default Wordpress posts pagination and remove default arrows
		$this->pagination = paginate_links(array(
			'prev_text'          => __('Prev'),
			'next_text'          => __('Next'),
		));

		$this->tagName = $this->initTagName();
	}

	public function isPostPlural () {
		$countOfPosts = $this->getCountOfTotalPostsInTag();

		if($countOfPosts > 1){
			return true;
		}

		return false;
	}

	public function getTagName() {
		return $this->tagName->name;
	}

	public function getCountOfTotalPostsInTag() {
		// Get category name
		$tagData = get_queried_object();

		// Get total post in category
		$args = array(
			'post_type' => 'post',
			'tag__in' => $tagData->term_taxonomy_id,
			'posts_per_page' => -1,
		);

		$totalPosts = get_posts( $args );

		return count($totalPosts);
	}

	public function hasPosts() {
		return ($this->posts && count($this->posts));
	}

	public function getPosts() {
		return $this->posts;
	}

	public function hasPagination() {
		return !!($this->pagination);
	}

	public function getPagination() {
		return $this->pagination;
	}

	protected function initTagName() {
		// Get category name
		$tagData = get_queried_object();

		return $tagData;
	}

}

?>
