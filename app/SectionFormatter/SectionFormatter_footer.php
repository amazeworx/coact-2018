<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_footer;

/**
 * Class defining a section renderer
 */
class SectionFormatter_footer extends SectionFormatter implements SectionContract_footer {

	protected $subscribeTitle = null;
	protected $subscribeContent = null;
	protected $subscribeForm = null;

	protected $facebookLink = null;
	protected $linkedInLink  = null;
	protected $YoutubeLink  = null;

	public function initialiseData() {
		$this->subscribeTitle = get_field('subscribe_title', 'options');
		$this->subscribeContent = get_field('subscribe_content', 'options');
		$this->subscribeForm = get_field('subscribe_form', 'options');

		$this->facebookLink = get_field('facebook_link', 'options');
		$this->linkedInLink = get_field('linkedin_link',  'options');
		$this->YoutubeLink  = get_field('youtube_link',  'options');
	}

	public function hasSubscribeTitle() {
		return !!$this->subscribeTitle;
	}

	public function getSubscribeTitle() {
		return $this->subscribeTitle;
	}

	public function hasSubscribeContent() {
		return !!$this->subscribeContent;
	}

	public function getSubscribeContent() {
		return $this->subscribeContent;
	}

	public function hasSubscribeForm() {
		return !!$this->subscribeForm;
	}

	public function getSubscribeForm() {
		return $this->subscribeForm;
	}



	public function hasFacebookLink() {
		return !!$this->facebookLink;
	}

	public function getFacebookLink() {
		return $this->facebookLink;
	}

	public function hasLinkedInLink() {
		return !!$this->linkedInLink;
	}

	public function getLinkedInLink() {
		return $this->linkedInLink;
	}

	public function hasYoutubeLink() {
		return !!$this->YoutubeLink;
	}

	public function getYoutubeLink() {
		return $this->YoutubeLink;
	}

}

?>
