<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_post_content;

/**
 * Class defining a section renderer
 */
class SectionFormatter_post_content extends SectionFormatter implements SectionContract_post_content {

	protected $postTitle = null;
	protected $postContent = null;

	public function initialiseData() {
		$this->postTitle = $this->post->post_title;
		$this->postContent = $this->post->post_content;
	}

	public function hasPostTitle() {
		return !!$this->postTitle;
	}

	public function getPostTitle() {
		return $this->postTitle;
	}

	public function hasPostContent() {
		return !!$this->postContent;
	}

	public function getPostContent() {
		return $this->postContent;
	}
}

?>
