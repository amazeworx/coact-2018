<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_top_featured_article;

/**
 * Class defining a section renderer
 */
class SectionFormatter_top_featured_article extends SectionFormatter implements SectionContract_top_featured_article {

	use \Frank\Traits\GetPrimaryTaxonomyTerm;

	protected $featuredArticle = null;
	protected $featuredArticleTitle = null;
	protected $featuredArticleImage = null;
	protected $featuredArticleExcerpt = null;
	protected $featuredArticlePermalink = null;
	protected $featuredArticleCategoryLink = null;
	protected $primaryCategory = null;

	public function initialiseData() {
		$this->featuredArticle = get_field('top_featured_article', $this->post->ID);
		$this->featuredArticleImage = get_field('featured_image', $this->featuredArticle->ID);
		$this->featuredArticleTitle = get_the_title($this->featuredArticle->ID);
		$this->featuredArticleExcerpt = get_the_excerpt($this->featuredArticle->ID);
		$this->featuredArticlePermalink = get_the_permalink($this->featuredArticle->ID);

		if (isset($this->featuredArticle->ID)) {
			$category = $this->getPrimaryTaxonomyTerm($this->featuredArticle->ID, 'category');

			if (isset($category, $category->name)) {
				$this->primaryCategory = $category;
				$this->featuredArticleCategoryLink = get_term_link($this->primaryCategory, 'category');
			}
		}
	}

	public function hasTopFeaturedArticle() {
		return !!$this->featuredArticle;
	}

	public function getTopFeaturedArticle() {
		return $this->featuredArticle;
	}

	public function hasArticleFeaturedImage() {
		return !!$this->featuredArticleImage;
	}

	public function getArticleFeaturedImage() {
		return $this->featuredArticleImage;
	}

	public function hasCategoriesAttachedToArticle() {
		return !!$this->primaryCategory;
	}

	public function getCategoriesAttachedToArticle() {
		return $this->primaryCategory->name;
	}

	public function getCategoriesLinkAttachedToArticle() {
		return $this->featuredArticleCategoryLink;
	}

	public function hasTitleAttachedToArticle() {
		return !!$this->featuredArticleTitle;
	}

	public function getTitleAttachedToArticle() {
		return $this->featuredArticleTitle;
	}

	public function hasExcerptAttachedToArticle() {
		return !!$this->featuredArticleExcerpt;
	}

	public function getExcerptAttachedToArticle() {
		return $this->featuredArticleExcerpt;
	}

	public function hasPermalinkAttachedToArticle() {
		return !!$this->featuredArticlePermalink;
	}

	public function getPermalinkAttachedToArticle() {
		return $this->featuredArticlePermalink;
	}
}

?>
