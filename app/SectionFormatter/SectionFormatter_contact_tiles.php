<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_contact_tiles;

/**
 * Class defining a section renderer
 */
class SectionFormatter_contact_tiles extends SectionFormatter implements SectionContract_contact_tiles {

	protected $contactTiles = null;

	public function initialiseFlexibleData() {
		$this->contactTiles = $this->getFlexibleSectionDataByKey('contact_tiles');
	}

	public function hasContactTiles() {
		return (
			is_array($this->contactTiles) &&
			sizeof($this->contactTiles)
		);
	}

	public function getContactTiles() {
		return $this->contactTiles;
	}
}
