<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_page_title;

/**
 * Class defining a section renderer for the archive title
 */
class SectionFormatter_archive_title extends SectionFormatter implements SectionContract_page_title {

	protected $title = null;


	public function initialiseData() {
		$this->title = get_the_archive_title();
	}

	public function getTitle() {
		return $this->title;
	}

}

?>
