<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_lockedin_header;

/**
 * Class defining a section renderer
 */
class SectionFormatter_lockedin_header extends SectionFormatter implements SectionContract_lockedin_header {
	protected $articleTitle = null;
	protected $partnerLogo = null;
	protected $contactNumber = null;

	public function initialiseData() {
		$this->articleTitle = $this->post->post_title;
		$this->partnerLogo = get_field('logo', $this->post->ID);
		$this->contactNumber = get_field('contact_number', 'options');
	}

	public function hasArticleTitle() {
		return !!$this->articleTitle;
	}

	public function getArticleTitle() {
		return $this->articleTitle;
	}

	public function hasPartnerLogo() {
		return !!$this->partnerLogo;
	}

	public function getPartnerLogo() {
		return $this->partnerLogo;
	}

	public function getContactNumber($stripSpaces = false) {
		if ($stripSpaces === true) {
			return preg_replace('/\s/', '', $this->contactNumber);
		}
		return $this->contactNumber;
	}
}
