<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_testimonials;

/**
* Class defining a section formatter
*/
class SectionFormatter_testimonials extends SectionFormatter implements SectionContract_testimonials {

	protected $testimonials = null;

	public function initialiseData() {
		$this->testimonials = get_field('testimonials_fields', $this->post->ID);
	}

	public function hasTestimonials() {
		return (
			is_array($this->testimonials) &&
			sizeof($this->testimonials)
		);
	}

	public function getTestimonials() {
		return $this->testimonials;
	}
}

?>
