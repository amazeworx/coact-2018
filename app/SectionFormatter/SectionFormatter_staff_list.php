<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_staff_list;

/**
* Class defining a section formatter
*/
class SectionFormatter_staff_list extends SectionFormatter implements SectionContract_staff_list {

	protected $staffList = null;

	public function initialiseData() {
		$this->staffList = get_field('staff_details', $this->post->ID);
	}

	public function hasStaffList() {
		return (
			is_array($this->staffList) &&
			sizeof($this->staffList)
		);
	}

	public function getStaffList() {
		return $this->staffList;
	}
}

?>
