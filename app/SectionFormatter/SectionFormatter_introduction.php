<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_introduction;

/**
 * Class defining a section renderer
 */
class SectionFormatter_introduction extends SectionFormatter implements SectionContract_introduction {

	protected $introductionHeading = null;
	protected $introductionText = null;

	public function initialiseData() {
		$this->introductionHeading = get_field('introduction_heading', $this->post->ID);
		$this->introductionText = get_the_excerpt($this->post->ID);

		$introduction = get_field('introduction_text', $this->post->ID);
		if ($introduction) {
			$this->introductionText = $introduction;
		}
	}

	public function hasIntroductionText() {
		return !!$this->introductionText;
	}

	public function getIntroductionText() {
		return $this->introductionText;
	}

	public function hasIntroductionHeading() {
		return !!$this->introductionHeading;
	}

	public function getIntroductionHeading() {
		return $this->introductionHeading;
	}

}

?>
