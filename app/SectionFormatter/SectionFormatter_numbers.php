<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_numbers;

/**
 * Class defining a section renderer
 */
class SectionFormatter_numbers extends SectionFormatter implements SectionContract_numbers {

	protected $numbers = null;

	public function initialiseFlexibleData() {
		$this->numbers = $this->getFlexibleSectionDataByKey('numbers');
	}

	public function initialiseData() {
		$this->numbers = get_field('numbers', $this->post->ID);
	}

	public function hasNumbers() {
		return (
			is_array($this->numbers) &&
			sizeof($this->numbers)
		);
	}

	public function getNumbers() {
		return $this->numbers;
	}
}

?>
