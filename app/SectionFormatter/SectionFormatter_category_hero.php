<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_category_hero;

/**
 * Class defining a section renderer
 */
class SectionFormatter_category_hero extends SectionFormatter implements SectionContract_category_hero {

	protected $heroImage = null;
	protected $categoryName = null;

	public function initialiseData() {
		$queried_object = get_queried_object();
		$term_id = $queried_object->term_id;
		$this->heroImage = get_field('category_featured_image', 'category_'.$term_id);

		if (is_null($this->heroImage)) {
			$this->heroImage = get_field('default_image', 'options');
		}

		$this->categoryName = $this->initCategoryName();
	}

	public function hasHeroImage() {
		return !!$this->heroImage;
	}

	public function getHeroImage() {
		return $this->heroImage;
	}

	public function hasCategoryTitle() {
		return !!$this->categoryName;
	}

	public function getCategoryTitle() {
		return $this->categoryName;
	}

	protected function initCategoryName() {
		// Get category name
		$category = get_queried_object();
		$categoryName = get_cat_name($category->term_id);

		return $categoryName;
	}
}
