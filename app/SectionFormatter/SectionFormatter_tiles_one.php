<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_tiles_one;

/**
* Class defining a section formatter
*/
class SectionFormatter_tiles_one extends SectionFormatter implements SectionContract_tiles_one {

	protected $featuredContent = null;
	protected $featuredContentTitle = null;
	protected $featuredContentLink = null;

	public function initialiseData() {
		$this->featuredContent = get_field('tiles_one_featured_content_posts', $this->post->ID);
		$this->featuredContentTitle = get_field('tiles_one_featured_content_title', $this->post->ID);
		$this->featuredContentLink = get_field('tiles_one_featured_content_button', $this->post->ID);
	}

	public function initialiseFlexibleData() {
		$this->featuredContent = $this->getFlexibleSectionDataByKey('tiles_one_featured_content_posts');
		$this->featuredContentTitle = $this->getFlexibleSectionDataByKey('tiles_one_featured_content_title');
		$this->featuredContentLink = $this->getFlexibleSectionDataByKey('tiles_one_featured_content_button');
	}

	public function hasFeaturedContentTitle() {
		return !!$this->featuredContentTitle;
	}

	public function getFeaturedContentTitle() {
		return $this->featuredContentTitle;
	}

	public function hasFeaturedContentLink() {
		return !!$this->featuredContentLink;
	}

	public function getFeaturedContentLink() {
		return $this->featuredContentLink;
	}

	public function hasFeaturedContent() {
		return (
			is_array($this->featuredContent) &&
			sizeof($this->featuredContent)
		);
	}

	public function getFeaturedContent() {
		return $this->featuredContent;
	}
}

?>
