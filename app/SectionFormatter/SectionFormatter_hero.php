<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_hero;

/**
* Class defining a section formatter
*/
class SectionFormatter_hero extends SectionFormatter implements SectionContract_hero {

	protected $image = null;
	protected $title = null;
	protected $subtitle = null;
	protected $ctas = null;

	// mapping for text colours - ["color", ["regex", "preg_replace template"]]
	protected $textColors = array(
		'orange' => array('/\_(.*?)\_/', '<span class="hero-text hero-text--orange">$1</span>'),
		'green' => array('/\*(.*?)\*/', '<span class="hero-text hero-text--green">$1</span>')
	);

	public function initialiseData() {
		$this->image    = get_field('hero_image',    $this->post->ID);
		$this->title    = get_field('hero_title',    $this->post->ID);
		$this->subtitle = get_field('hero_subtitle', $this->post->ID);
		$this->ctas     = get_field('hero_ctas', $this->post->ID);
	}

	public function hasImage() {
		return !!($this->image);
	}

	public function getImage() {
		return $this->image;
	}

	public function hasTitle() {
		return !!($this->title);
	}

	public function getTitle() {
		return $this->formatTextColours($this->title);
	}

	public function hasSubtitle() {
		return !!($this->subtitle);
	}

	public function getSubtitle() {
		return $this->subtitle;
	}

	public function hasCtas() {
		return !!($this->ctas);
	}

	public function getCtas() {
		return $this->ctas;
	}

	private function formatTextColours($string) {
		return preg_replace(
			array($this->textColors['orange'][0], $this->textColors['green'][0]),
			array($this->textColors['orange'][1], $this->textColors['green'][1]),
			$string
		);
	}

}

?>
