<?php
namespace App\SectionFormatter;

use Frank\SectionFormatter\SectionFormatter;
use App\SectionContract\SectionContract_general_tiles;

/**
* Class defining a section formatter
*/
class SectionFormatter_general_tiles extends SectionFormatter implements SectionContract_general_tiles {

	protected $generalTilesData = null;

	public function initialiseFlexibleData() {
		$this->generalTilesData = $this->getFlexibleSectionDataByKey('general_tiles');
	}

	public function hasGeneralTiles() {
		return (
			is_array($this->generalTilesData) &&
			sizeof($this->generalTilesData)
		);
	}

	public function getGeneralTiles() {
		return $this->generalTilesData;
	}
}

?>
