<?php
namespace Config\ACF;

use Config\ConfigInterface;

/**
 * Class handling all the colours in the ACF picker
 */
class Colours implements ConfigInterface {

	public function loadConfig() {
		add_action('in_admin_footer', array($this, 'addCustomColours'));
	}

	public function addCustomColours() {
		/*
		?>
		<script type="text/javascript">
		jQuery(document).ready(function(){
			var defaultPalettes = [
				'#AC1D2A',
				'#E30059',
				'#E3585D',
				'#F08A00',
				'#F9BA05',
				'#6AB023',
				'#6BBAB6'
			];
			jQuery('.acf-color_picker').each(function() {
				jQuery(this).iris({
					palettes: defaultPalettes,
					change: function(event, ui){
						jQuery(this).parents('.wp-picker-container').find('.wp-color-result').css('background-color', ui.color.toString());
					}
				});
			});
			if ('acf' in window && acf) {
				acf.add_filter('color_picker_args', function(args, $field) {
					args.palettes = defaultPalettes;
					return args;
				});
			}
		});
		</script>
		<?php
		*/
	}

}
?>
