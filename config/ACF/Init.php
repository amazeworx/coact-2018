<?php
namespace Config\ACF;

use Config\ConfigInterface;

/**
 * Class handling all the initialisation config related to the ACF PLugin
 *
 * Example: add options page
 */
class Init implements ConfigInterface {

	public function loadConfig() {
		add_action('acf/init', array($this, 'my_acf_init'));
	}

	function my_acf_init() {
		if (defined('GOOGLE_MAPS_API')) {
			acf_update_setting('google_api_key', GOOGLE_MAPS_API);
		} else {
			throw new \Exception("GOOGLE_MAPS_API must be defined");
		}

		if (function_exists('acf_add_options_page')) {
			acf_add_options_page([
				'page_title' => 'Theme Settings'
			]);
		}
	}

}
?>
