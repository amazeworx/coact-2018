<?php
namespace Config;

use Config\ConfigInterface;

class ConfigLoader implements ConfigInterface {

	protected $configurationClasses = null;

	/**
	 * Configuration loader
	 *
	 * @param array<string> $configurationClasses the list of configuration classes
	 */
	public function __construct(array $configurationClasses) {
		$this->configurationClasses = $configurationClasses;
	}

	/**
	 * Load the various configuration
	 *
	 * @return void
	 */
	public function loadConfig() {
		if ($this->configurationClasses && count($this->configurationClasses)) {
			foreach ($this->configurationClasses as $className) {
				// Check if the configuration class is implementing the ConfigInterface class
				$interfaces = class_implements($className);
				if (isset($interfaces['Config\ConfigInterface'])) {
					$config = new $className();
					$config->loadConfig();
				} else {
					throw new \Exception("Config {$className} doesn't implement Config\ConfigInterface.");
				}
			}
		}
	}

}
?>
