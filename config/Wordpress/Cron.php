<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class handling the declaration of the automated tasks
 */
class Cron implements ConfigInterface {

	/**
	 * Load the admin configuration
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_filter('cron_schedules', [$this, 'cronSchedules']);

		$this->scheduleCron(CRON_BRIDGE_ENTRIES, 'doEntriesSync', 0, 0, '5min');
	}

	/**
	 * Schedule a Wordpress cron
	 *
	 * @param string : $cronName the cron name
	 * @param string : $cronFunctionName the cron function name
	 * @param int : $startHour the start hour of the cron
	 * @param int : $startMinutes the starting minute. Default 0
	 * @param string : $frequency the cron requency. please reference https://codex.wordpress.org/Function_Reference/wp_schedule_event Default 'daily'
	 */
	protected function scheduleCron($cronName, $cronFunctionName, $startHour, $startMinutes = 0, $frequency = 'daily') {
		// Declare the cron action
		add_action($cronName, array($this, $cronFunctionName));

		// Schedule the cron
		if (!wp_next_scheduled($cronName)) {
			$timezone = new \DateTimeZone('Australia/Sydney');
			$startingDateTime = new \DateTime('tomorrow', $timezone);
			$startingDateTime->setTime($startHour, $startMinutes);
			wp_schedule_event($startingDateTime->format('U'), $frequency, $cronName);
		}
	}

	public function cronSchedules($schedules){
		if (!isset($schedules['5min'])) {
			$schedules['5min'] = [
				'interval' => 5 * 60,
				'display' => __('Once every 5 minutes')
			];
		}
		return $schedules;
	}

	public function doEntriesSync() {
		$entries = new \App\EntriesSynchronisation\Entries();
		$entries->synchroniseEntries();
	}

}
?>
