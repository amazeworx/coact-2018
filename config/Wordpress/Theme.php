<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class handling all the config related to the theme settings
 *
 * Example: theme support, image sizes, CSS, JS, ...
 */
class Theme implements ConfigInterface {

	/**
	 * Load the theme configuration
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_action('after_setup_theme', array($this, 'afterThemeSetup'));

		add_action('wp_enqueue_scripts', array($this, 'setCssStyles'));
		add_action('wp_enqueue_scripts', array($this, 'setJsScripts'));

		// add_filter('template_include', array($this, 'setJsScriptsForServiceLocator'));

		if (defined('WPSEO_VERSION')){
			add_action('get_header', array($this, 'startRemovingYoastComments'));
			add_action('wp_head', array($this, 'endRemovingYoastComments'), 999);
		}

        add_action( 'wp_footer', array($this, 'googleMapsServiceFinder'), 999 );
        add_filter( 'rest_service-partner_query', array($this, 'setRestServicePartnerPerPage'), 10, 2 );
        add_filter( 'script_loader_tag', array($this, 'wpse_script_loader_tag'), 10, 2 );
	}

	public function startRemovingYoastComments() {
		ob_start(function ($o) {
			return preg_replace('/^<!--.*?[Y]oast.*?-->$/mi','',$o);
		});
	}

	public function endRemovingYoastComments() {
		ob_end_flush();
	}

	public function afterThemeSetup() {
		add_theme_support('post-thumbnails');
		add_theme_support('title-tag');

		// add_image_size('custom_size', 1440, 800);
	}

	public function setCssStyles() {
		wp_enqueue_style(
			'app',
			get_template_directory_uri() . '/assets/app.css',
			array(),
			APP_VERSION,
			'screen'
		);
		// wp_enqueue_style(
		// 	'material-icons',
		// 	'https://fonts.googleapis.com/icon?family=Material+Icons',
		// 	array()
		// );
	}

	public function setJsScripts() {
		wp_enqueue_script(
			'fonts-dll',
			get_template_directory_uri() . '/dll/fonts.dll.js',
			array(),
			APP_VERSION,
			true
		);
		wp_enqueue_script(
			'fonts',
			get_template_directory_uri() . '/assets/fonts.js',
			array(
				'fonts-dll'
			),
			APP_VERSION,
			true
		);
		wp_enqueue_script(
			'vendors-dll',
			get_template_directory_uri() . '/dll/vendors.dll.js',
			array(),
			APP_VERSION,
			true
		);
		wp_enqueue_script(
			'select2',
			'https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js',
			array(),
			APP_VERSION,
			true
		);
		wp_enqueue_script(
			'service-finder',
			get_template_directory_uri() . '/service-finder/service-finder.js',
			array('jquery', 'select2'),
			APP_VERSION,
			true
		);
		wp_enqueue_script(
			'app',
			get_template_directory_uri() . '/assets/app.js',
			array(
				'vendors-dll',
				'fonts'
			),
			APP_VERSION,
			true
		);
	}

	function setJsScriptsForServiceLocator( $template ){
		if (is_page_template('template-service_locator.php')) {
			$googleMapsApiUrl = 'https://maps.googleapis.com/maps/api/js?key='.GOOGLE_MAPS_API.'&v=3.exp&libraries=geolocation,geometry,drawing,places';
			wp_enqueue_script('google-maps', $googleMapsApiUrl);
		}
		return $template;
	}

    function googleMapsServiceFinder() {
        /* Enqueue Google Maps Javascript */
        if (is_page_template('template-service_finder.php')) {
            echo '<script defer src="https://maps.googleapis.com/maps/api/js?key='.GOOGLE_MAPS_API.'&libraries=places,geometry" data-cfasync="false"></script>';
            echo '<script src="https://unpkg.com/@googlemaps/markerclustererplus/dist/index.min.js" data-cfasync="false"></script>';
        }
    }

    function wpse_script_loader_tag( $tag, $handle ) {
        if ( 'service-finder' == $handle || 'jquery-core' == $handle || 'select2' == $handle ) {
            return str_replace( ' src', ' data-cfasync="false" src', $tag );
        }
        return $tag;
    }

    function setRestServicePartnerPerPage( $args, $request ) {
        /* change amount of posts returned by REST API to unlimited */
        $max = max( (int)$request->get_param( 'per_page' ), 999999 );
        $args['posts_per_page'] = $max;
        return $args;
    }

}
?>
