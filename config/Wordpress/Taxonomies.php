<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class handling all the taxonomies declarations
 *
 */
class Taxonomies implements ConfigInterface {

	/**
	 * Load the taxonomies declarations
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_action('init', array($this, 'taxonomiesDeclaration'));

	}


	public function taxonomiesDeclaration() {
		// register_taxonomy(
		// 	'case_studies_category',
		// 	'case-studies',
		// 	[
		// 		'label' => __('Category'),
		// 		'hierarchical' => true,
		// 		'public' =>  true,
		// 		'show_ui' => true,
		// 	]
		// );
		// register_taxonomy_for_object_type('case_studies_category', 'case-studies');

		register_taxonomy(
			'service_types',
			'service-partner',
			array(
				'hierarchical' => true,
				'label' => 'Service Types',
				'show_ui' => true,
				'query_var' => true,
                'show_in_rest' => true,
				'show_admin_column' => false,
			)
		);

		register_taxonomy(
			'serviced_areas',
			'service-partner',
			array(
				'hierarchical' => false,
				'label' => 'Serviced Areas',
				'show_ui' => true,
				'query_var' => true,
				'show_admin_column' => false,
				'publicly_queryable' => false,
				'show_in_rest' => false,
			)
		);
	}

}
?>
