<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class handling all the config related to the theme menus
 *
 */
class Menus implements ConfigInterface {

	/**
	 * Load the admin configuration
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_action('init', array($this, 'registerMenus'));

	}


	public function registerMenus() {
		register_nav_menus(
			array(
				'primary_navigation' => __('Primary Navigation'),
				'primary_navigation_side' => __('Primary Navigation Side'),
				'individual_menu' => __('Individual Menu'),
				'employer_menu' => __('Employer Menu'),
				'service_partner_menu' => __('Service Partner Menu'),
				'referral_partner_menu' => __('Referral Partner Menu'),
				'about_us_menu' => __('About Us Menu'),
				'innovations_menu' => __('Innovations Menu'),
				'footer_navigation'  => __('Footer Navigation'),
				'mobile_navigation'  => __('Mobile Navigation'),
			)
		);
	}
}
?>
