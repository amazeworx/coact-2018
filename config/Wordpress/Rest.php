<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class declaring the custo Wordpress REST controllers
 *
 */
class Rest implements ConfigInterface {

	/**
	 * Load the REST configuration
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_action('rest_api_init', array($this, 'registerEndpoints'));

	}


	public function registerEndpoints() {
		// Add the endpoints
	}

}
?>
