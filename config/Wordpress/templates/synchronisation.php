<?php
use \App\Utilities\RoundTime;
$successNotice = null;

if (isset($_GET['action'])) {
	if ($_GET['action'] === 'entries_sync') {
		$entries = new \App\EntriesSynchronisation\Entries();
		$entries->synchroniseEntries();
		$successNotice = "<p>Entries synchronisation performed</p>";
	}
}

?>
<div id="poststuff" class="theme-options wrap">
	<h1 class="theme-options__title">Synchronisation Settings</h1>

	<?php if ($successNotice) : ?>
		<div class="notice notice-success is-dismissible">
			<?php echo $successNotice; ?>
		</div>
	<?php endif; ?>


	<div class="postbox">
		<h2 class="hndle ui-sortable-handle">Contact Submissions Synchronisation</h2>
		<div class="inside">
			<p class="theme-options__text">
				The entries will be synchronised in: <strong><?php echo RoundTime::getRoundedTime(wp_next_scheduled(CRON_BRIDGE_ENTRIES)); ?></strong>
			</p>
			<div class="theme-options__buttons tachyons">
				<a href="/wp-admin/admin.php?page=sync-settings&amp;action=entries_sync" class="button button-primary">Manually Synchronise the Entries</a>
			</div>
		</div>
	</div>

</div>
