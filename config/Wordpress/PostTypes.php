<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class handling all the post types declarations
 *
 */
class PostTypes implements ConfigInterface {

	/**
	 * Load the post types declarations
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_action('init', array($this, 'postTypesDeclaration'));
		add_action('save_post', array($this, 'postSaveCustomisation'));

	}


	public function postTypesDeclaration() {
		register_post_type('service-partner', array(
			'label' => 'Service Partners',
			'description' => '',
			'public' => true,
			'exclude_from_search' => false,
			'publicly_queryable' => true,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => true,
			'rewrite' => array(
				'slug' => 'service-partner',
				'with_front' => false,
			),
			'query_var' => true,
			'has_archive' => false,
            'show_in_rest'      => true,
			'supports' => array('title','editor','revisions','page-attributes'),
			'labels' => array (
				'name' => 'Service Partners',
				'singular_name' => 'Service Partner',
				'menu_name' => 'Service Partners',
				'add_new' => 'Add Service Partner',
				'add_new_item' => 'Add New Service Partner',
				'edit' => 'Edit',
				'edit_item' => 'Edit Service Partner',
				'new_item' => 'New Service Partner',
				'view' => 'View Service Partner',
				'view_item' => 'View Service Partner',
				'search_items' => 'Search Service Partners',
				'not_found' => 'No Service Partners Found',
				'not_found_in_trash' => 'No Service Partners Found in Trash',
				'parent' => 'Parent Service Partner',
			)
		));

		register_post_type('form-submission', array(
			'label' => 'Contact form submission',
			'description' => '',
			'public' => false,
			'exclude_from_search' => false,
			'publicly_queryable' => false,
			'show_ui' => true,
			'show_in_menu' => true,
			'capability_type' => 'post',
			'map_meta_cap' => true,
			'hierarchical' => true,
			'query_var' => true,
			'has_archive' => false,
			'supports' => array('title','editor'),
		));
	}

	public function postSaveCustomisation($postId) {
		// if (wp_is_post_revision($postId)) {
		// 	return;
		// }
		//
		// $postType = get_post_type($postId);
		//
		// if ($postType === 'case-studies') {
		// 	// Do something
		// }
	}

}
?>
