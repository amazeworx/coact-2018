<?php
namespace Config\Wordpress;

use Config\ConfigInterface;

/**
 * Class handling all the config related to the admin customisation
 *
 * Example: editor styles, ...
 */
class Admin implements ConfigInterface {

	/**
	 * Load the admin configuration
	 *
	 * @return void
	 */
	public function loadConfig() {

		add_action('admin_init', array($this, 'customEditorStyles'));
		add_action('admin_print_styles', array($this, 'customAdminStyles'), 20);
		add_action("admin_menu", array($this, 'optionsMenu'));
		add_filter('upload_mimes', array($this, 'addUploadMimes'));

	}

	public function customEditorStyles() {
		// add_editor_style('custom-editor-style.css');
	}

	public function customAdminStyles() {
		wp_register_style('admin-custom', get_template_directory_uri() . '/assets/admin-custom.css');
		wp_enqueue_style('admin-custom');
	}

	public function addUploadMimes($mimes) {
		$mimes = array_merge($mimes, [
			// 'xml' => 'application/xml'
			'svg' => 'image/svg+xml',
		]);
		return $mimes;
	}

	function optionsMenu() {
		/**
		* @function merivale_options_menu
		* Creates the synchronisation settings menu item
		*/
		$page_title = "Synchronisation Settings";
		$menu_title = "Synchronisation Settings";
		$capability = "manage_options";
		$menu_slug  = "sync-settings";
		$function   = [$this, 'synchronisationSettingsPage'];
		$icon_url   = "dashicons-update";
		$position   = 90;

		add_menu_page(
			$page_title,
			$menu_title,
			$capability,
			$menu_slug,
			$function,
			$icon_url,
			$position
		);
	}

	public function synchronisationSettingsPage() {
		// Include each settings block
		include(get_template_directory() . "/config/Wordpress/templates/synchronisation.php");
	}

}
?>
