<?php
namespace Config;

interface ConfigInterface {

	/**
	 * Load the configuration
	 *
	 * @return void
	 */
	public function loadConfig();

}

?>
