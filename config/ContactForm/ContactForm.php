<?php
namespace Config\ContactForm;

use Config\ConfigInterface;

/**
 * Class handling all the Contact Form 7 customisations
 *
 */
class ContactForm implements ConfigInterface {

	public function loadConfig() {
		// Add the ACF fields to the Algolia index
		add_filter('wpcf7_before_send_mail', array($this, 'handleFormSubmission'), 10, 2);

	}

	/**
	 * Handle the form submission
	 *
	 * @param contact form instance
	 *
	 * @return void
	 */
	public function handleFormSubmission() {
		$timezone = null;
		if (get_option('timezone_string')) {
			$timezone = new \DateTimeZone(get_option('timezone_string'));
		}
		$now = new \DateTime('now', $timezone);

		if (class_exists('WPCF7_Submission')) {
			$submission = \WPCF7_Submission::get_instance();

			$defaultEnquiryFormId = get_field('enquiry_form', 'options');

			if (
				$defaultEnquiryFormId &&
				intval($submission->get_posted_data('_wpcf7')) === intval($defaultEnquiryFormId)
			) {

				$form_data = json_decode(json_encode($submission->get_posted_data()));
				$post_title = 'Enquiry form submission - ' . $submission->get_posted_data('contact-type') . ' - ' . $now->format('Y-m-d H:i:s');

				// Add the page URL
				if (isset($_SERVER['HTTP_REFERER'])) {
					$form_data->page_referrer = $_SERVER['HTTP_REFERER'];
				}

				// Assign the API ID
				$this->addApiId($form_data);

				// Assign UTM data
				$this->addUtmData($form_data);

				if (isset($form_data->_wpcf7cf_hidden_group_fields)) {
					unset($form_data->_wpcf7cf_hidden_group_fields);
				}
				if (isset($form_data->_wpcf7cf_hidden_groups)) {
					unset($form_data->_wpcf7cf_hidden_groups);
				}
				if (isset($form_data->_wpcf7cf_visible_groups)) {
					unset($form_data->_wpcf7cf_visible_groups);
				}
				if (isset($form_data->_wpcf7cf_options)) {
					unset($form_data->_wpcf7cf_options);
				}

				$form_data_encoded = json_encode($form_data);

				wp_insert_post([
					'post_content' => $form_data_encoded,
					'post_title' => $post_title,
					'post_status' => 'draft',
					'post_type' => 'form-submission',
					'post_date' => $now->format('Y-m-d H:i:s'),
				]);
			}
		}
	}

	protected function addApiId(&$form_data) {
		if (isset($_SERVER['HTTP_REFERER'])) {
			$postId = url_to_postid($_SERVER['HTTP_REFERER']);
			if ($postId) {
				$post = get_post($postId);
				// Check if service partner page
				if (
					isset($post->post_type) &&
					$post->post_type === 'service-partner'
				) {
					// get the site id for the bridge API
					$apiId = get_field('api_id', $postId);
					if ($apiId) {
						$form_data->api_id = $apiId;
					}
				}
			}
		}
	}

	/**
	 * Add UTM data to form submission
	 */
	protected function addUtmData(&$form_data) {
		$utm = $this->parseUtmz();

		$_gclid = false;
		if (
			isset($_COOKIE['_gclid']) &&
			!empty($_COOKIE['_gclid']) &&
			$_gclid = sanitize_text_field($_COOKIE['_gclid'])
		) {
			$form_data->gclid = $_gclid;
		}

		if (
			is_array($utm) &&
			sizeof($utm)
		) {
			foreach($utm as $utm_key => $utm_tag) {
				$param = sanitize_title($utm_key);

				$form_data->$param = esc_attr($utm_tag);
			}
		}
	}

	/**
	 * Function copied from CF7 UTM plugin
	 */
	protected function parseUtmz() {

		if (isset($_COOKIE['_utmz_cf7'])) {
			$utmz = $_COOKIE['_utmz_cf7'];
		} elseif (isset($_COOKIE['__utmz'])) {
			$utmz = $_COOKIE['__utmz'];
		} else {
			return false;
		}
		$UTM_arr = array();

		//Break cookie in half
		$utmz_b = strstr($utmz, 'u');

		//assign variables to first half of cookie
		//list($this->utmz_domainHash, $this->utmz_timestamp, $this->utmz_sessionNumber, $this->utmz_campaignNumber) = explode('.', $utmz_a);

		//break apart second half of cookie
		$utmzPairs = array();
		$z = explode('|', $utmz_b);
		foreach ($z as $value) {
			$v = explode('=', $value);
			$utmzPairs[$v[0]] = $v[1];
		}

		//Variable assignment for second half of cookie
		foreach ($utmzPairs as $key => $value) {
			switch ($key) {
				case 'utmcsr':
					$UTM_arr['utm_source'] = $value;
					break;
				case 'utmcmd':
					$UTM_arr['utm_medium'] = $value;
					break;
				case 'utmctr':
					$UTM_arr['utm_term'] = $value;
					break;
				case 'utmcct':
					$UTM_arr['utm_content'] = $value;
					break;
				case 'utmccn':
					$UTM_arr['utm_campaign'] = $value;
					break;
				case 'utmgclid':
					$UTM_arr['utm_gclid'] = $value;
					break;
				default:
					//do nothing
			}
		}

		return $UTM_arr;
	}
}
?>
