<?php
namespace Config\Algolia;

use Config\ConfigInterface;

/**
 * Class handling all the additional data pushed to algolia
 *
 * Implementation based on https://community.algolia.com/wordpress/advanced-custom-fields.html
 *
 */
class AdditionalFields implements ConfigInterface {

	public function loadConfig() {
		// Add the ACF fields to the Algolia index
		add_filter('algolia_post_shared_attributes', array($this, 'add_posts_attributes'), 10, 2);
		add_filter('algolia_searchable_post_shared_attributes', array($this, 'add_posts_attributes'), 10, 2);

	}

	/**
	 * Add the new attributes for post types
	 *
	 * @param array $attributes
	 * @param WP_Post $post
	 *
	 * @return array
	 */
	public function add_posts_attributes($attributes, \WP_Post $post) {
		if (
			$post->post_type === 'service-partner'
		) {
			$attributes = $this->addPartnerDetails($attributes, $post);
		} else if (
			$post->post_type === 'post'
		) {
			$attributes = $this->addPostDetails($attributes, $post);
		}

		return $attributes;
	}

	/**
	 * Add the location, contact details, description and proof points
	 *
	 * @param array $attributes
	 * @param WP_Post $post
	 *
	 *
	 * @return array
	 */
	protected function addPartnerDetails($attributes, \WP_Post $post) {
		// Get the geolocation data
		$geolocation = get_field('location', $post->ID);
		$enquiryContactNumber = null;
		$existingClientNumbers = null;

		$phoneNumbers = get_field('contact_numbers', $post->ID);

		$description = get_field('description', $post->ID);
		$proof_points = get_field('checkmark_list', $post->ID);

		// Set the geolocation details
		if (
			isset(
				$geolocation['lat'],
				$geolocation['lng'],
				$geolocation['address']
			)
		) {
			// Add the geolocation details
			$attributes['_geoloc'] = [
				'lat' => $geolocation['lat'],
				'lng' => $geolocation['lng'],
			];
			$attributes['address'] = $geolocation['address'];
		}

		//Set array of phone to number to key
		if ($phoneNumbers && is_array($phoneNumbers)) {
			$attributes['phone_numbers'] = $phoneNumbers;
		}

		// Set the description
		if (isset($description)) {
			$attributes['description'] = $description;
		}

		// Set the proof_points
		if (isset($proof_points)) {
			$formattedProofPoints = [];
			foreach ($proof_points as $item) {
				if (isset($item['point'])) {
					$formattedProofPoints[] = $item['point'];
				}
			}
			$attributes['proof_points'] = $formattedProofPoints;
		}

		return $attributes;
	}

	protected function addPostDetails($attributes, \WP_Post $post) {
		$post_image = get_field('featured_image', $post->ID);

		//Set posts image
		if (isset($post_image)) {
			$attributes['post_image'] = $post_image;
		}

		return $attributes;
	}
}
?>
