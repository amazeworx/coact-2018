<?php
namespace Config\Algolia;

use Config\ConfigInterface;

/**
 * Class handling which post types are sent to algolia
 *
 */
class SearchablePosts implements ConfigInterface {

	public function loadConfig() {
		// Define which are the searchable posts for Algolia
		add_filter('algolia_searchable_post_types', array($this, 'set_searchable_posts'));

	}

	/**
	 * Define the searchable posts
	 *
	 * @param array $post_types
	 *
	 * @return array the searchable post types
	 */
	public function set_searchable_posts($post_types) {
		return [
			'post',
			'page',
			'service-partner',
		];
	}


}
?>
