Frank Seed Theme
================

Version 0.1

The Frank Seed Theme aims at helping the creation of a Wordpress theme for the clients.

The goal is to provide a system of dynamic injection of sections within templates.
