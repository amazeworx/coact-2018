module.exports =  {
	plugins: [
		require('autoprefixer')(),
		require('cssnano')({
			discardUnused: false,
			minifyFontValues: false,
			zindex: false
		})
	]
};
